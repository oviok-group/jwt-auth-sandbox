Ce dossier contient toute la documentation facilitant aussi bien la compréhension du besoin que d'installation de l'application :

- **Scripts SQL** : 
	- les DDL (scripts de génération de schémas, donc des tables)
	- les DML (scripts d'insertion des données dans les tables)

**NB** : les scripts SQL fournis concernent les SGBD suivants : 
- h2
- Maraidb
- Mysql
- Oracle
- PostgreSQL
- MS SQL Server

- **Images** :
	- le diagramme des cas d'utilisation (use case)
	- le diagramme de séquences du fonctinnement générique
	- le diagramme de classes des objets de gestion des propriétés de l'application
	- le diagramme de classes du modèle de données

- **Autres informations** : 
