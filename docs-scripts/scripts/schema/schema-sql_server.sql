/*  ---------------------------------------
	-- DDL : Data Definition Language
	-- DATABASE : POSTGRESQL 
	-- SCHEMA OR CATALOG :  
	---------------------------------------
*/

/*  -------------------------
	-- Scripts de Suppression 
	---------------------------
*/

-- Suppression de la clé étrangère
GO

ALTER TABLE USER_ROLES 
	DROP CONSTRAINT FKs6y4k5lgw4a4ei5lj2u2ibkh5

-- Suppression de la table T_USERS
DROP TABLE T_USERS

-- Suppression de la table USER_ROLES
DROP TABLE USER_ROLES

-- Suppression de la séquence hibernate_sequence
DROP sequence hibernate_sequence
GO

/*  ------------------------
	-- Scripts de Création 
	------------------------
*/

-- Création de la séquence pour incrément automatique de l'identifiant
GO

CREATE sequence hibernate_sequence start with 1 increment by 1

-- Création de la table T_USERS
CREATE TABLE T_USERS (
	ID BIGINT NOT NULL,
	USER_ACTIVE BIT,
	DATE_CREATION DATE,
	EMAIL VARCHAR(25) NOT NULL,
	HORODATAGE DATETIME2,
	USER_PASSWORD VARCHAR(255),
	LOGIN VARCHAR(80) NOT NULL,
	OPTLOCK INT DEFAULT 0 NOT NULL,
	primary KEY (ID)
)

-- Création de la TABLE USERS_ROLES
CREATE TABLE USER_ROLES (
	USER_ID BIGINT NOT NULL,
	ROLES INT
)

-- Contrainte unicité de l'adresse électronique
ALTER TABLE T_USERS 
	ADD CONSTRAINT UK_kbdgs6v1gu1pcoq5u9ohje6ep UNIQUE (EMAIL)

-- Contrainte unicité du login
ALTER TABLE T_USERS 
	ADD CONSTRAINT UK_37ow8xtktqu7dgkbq7kfywdh0 UNIQUE (LOGIN)

-- Contrainte de la clé étrangère dans la table USERS_ROLES
ALTER TABLE USER_ROLES 
	ADD CONSTRAINT FKs6y4k5lgw4a4ei5lj2u2ibkh5 
	FOREIGN KEY (USER_ID) 
	REFERENCES T_USERS
GO