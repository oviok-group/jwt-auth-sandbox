/*  ---------------------------------------
	-- DDL : Data Definition Language
	-- DATABASE : POSTGRESQL 
	-- SCHEMA OR CATALOG :  
	---------------------------------------
*/

/*	---------------------------
	-- Scripts de Suppression 
	---------------------------
*/

-- Suppression de la clé étrangère de la table USER_ROLES
ALTER TABLE IF EXISTS USER_ROLES 
DROP CONSTRAINT IF EXISTS FKs6y4k5lgw4a4ei5lj2u2ibkh5;

-- Suppression de la table T_USERS
DROP TABLE IF EXISTS T_USERS CASCADE;

-- Suppression de la table USER_ROLES
DROP TABLE IF EXISTS USER_ROLES CASCADE;

-- Suppression de la séquence hibernate_sequence
DROP sequence IF EXISTS hibernate_sequence;

/*  ------------------------
	-- Scripts de Création 
	------------------------
*/ 
-- Création de la séquence pour incrément automatique de l'identifiant
CREATE sequence hibernate_sequence START 1 increment 1;

-- Création de la table T_USERS
CREATE TABLE T_USERS (
	ID INT8 NOT NULL,
	USER_ACTIVE BOOLEAN,
	DATE_CREATION DATE,
	EMAIL VARCHAR(25) NOT NULL,
	HORODATAGE TIMESTAMP,
	USER_PASSWORD VARCHAR(255),
	LOGIN VARCHAR(80) NOT NULL,
	OPTLOCK INT4 DEFAULT 0 NOT NULL,
	PRIMARY KEY (ID)
);

-- Création de la table USERS_ROLES
CREATE TABLE USER_ROLES (
	USER_ID INT8 NOT NULL,
	ROLES INT4
);

-- Contrainte unicité de l'adresse électronique
ALTER TABLE IF EXISTS T_USERS 
	ADD CONSTRAINT UK_kbdgs6v1gu1pcoq5u9ohje6ep unique (EMAIL);

-- Contrainte unicité du login
ALTER TABLE IF EXISTS T_USERS 
 	ADD CONSTRAINT UK_37ow8xtktqu7dgkbq7kfywdh0 unique (LOGIN);

-- Contrainte de la clé étrangère dans la table USERS_ROLES
ALTER TABLE IF EXISTS USER_ROLES 
	ADD CONSTRAINT FKs6y4k5lgw4a4ei5lj2u2ibkh5 
	FOREIGN KEY (USER_ID) 
	REFERENCES T_USERS;