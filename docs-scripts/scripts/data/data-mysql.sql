/* Inserion dans la table T_USERS */
INSERT INTO `T_USERS` (`ID`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `OPTLOCK`) 
VALUES 
(1, b'1', '2020-10-19', 'admin1.test@live.fr', '2020-10-19 02:41:45', '$2a$10$uk1lLoeMvFhGWEquR3sqAONrmwbgc4/6J.l5gDt/S7iKVRuV42202', 'admin1', 0),
(2, b'0', '2020-10-19', 'admin2.test@live.fr', '2020-10-19 02:41:45', '$2a$10$4A7ZAl5CLZgofMk4T2xxOuvuHIX9etduxjd0gNJIlkUwjzRvJkOme', 'admin2', 0),
(3, b'1', '2020-10-19', 'client1.test@live.fr', '2020-10-19 02:41:45', '$2a$10$UVLZvESy36fbafC9Ta5S.OZdLP4bm5C3uZIeGw3xxEm8xJmHngVSG', 'client1', 0),
(4, b'0', '2020-10-19', 'client2.test@live.fr', '2020-10-19 02:41:45', '$2a$10$MqyWhnnIxs5NyJLt8MgRV.gmvU/oTn9b9WQOpw.snfqY70pUJU5Aa', 'client2', 0),
(5, b'1', '2020-10-19', 'anonymous1.test@live.fr', '2020-10-19 02:41:45', '$2a$10$od9lMiFDRUjZIiHUfwZ.EeTBvalw87FKPjuTuxVUMSbUKU7CMM31C', 'anonymous1', 0),
(6, b'0', '2020-10-19', 'anonymous2.test@live.fr', '2020-10-19 02:41:45', '$2a$10$ljHD5yyDCapXW53kuWKK8uXR8AMmm.vUlWn9vksgmhUtVp/1Ab1M6', 'anonymous2', 0);

/* Insertion dans la table USER_ROLES */
INSERT INTO `USER_ROLES` (`USER_ID`, `ROLES`) 
VALUES 
(1, 0),
(2, 0),
(3, 1),
(4, 1),
(5, 2),
(6, 2);


-- Inserion dans la table T_USERS
/*
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (1, b'1', '2020-10-19', 'admin1.test@live.fr', '2020-10-19 02:41:45', '$2a$10$uk1lLoeMvFhGWEquR3sqAONrmwbgc4/6J.l5gDt/S7iKVRuV42202', 'admin1', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (2, b'0', '2020-10-19', 'admin2.test@live.fr', '2020-10-19 02:41:45', '$2a$10$4A7ZAl5CLZgofMk4T2xxOuvuHIX9etduxjd0gNJIlkUwjzRvJkOme', 'admin2', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (3, b'1', '2020-10-19', 'client1.test@live.fr', '2020-10-19 02:41:45', '$2a$10$UVLZvESy36fbafC9Ta5S.OZdLP4bm5C3uZIeGw3xxEm8xJmHngVSG', 'client1', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (4, b'0', '2020-10-19', 'client2.test@live.fr', '2020-10-19 02:41:45', '$2a$10$MqyWhnnIxs5NyJLt8MgRV.gmvU/oTn9b9WQOpw.snfqY70pUJU5Aa', 'client2', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (5, b'1', '2020-10-19', 'anonymous1.test@live.fr', '2020-10-19 02:41:45', '$2a$10$od9lMiFDRUjZIiHUfwZ.EeTBvalw87FKPjuTuxVUMSbUKU7CMM31C', 'anonymous1', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (6, b'0', '2020-10-19', 'anonymous2.test@live.fr', '2020-10-19 02:41:45', '$2a$10$ljHD5yyDCapXW53kuWKK8uXR8AMmm.vUlWn9vksgmhUtVp/1Ab1M6', 'anonymous2', 1);
*/

-- Insertion dans la table USER_ROLES
/* 
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (1, 0);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (2, 0);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (3, 1);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (4, 1);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (5, 2);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (6, 2);
*/