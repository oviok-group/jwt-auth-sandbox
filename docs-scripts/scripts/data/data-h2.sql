/* Insertion dans la table T_USERS */
INSERT INTO T_USERS(ID, USER_ACTIVE, DATE_CREATION, EMAIL, HORODATAGE, USER_PASSWORD, LOGIN, OPTLOCK)
VALUES 
(1,'TRUE','2020-10-19','admin1.test@live.fr','2020-10-19 05:17:49.797919','$2a$10$wVqgPSuRCMORtqLP4C60y.ZadSHOYZV2DxZKE1GWZv/sxC9UyThQG','admin1',0),
(2,'FALSE','2020-10-19','admin2.test@live.fr','2020-10-19 05:17:49.951911','$2a$10$JW5pb2oZ./CEKR7kfL2fVONMorxpDOEWwcNGeV1mBscFTEFy2PNoW','admin2',0),
(3,'TRUE','2020-10-19','client1.test@live.fr','2020-10-19 05:17:50.11691','$2a$10$DjYqVoitT4q5ZaXEF2Jb0u2l3tMzA7V/VlkPJXP3ZV5vUZ2jKIfuy','client1',0),
(4,'FALSE','2020-10-19','client2.test@live.fr','2020-10-19 05:17:50.275913','$2a$10$RyD3Q8NdVIZ2iKQDArNlceo.s2E1/KVAUx2YCSvV7GUCDV/Q4MK5y','client2',0),
(5,'TRUE','2020-10-19','anonymous1.test@live.fr','2020-10-19 05:17:50.489914','$2a$10$kT/FWii3ehz5/atHmXKR/.pjSm9VjC4n.GGEiOGNg5iIbK2NnQRUu','anonymous1',0),
(6,'FALSE','2020-10-19','anonymous2.test@live.fr','2020-10-19 05:17:50.692919','$2a$10$Jb7ZNBkw6q5.efW9DeoS6OuP8qAS1kwdelj3pOl3/KW6HiJ6BUYcS','anonymous2',0);

/* Insertion dans la table T_USERS */
INSERT INTO USER_ROLES(USER_ID, ROLES)
VALUES 
(1,0),
(2,0),
(3,1),
(4,1),
(5,2),
(6,2);

