/* Inserion dans la table T_USERS */
INSERT INTO `T_USERS` (`ID`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `OPTLOCK`) 
VALUES 
(1, b'1', '2020-10-18', 'admin1.test@live.fr', '2020-10-18 19:21:23', '$2a$10$pXpQ/X5.SpCW4WnBmlCEUOhiRroa.c3uDkvZvktL95NDMIO5WV95e', 'admin1', 0),
(2, b'0', '2020-10-18', 'admin2.test@live.fr', '2020-10-18 19:21:24', '$2a$10$v2jyVzdCN9B8kJ0cAPszdOVvnDokqOfSvM2L.apxMLLEEwqikGeYK', 'admin2', 0),
(3, b'1', '2020-10-18', 'client1.test@live.fr', '2020-10-18 19:21:24', '$2a$10$sUtaOrZIE3RmI5QKwRXhuepvmk8yP0dyU8AjOYHtFYbp3DNaVeNNC', 'client1', 0),
(4, b'0', '2020-10-18', 'client2.test@live.fr', '2020-10-18 19:21:24', '$2a$10$WyteSVYB6QHmLGPjB9VJ2utrGVXTFgwodXJlVZIvr7HdH85QKz2CS', 'client2', 0),
(5, b'1', '2020-10-18', 'anonymous1.test@live.fr', '2020-10-18 19:21:24', '$2a$10$2ZVcOoUI8oxrgjiDEE6u4eIUE1PV5cnwInSS0DgZ.JJnEf.ka3n1K', 'anonymous1', 0),
(6, b'0', '2020-10-18', 'anonymous2.test@live.fr', '2020-10-18 19:21:24', '$2a$10$05sZTVrXp9UenS1OniRhyO2lU9YwCmHU3hM8eH/h0epM4oUWJXZeW', 'anonymous2', 0);

/* Insertion dans la table USER_ROLES */
INSERT INTO `USER_ROLES` (`USER_ID`, `ROLES`)
VALUES 
(1, 0),
(2, 0),
(3, 1),
(4, 1),
(5, 2),
(6, 2);



-- Inserion dans la table T_USERS
/*  
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (1, b'1', '2020-10-18', 'admin1.test@live.fr', '2020-10-18 19:21:23', '$2a$10$pXpQ/X5.SpCW4WnBmlCEUOhiRroa.c3uDkvZvktL95NDMIO5WV95e', 'admin1', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (2, b'0', '2020-10-18', 'admin2.test@live.fr', '2020-10-18 19:21:24', '$2a$10$v2jyVzdCN9B8kJ0cAPszdOVvnDokqOfSvM2L.apxMLLEEwqikGeYK', 'admin2', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (3, b'1', '2020-10-18', 'client1.test@live.fr', '2020-10-18 19:21:24', '$2a$10$sUtaOrZIE3RmI5QKwRXhuepvmk8yP0dyU8AjOYHtFYbp3DNaVeNNC', 'client1', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (4, b'0', '2020-10-18', 'client2.test@live.fr', '2020-10-18 19:21:24', '$2a$10$WyteSVYB6QHmLGPjB9VJ2utrGVXTFgwodXJlVZIvr7HdH85QKz2CS', 'client2', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (5, b'1', '2020-10-18', 'anonymous1.test@live.fr', '2020-10-18 19:21:24', '$2a$10$2ZVcOoUI8oxrgjiDEE6u4eIUE1PV5cnwInSS0DgZ.JJnEf.ka3n1K', 'anonymous1', 1);
INSERT INTO `t_users` (`id`, `USER_ACTIVE`, `DATE_CREATION`, `EMAIL`, `HORODATAGE`, `USER_PASSWORD`, `LOGIN`, `VERSION`) VALUES (6, b'0', '2020-10-18', 'anonymous2.test@live.fr', '2020-10-18 19:21:24', '$2a$10$05sZTVrXp9UenS1OniRhyO2lU9YwCmHU3hM8eH/h0epM4oUWJXZeW', 'anonymous2', 1);
*/ 

-- Insertion dans la table USER_ROLES
/*  
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (1, 0);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (2, 0);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (3, 1);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (4, 1);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (5, 2);
INSERT INTO `user_roles` (`User_id`, `roles`) VALUES (6, 2);
*/ 