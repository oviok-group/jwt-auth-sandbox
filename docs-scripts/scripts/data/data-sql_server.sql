/* Insertion dans la table T_USERS */
GO

INSERT INTO T_USERS (ID,USER_ACTIVE,DATE_CREATION,EMAIL,HORODATAGE,USER_PASSWORD,LOGIN,OPTLOCK) 
VALUES
(1,'1','2020-10-19','admin1.test@live.fr','2020-10-19 07:09:27.8020796','$2a$10$IYwDfBEAfMR8TSQQHlQZlOX./Qzjf5H6Sb45vwIHQ4.UOdxQIHVUy','admin1',0),
(2,'0','2020-10-19','admin2.test@live.fr','2020-10-19 07:09:27.9660806','$2a$10$DIZKCVV9vSgmXCzcfAWk/eBqf1FXaLWyyqpowsPpF1qptuI7bE9tm','admin2',0),
(3,'1','2020-10-19','client1.test@live.fr','2020-10-19 07:09:28.1250748','$2a$10$JxuAzH8ZuxqVe2tJdk.ZRuy9ZaC2afSAbxFX0M6y6EdKQdakrlwpy','client1',0),
(4,'0','2020-10-19','client2.test@live.fr','2020-10-19 07:09:28.2940747','$2a$10$1V3JsTiMHd0IOLQU1EbZq.UW.CJsjxiFJkSquQo0zjAih0Fj0jpky','client2',0),
(5,'1','2020-10-19','anonymous1.test@live.fr','2020-10-19 07:09:28.4540775','$2a$10$bS.Y5QlKQ3qKnup4Vd8CpeiI7ldTbzL8IWtzHOTSujsYgvYC2G/jO','anonymous1',0),
(6,'0','2020-10-19','anonymous2.test@live.fr','2020-10-19 07:09:28.6100768','$2a$10$BfSFAHlubYa6QV1DhfznZeDpkPinH6QgT7ZQTOPD34ein7uE7Wrwa','anonymous2',0)
GO

/* Insertion dans la table USER_ROLES */
GO

INSERT INTO USER_ROLES (USER_ID,ROLES) 
VALUES
(1,0),
(2,0),
(3,1),
(4,1),
(5,2),
(6,2)  
GO           
           
/* 
USE [jwtauth]
GO

INSERT INTO [dbo].[T_USERS]
           ([id]
           ,[USER_ACTIVE]
           ,[DATE_CREATION]
           ,[EMAIL]
           ,[HORODATAGE]
           ,[USER_PASSWORD]
           ,[LOGIN]
           ,[VERSION])
     VALUES
           (<id, bigint,>
           ,<USER_ACTIVE, bit,>
           ,<DATE_CREATION, date,>
           ,<EMAIL, varchar(25),>
           ,<HORODATAGE, datetime2(7),>
           ,<USER_PASSWORD, varchar(255),>
           ,<LOGIN, varchar(80),>
           ,<VERSION, int,>)
GO
*/

/*  
USE [jwtauth]
GO

INSERT INTO [dbo].[User_roles]
           ([User_id]
           ,[roles])
     VALUES
           (<User_id, bigint,>
           ,<roles, int,>)
GO
*/