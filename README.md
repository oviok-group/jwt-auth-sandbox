# jwt-auth-sandbox

API authentification avec JWT Spring Security et perisistance avec Jpa Hibernate.

**NB** : Tout ce qui va suivre dans le cadre de cette réalisation ne concerne que le backend (l'API serveur). Des outils comme **_Postman_**,**_Swagger UI_** ou **_curl_** permettront de
tester facilement les fonctionnalités embarquées par l'application.

![](https://img.shields.io/badge/build-success-brightgreen.svg)

## 1- Stack Technique
Les élements de l'environnement technique de réalisation de ce besoin sont présentés selon les points ci-dessous :
### 1-1 Technologies
Une liste non exhaustive des techonologioes embarquées par l'application:

![](https://img.shields.io/badge/Java-✓-blue.svg)
![](https://img.shields.io/badge/Maven-✓-blue.svg)
![](https://img.shields.io/badge/Spring_boot-✓-blue.svg)
![](https://img.shields.io/badge/Spring_security-✓-blue.svg)
![](https://img.shields.io/badge/jwt-✓-blue.svg)
![](https://img.shields.io/badge/Jpa-✓-blue.svg)
![](https://img.shields.io/badge/Hibernate-✓-blue.svg)
![](https://img.shields.io/badge/Mariadb-✓-blue.svg)
![](https://img.shields.io/badge/Docker-✓-blue.svg)
![](https://img.shields.io/badge/Swagger-✓-blue.svg)
![](https://img.shields.io/badge/Open_Api_3-✓-blue.svg)
![](https://img.shields.io/badge/JavaMailSender-✓-blue.svg)

### 1-2 Catégorisation des Technologies
Le tableau ci-dessous presente les technologies utilisées par catégorie :

|Catégorie|Description|Technologie Utilisée
|---|---|---
|**_Langage Java_**|_Décrire les actions à réaliser par lapplication_|_version 11 de JDK_
|**_Build_**|_Gestion du cycle de vie/Constructions d'archive et container_|_Maven 3, Docker_
|**_Base de données_**|_Structurer et stocker les informations pour les traitements. Scritps founis pour plusieurs type de SGBD_|_H2, MariaDB, MysSQL, Oracle, SQL Server, Postgresql_ 
|**_ORM-DAO_**|_Mapping objets du modèle/Couche abstraction accès aux données_|_spring-data-jpa (JPA/Hibernate)_
|**_Intégration_**|_Relier les différentes composantes avec l'éco-système Spring_|_Spring, Spring Boot_
|**_Messageire_**|_Création et envoie de messages_|_spring-boot-starter-mail, JavaMailSender_
|**_Sécurité_**|_Sécurisation accèes aux ressources et des échanges entre client et serveur_|_Spring Security, JWT_
|**_Tests_**|_Tester unitairement, tests d'intégration, fonctionnels_|_Spring Boot Test, Junit, librairies de Mock, Postman, Swagger UI_

## 2 -Expression de besoins et exigences

### 2-1 Expression de besoins

Le besoin exprimé est de créer un SI permettant de gérer l'authentification et l'autorisation

### 2-2 Les exigences
Les exigences sont principalement de deux ordres dans le cadre de cette réalisation, à savoir :
- **les exigences fonctionnelles** : elles concernent les besoins metiers embarqués ou implémentés. Ce sont principalement

|Type exigence|Description
|---|---
|Processus de gestion du compte de l'utilisateur courant|_Enregister, activer, rechercher, mettre à jour, envoyer un message_
|Processus de gestion des utilisateurs|_Créer, rechercher, mettre à jour, liste utilisateurs, listes rôle_
|Processus de gestion Authentification et Autorisation|_Gérer les demandes d'authentification et d'autorisation, créer le jeton JWT, Valider le jeton JWT_
|Processus de gestion de Renouvellement de jetons JWT|_Gérer l'expiration des jetons, créer le jeton de renouvellement, rafraîchir/mettre à jour les informations de l'utilisateur_
|Processus de gestion des mails|_Créer, Envoyer des messages_
|Processus de gestion du mécanisme de planification de tâches|_Créer CRON, planifier les opérations à exécuter_

- **les exigences non fonctionnelles** : beaucoup plus d'ordre technique, elles concourent également au bon fonctionnement de l'application. Il s'agit principalement de :
	- la gestion des propriétés 
	- la gestion des logs 
	- la gestion des erreur/exception 
	- la gestion des accès aux informations en base de données 

### 2-3 Les cas d'utilisation
D'un point de vue macroscopique, les cas d'utilisation sont présentés par le diagramme ci-dessous.
![Diagramme des cas d'utlisation](./docs-scripts/images/use_case_api_auth_jwt.png "Use case Serveur API Auth JWT")


## 3- Le fonctionnement global
### 3-1 Présentation de  JWT
En résumé, la technologie JWT (JSON Web Tokens) et **une authentification stateless basée sur l'échange de token** (jeton) permettant d'assurer les échanges d'informations de manière sécurisée entre le client et le serveur.
Le token contient les informations suffisantes pour :
- **identifer** le client : vérifier que le token est bien celui qui a été transmis lors de l'authentification
- **autoriser** le client : vérifier que le token contient bien les droits du client

### 3-2 Fonctionnement du processus d'Authentification et Autorisation de l'utilisateur
Le fonctionnement global du processus de gestion de l'identification et autorisation de l'utiissateur, mis en en place de  au tavers de cette application
est fourni par le diagramme de séquences ci-dessous. Il fournit une vue macroscopique des différents éléments qui seront implémentés et qui concourent au bon
fonctionnement de ce processus.
![Diagramme de séquences](./docs-scripts/images/authentification_autorisation_jwt.png "Authentification/Autorisation : Création et Validation de jeton JWT!")

### 3-2 Fonctionnement global du processus de Renouvellement du jeton JWT

TODO

## 4-  Spécifications techniques
Il s'agit de la présentation des éléments qui ont permis (ou permettent) d'implémenter les exigence aussi bien focntionnelles que non.

### 4-1 Le modèle de données
Pour le bon fonctionnement de l'application des informations sont stockées en base de données, notamment **les données de l'utilisateur et ses rôles**. Le diagramme de classes ci-dessous fournit
une représentation schématique des objts de mapping des tables stockant ces informations dans la base de données.
![Diagramme de Classes](./docs-scripts/images/model_de_donnees_auth_jwt.png "Modèle de données")

### 4-2 La gestion des propriétés de l'application
- **Les propriétés d'accès aux ressources de la base de données**
Les propriétés indispensables au bon fonctionnement de l'application sont **externalisées** dans le fichier "_jwt-auth-sandbox-application.properties_" situé dans le packages des ressources de l'applicaion.
Les éléments contenus dans le fchier sont lus au démarrage par Spring pour alimenter les différents composants qui en ont besoin. Le diagramme de classes ci-dessous permet de schématiser les 
objets embarqués par l'application pour véhiculer ces informations.

![Diagramme de Classes](./docs-scripts/images/model_gestion_proprietes_auth_jwt.png "Gestion des propriétés")

- **Les autres propriétés** 
Celles-ci concernent principalement les proprités externaliséés pour Swagger, JWT, 

TODO