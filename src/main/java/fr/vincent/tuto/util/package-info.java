/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 28 oct. 2020
 * Heure de création : 17:41:29
 * Package : fr.vincent.tuto.util
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Utilities forJWT authnetication App.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.util;
