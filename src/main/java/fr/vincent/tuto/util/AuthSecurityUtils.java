/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AuthSecurityUtils.java
 * Date de création : 28 oct. 2020
 * Heure de création : 17:45:40
 * Package : fr.vincent.tuto.util
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.util;

import java.util.Optional;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import fr.vincent.tuto.enumeration.RoleNameEnum;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;

/**
 * App Spring Security Utilities.
 * <p>
 * <ul>
 * <li>Get current user login</li>
 * <li>Get current user JWT token</li>
 * <li>Check if user is authenticated</li>
 * <li>Check if user has specific role</li>
 * <li>Checks if authorization header contains valid authorization</li>
 * <li>Get's request and extract the bearer token.</li>
 * <li>Retrieve authentication roles in string stream.</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
public final class AuthSecurityUtils
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthSecurityUtils.class);

    /**
     * Private constructor for utilities class.
     */
    private AuthSecurityUtils()
    {
        //
    }

    /**
     * Get the login of the current user.
     * 
     * @return the login of the current user.
     */
    public static Optional<String> getCurrentUsername()
    {
        LOGGER.info("[getCurrentUsername] - Get the login of the current user.");

        //
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())//
        .map(authentication -> {
            if (UserDetails.class.isAssignableFrom(authentication.getPrincipal().getClass()))
            {
                final UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                return springSecurityUser.getUsername();
            }
            else if (String.class.isAssignableFrom(authentication.getPrincipal().getClass()))
            {
                return (String) authentication.getPrincipal();
            }
            return null;
        });
    }

    /**
     * Get the JWT token for the current user.
     * 
     * @return the JWT token of the current user
     */
    public static Optional<String> getCurrentUserJWTToken()
    {
        LOGGER.info("[getCurrentUserJWTToken] - Get the JWT token for the current user.");

        final SecurityContext securityContext = SecurityContextHolder.getContext();

        return Optional.ofNullable(securityContext.getAuthentication())//
        .filter(authentication -> String.class.isAssignableFrom(authentication.getCredentials().getClass()))//
        .map(authentication -> (String) authentication.getCredentials());
    }

    /**
     * Check if the user is authenticated.
     * 
     * @return true if the user is authenticated, false otherwise.
     */
    public static Boolean isAuthenticated()
    {
        LOGGER.info("[isAuthenticated] - Check if the user is authenticated.");

        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && getRoles(authentication).noneMatch(RoleNameEnum.ROLE_ANONYMOUS.name()::equals);
    }

    /**
     * If the current user has a specific authority (security role).
     * 
     * @param pRole the authority to check.
     * @return true if the current user has the role, false otherwise.
     */
    public static Boolean isCurrentUserInRole(final String pRole)
    {
        LOGGER.info("[isCurrentUserInRole] - If the current user has a specific authority (security role).");

        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && getRoles(authentication).anyMatch(pRole::equals);
    }

    /**
     * Checks if authorization header contains valid authorization.
     * 
     * @param pBearerToken authorization header
     * @return true if contains Bearer false otherwise
     */
    public static Boolean isValidAuthorizationHeader(final String pBearerToken, final String pTokenPrefix)
    {
        LOGGER.debug("[isValidAuthorizationHeader] - check if header contains {}", pTokenPrefix);

        //
        return StringUtils.isNotBlank(pBearerToken) && StringUtils.contains(pBearerToken, StringUtils.SPACE)
        && StringUtils.startsWith(pBearerToken, pTokenPrefix);
    }

    /**
     * Get's request and extract the bearer token.
     * <p>
     * JWT Token is in the form "Bearer token". Remove Bearer word and get
     * only the Token.
     * 
     * @param pRequest             the HTTP Servlet request
     * @param pAuthorizationHeader the authorization header
     * @param pTokenPrefix         the token prefix
     * @return
     */
    public static Optional<String> extractJwtFromRequest(final HttpServletRequest pRequest,
    final String pAuthorizationHeader,
    final String pTokenPrefix)
    {
        LOGGER.info("[tokenHeaderResolver] - Get's request and extract the bearer token.");

        final String bearerToken = pRequest.getHeader(pAuthorizationHeader);
        return Optional.ofNullable(pAuthorizationHeader)//
        .filter(token -> StringUtils.isNotBlank(bearerToken) && isValidAuthorizationHeader(bearerToken,
        pTokenPrefix))//
        .map(token -> StringUtils.substring(bearerToken, pTokenPrefix.length(), bearerToken.length()).trim());
    }

    /**
     * Check not expired JWT token exception.
     * 
     * @param ex the exception. to check.
     * @return true if not expired exception false otherwise.
     */
    public static boolean isNotExpiredException(final RuntimeException ex)
    {
        return SignatureException.class.isAssignableFrom(ex.getClass()) || MalformedJwtException.class.isAssignableFrom(
        ex.getClass()) || UnsupportedJwtException.class.isAssignableFrom(ex.getClass());
    }


    /**
     * Retrieve authentication roles in string stream.
     * 
     * @param pAuthentication the authentication
     * @return roles stream string
     */
    private static Stream<String> getRoles(final Authentication pAuthentication)
    {
        return pAuthentication.getAuthorities()//
        .stream()//
        .map(GrantedAuthority::getAuthority);
    }
}
