/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AuthAppConstant.java
 * Date de création : 30 oct. 2020
 * Heure de création : 09:56:05
 * Package : fr.vincent.tuto.constant
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.constant;

import java.util.Set;

import org.springframework.http.MediaType;

import com.google.common.collect.Sets;

/**
 * App constant.
 * 
 * @author Vincent Otchoun
 */
public final class AuthAppConstant
{
    // Cross-Origin - CORS constants
    public static final String ALOW_ORIGIN = "Access-Control-Allow-Origin";
    public static final String ORIGIN = "*";
    public static final String ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    public static final String CREDENTIALS = "true";
    public static final String ALLOW_METHODS = "Access-Control-Allow-Methods";

    // DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT
    public static final String METHODS = "{GET; POST; PUT; DELETE; OPTION; PATCH; HEAD}";
    public static final String ALLOW_AGE = "Access-Control-Max-Age";
    public static final String MAX_AGE = "3600";
    public static final String ALLOW_HEADERS = "Access-Control-Allow-Headers";

    // public static final String HEADERS = "Origin, Content-Type, Accept, X-Requested-With,Remember-me";
    public static final String HEADERS = "{Origin; Content-Type; Accept; X-Requested-With; Remember-me; Authorization}";

    // App profiles
    public static final String DEFAULT_PROFILE = "spring.profiles.default";
    public static final String DEVELOPMENT_PROFILE = "dev";
    public static final String TEST_PROFILE = "test";
    public static final String PRODUCTION_PROFILE = "prod";
    public static final String CLOUD_PROFILE = "cloud";
    public static final String SWAGGER_PROFILE = "swagger";
    public static final String K8S_PROFILE = "k8s";

    public static final String HTTP = "http";
    public static final String HTTPS = "https";
    public static final String SECURITY_KEY = "server.ssl.key-store";
    public static final String SERVER_PORT = "server.port";
    public static final String CONTEXT_PATH = "server.servlet.context-path";
    public static final String DEFAULT_CONTEXT_PATH = "/";
    public static final String HOST_ADRESS = "localhost";
    public static final String APP_NAME = "spring.application.name";

    // Swagger
    public static final String[] SWAGGER_URL_PATHS = new String[] { "/swagger-ui/index.html", "/swagger-resources/**",
            "/v3/api-docs**", "/webjars/**" };

    // new String[] { "/jwt-auth-sandbox/swagger-ui/index.html",
    // "/swagger-ui/index.html", "/swagger-ui.html**", "/swagger-resources/**", "/v3/api-docs**",
    // "/webjars/springfox-swagger-ui/", "/v2/api-docs**", "/webjars/**" };

    public static final String AUTH_BAERER_TOKEN = "Bearer %token";
    public static final String AUTHORISATION = "Authorization";
    public static final String HEADER = "Header";

    public static final String DEFAULT_CONTROLLER_BASE_PACKAGE = "fr.vincent.tuto.controller";
    public static final String USER_CONTROLLER_BASE_PACKAGE = "fr.vincent.tuto.controller.user";
    public static final String ACCOUNT_CONTROLLER_BASE_PACKAGE = "fr.vincent.tuto.controller.account";

    public static final String DEFAULT_GROUPENAME = "votjwt-default-controller";
    public static final String USER_GROUPENAME = "votjwt-user-controller";
    public static final String ACCOUNT_GROUPENAME = "votjwt-account-controller";

    public static Set<String> mediaTypeSet = Sets.newHashSet();
    static
    {
        mediaTypeSet.add(MediaType.APPLICATION_JSON_VALUE);
    }

    //

    /**
     * Private constructor for utilities class.
     */
    private AuthAppConstant()
    {
        //
    }
}
