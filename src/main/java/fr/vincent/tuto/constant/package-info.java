/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 30 oct. 2020
 * Heure de création : 09:34:45
 * Package : fr.vincent.tuto.constant
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * @author Vincent Otchoun
 *
 */
package fr.vincent.tuto.constant;
