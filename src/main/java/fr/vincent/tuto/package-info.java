/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 13 oct. 2020
 * Heure de création : 14:39:41
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Basic package of application components
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto;
