/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : JwtAuthEnvAddressConfig.java
 * Date de création : 1 nov. 2020
 * Heure de création : 16:54:11
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.config;

import java.net.InetAddress;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * Recovery of hostname address and the deployment environment.
 * <p>
 * <ul>
 * <li>Retrieves the address of the host of the execution environment the application</li>
 * <li>Allows you to check if the host is local or not</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
@Configuration
public class JwtAuthEnvAddressConfig implements InitializingBean, DisposableBean
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthEnvAddressConfig.class);

    @Autowired
    private ConfigurableEnvironment environment;

    private String hostAddress = null;

    /**
     * Get's host address.
     * 
     * @return the host address.
     */
    public String getHostAddress()
    {
        LOGGER.info("[getHostAddress] - Get's host adress.");

        //
        return StringUtils.isNotBlank(this.hostAddress) && this.environment.getProperty(
        "votjwt.swagger-props.nom-machine-dev").trim().equals(this.hostAddress) ? this.hostAddress.trim() : null;
    }

    /**
     * Check if is localhost.
     * 
     * @return
     */
    public Boolean isLocalHost()
    {
        LOGGER.info("[isLocalHost] - Check's if is local adress.");

        final String localMachineName = this.environment.getProperty("votjwt.swagger-props.nom-machine-dev").trim();
        return StringUtils.isNotBlank(localMachineName) && StringUtils.isNotBlank(this.getHostAddress()) && this
        .getHostAddress().contains(localMachineName)
        ? Boolean.TRUE
        : Boolean.FALSE;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        this.hostAddress = InetAddress.getLocalHost().getHostName();
    }

    @Override
    public void destroy() throws Exception
    {
        this.environment = null;
        this.hostAddress = null;
    }
}
