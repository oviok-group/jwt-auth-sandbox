/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : JwtAuthSandboxBaseConfig.java
 * Date de création : 13 oct. 2020
 * Heure de création : 15:08:56
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.config;

import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Application base configuration that provides :
 * <ul>
 * <li>App base package to scan for annotated components reading (Autodiscovery)</li>
 * <li>App properties files localization</li>
 * <li>App persistent object base package</li>
 * <li>App data access object (DAO) base package</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
@Configuration
@ComponentScan(basePackages = "fr.vincent.tuto")
@PropertySources(value = { @PropertySource(value = {
        "classpath:jwt-auth-sandbox-application.properties" }, ignoreResourceNotFound = true),
        @PropertySource(value =
        { "classpath:jwt-auth-sandbox-auth-swagger.properties" }, ignoreResourceNotFound = true) })
@EntityScan("fr.vincent.tuto.model")
@EnableJpaRepositories(basePackages = "fr.vincent.tuto.repository", entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "transactionManager")
public class JwtAuthSandboxBaseConfig
{
    /*
     * Hash element configurations (process of generating a string, from a given message using a mathematical function
     * known as a cryptographic hash function) of the password
     */
    @Bean(name = "bCryptPasswordEncoder")
    public BCryptPasswordEncoder bCryptPasswordEncoder()
    {
        return new BCryptPasswordEncoder();
    }

    /*
     * API Jackson producer bean config
     */
    @Bean(name = "jackson2ObjectMapperBuilder")
    public Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder()
    {
        //
        return new Jackson2ObjectMapperBuilder()
        {
            //
            @Override
            public void configure(final ObjectMapper pObjectMapper)
            {
                //
                super.configure(pObjectMapper);

                pObjectMapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE);
                pObjectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
                pObjectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                pObjectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                pObjectMapper.configure(SerializationFeature.INDENT_OUTPUT, false);
                pObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
                pObjectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
                pObjectMapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true);
                pObjectMapper.configure(SerializationFeature.WRAP_EXCEPTIONS, true);
            }
        };
    }

    /**
     * Object mapper for JSON format build bean configuratuion.
     *
     * @return objectMapper instance
     */
    @Bean(name = "objectMapper")
    @Primary
    public ObjectMapper objectMapper()
    {
        return this.jackson2ObjectMapperBuilder().build();
    }

    // custom property maps and converters to handle the transfer from data transfer object to entity and vice versa
    @Bean(name = "modelMapper")
    @Primary
    public ModelMapper modelMapper()
    {
        return new ModelMapper();
    }
}
