/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AppEnvLogStartConfig.java
 * Date de création : 1 nov. 2020
 * Heure de création : 08:10:18
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.config;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

import com.google.common.collect.Maps;

import fr.vincent.tuto.constant.AuthAppConstant;

/**
 * App execution environment and logs provider on startup.
 * 
 * @author Vincent Otchoun
 */
@Configuration
public class AppEnvLogStartConfig implements InitializingBean
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(AppEnvLogStartConfig.class);

    @Autowired
    private ConfigurableEnvironment environment;

    /**
     * @param pEnvironment
     */
    public static void logStartUp(final Environment pEnvironment)
    {
        // protocol process
        String protocol = AuthAppConstant.HTTP;
        final String securityKey = pEnvironment.getProperty(AuthAppConstant.SECURITY_KEY);
        if (StringUtils.isNotBlank(securityKey))
        {
            protocol = AuthAppConstant.HTTPS;
        }

        // port process
        final String portServer = pEnvironment.getProperty(AuthAppConstant.SERVER_PORT);
        String contextPath = pEnvironment.getProperty(AuthAppConstant.CONTEXT_PATH);
        if (StringUtils.isBlank(contextPath))
        {
            contextPath = AuthAppConstant.DEFAULT_CONTEXT_PATH;
        }

        // host process
        String hostAdresse = null;
        try
        {
            hostAdresse = InetAddress.getLocalHost().getHostAddress();
        }
        catch (UnknownHostException e)
        {
            hostAdresse = AuthAppConstant.HOST_ADRESS;
            LOGGER.error(pEnvironment.getProperty("votjwt.hostname.error.message"));
        }

        // app name process
        final String appLog = pEnvironment.getProperty("votjwt.app.log.message");
        final String appName = pEnvironment.getProperty(AuthAppConstant.APP_NAME);
        LOGGER.info(appLog, appName, protocol, portServer, contextPath, protocol,
        hostAdresse, portServer, contextPath, pEnvironment.getActiveProfiles());
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        //
        final Collection<String> activeProfiles = Arrays.asList(this.environment.getActiveProfiles());
        if (activeProfiles.contains(AuthAppConstant.DEVELOPMENT_PROFILE) && activeProfiles.contains(
        AuthAppConstant.PRODUCTION_PROFILE))
        {
            LOGGER.error(this.environment.getProperty("votjwt.dev-prod.error.message"));
            return;
        }

        if (activeProfiles.contains(AuthAppConstant.DEVELOPMENT_PROFILE) && activeProfiles.contains(
        AuthAppConstant.CLOUD_PROFILE))
        {
            LOGGER.error(this.environment.getProperty("votjwt.dev-cloud.error.message"));
            return;
        }
    }

    /**
     * @param pApplication
     */
    public static void addDefaultProfile(final SpringApplication pApplication)
    {
        final Map<String, Object> defaultProperties = Maps.newHashMap();
        defaultProperties.put(AuthAppConstant.DEFAULT_PROFILE, AuthAppConstant.DEVELOPMENT_PROFILE);
        pApplication.setDefaultProperties(defaultProperties);
    }
}
