/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : DatabaseAccesConfig.java
 * Date de création : 16 oct. 2020
 * Heure de création : 17:48:36
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.config;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.jdbc.DataSourceInitializationMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.SharedEntityManagerBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.google.common.annotations.VisibleForTesting;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.HikariPool;

import fr.vincent.tuto.exception.TechnicalAppError;
import fr.vincent.tuto.service.props.DatabasePropsService;

/**
 * Abstract layer for database informations access bean's configuration. It provide :
 * <ul>
 * <li>The HikariCP pooled DataSource : {@link HikariDataSource}</li>
 * <li>HikariCP pool connection : {@link HikariPool}</li>
 * <li>JPA {@link EntityManagerFactory} container bootstrap</li>
 * <li>Spring's imperative transaction infrastructure manager : {@link JpaTransactionManager}</li>
 * <li>Shared JPA {@link EntityManager} reference for a given {@link EntityManagerFactory}</li>
 * <li>Plug in vendor-specific behavior into Spring's EntityManagerFactory creator :
 * {@link HibernateJpaVendorAdapter}</li>
 * <li>Jpa Diaect : {@link HibernateJpaDialect}</li>
 * <li>Persistence exception translation : {@link PersistenceExceptionTranslationPostProcessor}</li>
 * <li>vendor-specific error codes analysis : {@link SQLErrorCodeSQLExceptionTranslator}</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
@Configuration
@Import(JwtAuthSandboxBaseConfig.class)
@EnableTransactionManagement
public class DatabaseAccesConfig
{
    // App properties loading bean injection
    @Autowired
    private DatabasePropsService databasePropsService;

    /**
     * Create Hikari pool DataSource bean for data accees
     * 
     * @return the bean
     * @throws TechnicalAppError exception when a problem occurs
     */
    @Primary
    @Bean(name = "dataSource", destroyMethod = "close")
    public HikariDataSource dataSource() throws TechnicalAppError
    {
        //
        try
        {
            // les propréités de la datasourece à construire
            final DataSourceProperties sourceProperties = new DataSourceProperties();
            sourceProperties.setType(HikariDataSource.class);
            sourceProperties.setDriverClassName(this.databasePropsService.getDataSourceProps().getDriverClassName()
            .trim());
            sourceProperties.setUrl(this.databasePropsService.getDataSourceProps().getJdbcUrl().trim());
            sourceProperties.setUsername(this.databasePropsService.getDataSourceProps().getUserName().trim());
            sourceProperties.setPassword(this.databasePropsService.getDataSourceProps().getPassword().trim());
            sourceProperties.setPlatform(this.databasePropsService.getDataSourceProps().getPlatform().trim());

            /*
             * XXX : Add properties to load initial data: run DDL and DML scripts (non-programmatic approach)
             * 1°) - intialization-mode : ALWAYS, EMBEDDED, NEVER
             * 2°) - schema loader
             * 3°) - data loader
             */

            // intialization-mode
            final String intialMode = this.databasePropsService.getDataSourceProps().getInitializationMode().trim();
            final String ddl = this.databasePropsService.getDataSourceProps().getInitSchema().trim();
            final String dml = this.databasePropsService.getDataSourceProps().getInitData().trim();
            final Boolean continueOnError = this.databasePropsService.getDataSourceProps().getInitContinueOnError();

            if (StringUtils.isNotBlank(intialMode))
            {
                sourceProperties.setInitializationMode(DataSourceInitializationMode.valueOf(intialMode.toUpperCase()));
            }

            if (StringUtils.isNotBlank(ddl))
            {
                sourceProperties.setSchema(Arrays.asList(ddl));
            }

            if (StringUtils.isNotBlank(dml))
            {
                sourceProperties.setData(Arrays.asList(dml));
            }

            if (null != continueOnError)
            {
                sourceProperties.setContinueOnError(continueOnError);
            }


            //
            sourceProperties.afterPropertiesSet();
            return (HikariDataSource) sourceProperties.initializeDataSourceBuilder().build();
        }
        catch (Exception e)
        {
            throw new TechnicalAppError("[dataSource] - Error during Hikari pool DataSource bean creation", e);
        }
    }

    /**
     * Create Hikari pool manager bean for data access
     * 
     * @return the bean
     */
    @Primary
    @Bean(name = "hikariPool")
    public HikariPool hikariPool()
    {
        //
        final HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setPoolName(this.databasePropsService.getHikariProps().getPoolName().trim());
        hikariConfig.setMaximumPoolSize(this.databasePropsService.getHikariProps().getMaximumPoolSize());
        hikariConfig.setMinimumIdle(this.databasePropsService.getHikariProps().getMinimumIdle());
        hikariConfig.setIdleTimeout(this.databasePropsService.getHikariProps().getIdleTimeout());
        hikariConfig.setMaxLifetime(this.databasePropsService.getHikariProps().getMaxLifetime());
        hikariConfig.setConnectionTimeout(this.databasePropsService.getHikariProps().getConnectionTimeout());

        // DS Connexion
        hikariConfig.setJdbcUrl(this.databasePropsService.getDataSourceProps().getJdbcUrl().trim());
        hikariConfig.setUsername(this.databasePropsService.getDataSourceProps().getUserName().trim());
        hikariConfig.setPassword(this.databasePropsService.getDataSourceProps().getPassword().trim());

        // DatasourceProperties props/value
        final String prepareStmtsCache = this.databasePropsService.getDataSourceProps().getCachePrepareStatements()
        .trim();
        final String prepareStmtsCacheSize = this.databasePropsService.getDataSourceProps()
        .getPrepareStatementCacheSize().trim();
        final String cacheSqlLimit = this.databasePropsService.getDataSourceProps().getPrepareStatementCacheSqlLimit()
        .trim();
        final String useServerPreStmts = this.databasePropsService.getDataSourceProps().getUseServerPrepareStatements()
        .trim();

        hikariConfig.addDataSourceProperty(prepareStmtsCache, this.databasePropsService.getHikariProps()
        .getCachePrepareStatements());
        hikariConfig.addDataSourceProperty(prepareStmtsCacheSize, this.databasePropsService.getHikariProps()
        .getPrepareStatementCacheSize());
        hikariConfig.addDataSourceProperty(cacheSqlLimit, this.databasePropsService.getHikariProps()
        .getPrepareStatementCacheSqlLimit());
        hikariConfig.addDataSourceProperty(useServerPreStmts, this.databasePropsService.getHikariProps()
        .getUseServerPrepareStatements());

        return new HikariPool(hikariConfig);
    }

    /**
     * Create entity manager factory bean for data access
     * 
     * @return the bean
     * @throws TechnicalAppError exception when a problem occurs
     */
    @Primary
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws TechnicalAppError
    {
        //
        final String packageScan = this.databasePropsService.getDataSourceProps().getPackageToScan().trim();
        final String unitName = this.databasePropsService.getDataSourceProps().getPersistenceUnitName().trim();

        //
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(this.dataSource());
        emf.setPersistenceUnitName(unitName);
        emf.setPackagesToScan(packageScan);
        emf.setJpaVendorAdapter(this.hibernatJpaVendorAdapter());
        emf.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        emf.setJpaDialect(this.hibernatJpaDialect());
        emf.setJpaProperties(this.additionalProperties());

        //
        emf.afterPropertiesSet();
        return emf;
    }

    /**
     * Create entity shared bean for data access
     * 
     * @return the bean
     * @throws TechnicalAppError exception when a problem occurs
     */
    @Bean(name = "sharedEntityManager")
    public SharedEntityManagerBean sharedEntityManager() throws TechnicalAppError
    {
        final SharedEntityManagerBean sharedEntityManagerBean = new SharedEntityManagerBean();
        sharedEntityManagerBean.setEntityManagerFactory(entityManagerFactory().getObject());
        sharedEntityManagerBean.afterPropertiesSet();
        return new SharedEntityManagerBean();
    }

    /**
     * Create transaction management bean for data access
     * 
     * @return the bean
     */
    @Primary
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager()
    {
        //
        final JpaTransactionManager txManager = new JpaTransactionManager();

        txManager.setDataSource(this.dataSource());
        txManager.setEntityManagerFactory(entityManagerFactory().getObject());
        txManager.setJpaDialect(this.hibernatJpaDialect());
        txManager.setDefaultTimeout(TransactionDefinition.TIMEOUT_DEFAULT);
        txManager.afterPropertiesSet();
        return txManager;
    }

    /**
     * Create Hibernate vendor adapter bean for data access
     * 
     * @return the bean
     */
    @Primary
    @Bean(name = "hibernatJpaVendorAdapter")
    public JpaVendorAdapter hibernatJpaVendorAdapter()
    {
        //
        final String databasePlatform = this.databasePropsService.getJpaHibernateProps().getDatabaseName().trim();
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

        vendorAdapter.setDatabasePlatform(databasePlatform);
        vendorAdapter.setGenerateDdl(this.databasePropsService.getDataSourceProps().getGenerateDdl());
        vendorAdapter.setShowSql(this.databasePropsService.getJpaHibernateProps().getShowSql());
        vendorAdapter.setDatabase(Database.valueOf(databasePlatform));
        return vendorAdapter;
    }

    /**
     * Create Jpa dialect bean for data access
     * 
     * @return the bean
     */
    @Primary
    @Bean(name = "hibernatJpaDialect")
    public JpaDialect hibernatJpaDialect()
    {
        return new HibernateJpaDialect();
    }

    /**
     * Persistence exception translation post processor.
     *
     * @return the persistence exception translation post processor
     */
    @Primary
    @Bean(name = "exceptionTranslationPostProcessor")
    public PersistenceExceptionTranslationPostProcessor exceptionTranslationPostProcessor()
    {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    /**
     * Sql exception translator.
     * 
     * @return
     * @throws TechnicalAppError exception levée lorsque survient un problème
     */
    @Primary
    @Bean(name = "sqlExceptionTranslator")
    public SQLErrorCodeSQLExceptionTranslator sqlExceptionTranslator() throws TechnicalAppError
    {
        return new SQLErrorCodeSQLExceptionTranslator(this.dataSource());
    }

    /**
     * @return
     */
    public DatabasePropsService getDatabasePropsService()
    {
        return this.databasePropsService;
    }

    @VisibleForTesting
    public void setDatabasePropsService(DatabasePropsService databasePropsService)
    {
        this.databasePropsService = databasePropsService;
    }

    /**
     * Create addtional hibernate properties {@link Hashtable}
     * 
     * @return the additional properties
     */
    private Properties additionalProperties()
    {
        // Create object and put retrieve properties
        Properties properties = new Properties();

        //
        properties.put(AvailableSettings.HBM2DDL_AUTO, this.databasePropsService.getJpaHibernateProps().getDdlAuto()
        .trim());
        properties.put(AvailableSettings.DIALECT, this.databasePropsService.getJpaHibernateProps().getDialect().trim());
        properties.put(AvailableSettings.SHOW_SQL, this.databasePropsService.getJpaHibernateProps().getShowSql());
        properties.put(AvailableSettings.FORMAT_SQL, this.databasePropsService.getJpaHibernateProps().getFormatSql());
        properties.put(AvailableSettings.USE_SQL_COMMENTS, this.databasePropsService.getJpaHibernateProps()
        .getUseSqlComments());
        properties.put(AvailableSettings.ENABLE_LAZY_LOAD_NO_TRANS, this.databasePropsService.getJpaHibernateProps()
        .getEnableLazy());
        properties.put(AvailableSettings.USE_SECOND_LEVEL_CACHE, this.databasePropsService.getJpaHibernateProps()
        .getUseSecondLevelCache());
        properties.put(AvailableSettings.GENERATE_STATISTICS, this.databasePropsService.getJpaHibernateProps()
        .getGenerateStatistics());
        properties.put(AvailableSettings.USE_REFLECTION_OPTIMIZER, this.databasePropsService.getJpaHibernateProps()
        .getBytecodeUseReflectionOptimizer());
        return properties;
    }
}
