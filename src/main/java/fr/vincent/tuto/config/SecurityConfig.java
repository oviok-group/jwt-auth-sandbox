/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : SecurityConfig.java
 * Date de création : 1 nov. 2020
 * Heure de création : 01:01:25
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import fr.vincent.tuto.constant.AuthAppConstant;
import fr.vincent.tuto.filter.AuthTokenFilter;

/**
 * Web security configuration.
 * 
 * @author Vincent Otchoun
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
    //
    @Autowired
    private AuthTokenFilter authTokenFilter;

    //
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        // Disable CSRF (cross site request forgery)
        http.csrf().disable();
        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).disable();

        // No session will be created or used by spring security
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Entry points
        // http.authorizeRequests()//
        http.requestMatchers()//
        .antMatchers(AuthAppConstant.SWAGGER_URL_PATHS)//
        .and()//
        .authorizeRequests()//
        // .antMatchers("/users/signin").permitAll()//
        // .antMatchers("/users/signup").permitAll()//
        .antMatchers(AuthAppConstant.SWAGGER_URL_PATHS).permitAll()//
        .antMatchers("/h2-console/**/**").permitAll()
        // Disallow everything else..
        .anyRequest().authenticated();

        // Apply JWT
        http.addFilterBefore(this.authTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }

    //
    // @Override
    // public void configure(WebSecurity web) throws Exception
    // {
    // web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**")//
    // .antMatchers("/app/**/*.{js,html}")//
    // .antMatchers("/i18n/**")//
    // .antMatchers("/content/**")//
    // .antMatchers("/h2-console/**")//
    // .antMatchers("/swagger-ui/index.html")//
    // .antMatchers("/test/**");
    // }

    @Override
    public void configure(WebSecurity web) throws Exception
    {
        // Allow swagger to be accessed without authentication
        web.ignoring()//
        .antMatchers("/v2/api-docs")//
        .antMatchers("/v3/api-docs")//
        .antMatchers("/swagger-ui/index.html")//
        .antMatchers("/swagger-resources/**")//
        .antMatchers("/swagger-ui.html")//
        .antMatchers("/configuration/**")//
        .antMatchers("/webjars/**")//
        .antMatchers("/public")

        // Un-secure H2 Database (for testing purposes, H2 console shouldn't be unprotected in production)
        .and().ignoring().antMatchers("/h2-console/**/**");;
    }
}
