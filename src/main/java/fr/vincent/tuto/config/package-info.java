/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 13 oct. 2020
 * Heure de création : 14:52:18
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * App configuration bean's layer
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.config;
