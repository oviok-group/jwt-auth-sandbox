/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : SwaggerConfig.java
 * Date de création : 1 nov. 2020
 * Heure de création : 16:29:22
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.config;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import fr.vincent.tuto.constant.AuthAppConstant;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author Vincent Otchoun
 */
@Configuration
@EnableOpenApi
public class SwaggerConfig
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(SwaggerConfig.class);

    @Autowired
    private SwaggerAuthPropsService swaggerAuthPropsService;

    /**
     * Default controller docket.
     * 
     * @return
     */
    @Bean
    public Docket defaultControllerDocket()
    {
        LOGGER.info("[defaultControllerDocket] - Build default controller docket.");
        //
        return new Docket(DocumentationType.OAS_30)//
        .groupName(AuthAppConstant.DEFAULT_GROUPENAME)//
        .select()//
        // .apis(RequestHandlerSelectors.any())//
        .apis(RequestHandlerSelectors.basePackage(AuthAppConstant.DEFAULT_CONTROLLER_BASE_PACKAGE))//
        .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))//
        .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))//
        .apis(RequestHandlerSelectors.withMethodAnnotation(ApiResponses.class))//
        .paths(PathSelectors.any())//
        // .paths(PathSelectors.regex("/default**"))
        .build()//
        .apiInfo(this.metadata())//
        .produces(AuthAppConstant.mediaTypeSet)//
        .consumes(AuthAppConstant.mediaTypeSet)//
        .securitySchemes(Collections.singletonList(this.apiKey()))//
        .securityContexts(Collections.singletonList(securityContext()))//
        .directModelSubstitute(java.time.LocalDate.class, String.class)// Convenience rule builder that substitutes
        // LocalDate with String
        .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)// Convenience rule builder that substitutes
        // ZonedDateTime with Date
        .directModelSubstitute(java.time.LocalDateTime.class, Date.class)// Convenience rule builder that substitutes
        // LocalDateTime with Date
        .genericModelSubstitutes(ResponseEntity.class)// essayer avec Optional
        .genericModelSubstitutes(Optional.class)//
        ;
    }

    /**
     * User controller docket.
     * 
     * @return
     */
    @Bean
    public Docket userControllerDocket()
    {
        LOGGER.info("[userControllerDocket] - Build user controller docket.");
        //
        return new Docket(DocumentationType.OAS_30)//
        .groupName(AuthAppConstant.USER_GROUPENAME)//
        .select()//
        // .apis(RequestHandlerSelectors.any())//
        .apis(RequestHandlerSelectors.basePackage(AuthAppConstant.USER_CONTROLLER_BASE_PACKAGE))//
        .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))//
        .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))//
        .apis(RequestHandlerSelectors.withMethodAnnotation(ApiResponses.class))//
        .paths(PathSelectors.any())//
        // .paths(PathSelectors.regex("/user.*"))
        .build()//
        .apiInfo(this.metadata())//
        .produces(AuthAppConstant.mediaTypeSet)//
        .consumes(AuthAppConstant.mediaTypeSet)//
        .securitySchemes(Collections.singletonList(this.apiKey()))//
        .securityContexts(Collections.singletonList(securityContext()))//
        .directModelSubstitute(java.time.LocalDate.class, String.class)// Convenience rule builder that substitutes
        // LocalDate with String
        .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)// Convenience rule builder that substitutes
        // ZonedDateTime with Date
        .directModelSubstitute(java.time.LocalDateTime.class, Date.class)// Convenience rule builder that substitutes
        // LocalDateTime with Date
        .genericModelSubstitutes(ResponseEntity.class)// essayer avec Optional
        .genericModelSubstitutes(Optional.class)//
        ;
    }

    /**
     * Account controller docket.
     * 
     * @return
     */
    @Bean
    public Docket accountControllerDocket()
    {
        LOGGER.info("[accountControllerDocket] - Build account controller docket.");
        //
        return new Docket(DocumentationType.OAS_30)//
        .groupName(AuthAppConstant.ACCOUNT_GROUPENAME)//
        .select()//
        // .apis(RequestHandlerSelectors.any())//
        .apis(RequestHandlerSelectors.basePackage(AuthAppConstant.ACCOUNT_CONTROLLER_BASE_PACKAGE))//
        .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))// allows selection of RequestHandler's
        .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))//
        .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))//
        .apis(RequestHandlerSelectors.withMethodAnnotation(ApiResponses.class))//
        .paths(PathSelectors.any())//
        // .paths(PathSelectors.regex("/acoount.*"))
        .build()//
        .apiInfo(this.metadata())//
        .produces(AuthAppConstant.mediaTypeSet)//
        .consumes(AuthAppConstant.mediaTypeSet)//
        .securitySchemes(Collections.singletonList(this.apiKey()))//
        .securityContexts(Collections.singletonList(securityContext()))//
        .directModelSubstitute(java.time.LocalDate.class, String.class)// Convenience rule builder that substitutes
        // LocalDate with String
        .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)// Convenience rule builder that substitutes
        // ZonedDateTime with Date
        .directModelSubstitute(java.time.LocalDateTime.class, Date.class)// Convenience rule builder that substitutes
        // LocalDateTime with Date
        .genericModelSubstitutes(ResponseEntity.class)// essayer avec Optional
        .genericModelSubstitutes(Optional.class)//
        ;
    }

    public SwaggerAuthPropsService getSwaggerAuthPropsService()
    {
        return this.swaggerAuthPropsService;
    }


    // @Bean
    // public Docket docket()
    // {
    // //
    // final String defaultRootRacine = this.swaggerAuthPropsService.getSwaggerProps().getContexteRacineDefault()
    // .trim();
    //
    // final String basePackage = this.swaggerAuthPropsService.getSwaggerProps().getAppBasePackage().trim();
    // // final String appBasePath = this.jwtAuthEnvAddressConfig.getHostAddress().trim();
    // // final boolean isLocalHostAdresse = this.jwtAuthEnvAddressConfig.isLocalHost();
    //
    // return new Docket(DocumentationType.OAS_30)// api mechanism is initialized for swagger specification 3.0
    // .select() // returns an instance of ApiSelectorBuilder to give fine grained control
    // .apis(RequestHandlerSelectors.any())//
    // // .apis(RequestHandlerSelectors.basePackage(basePackage))//
    // .apis(RequestHandlerSelectors.basePackage("fr.vincent.tuto.controller"))//
    // .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))// allows selection of RequestHandler's
    // .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))//
    // .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))//
    // .apis(RequestHandlerSelectors.withMethodAnnotation(ApiResponses.class))//
    // .paths(PathSelectors.any())// allows selection of Path's using a predicate.
    // .build()// The selector needs to be built after configuring the api and path selectors
    // // .pathMapping(defaultRootRacine) // Adds a servlet path mapping
    // .directModelSubstitute(java.time.LocalDate.class, String.class)// Convenience rule builder that substitutes
    // // LocalDate with String
    // .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)// Convenience rule builder that substitutes
    // // ZonedDateTime with Date
    // .directModelSubstitute(java.time.LocalDateTime.class, Date.class)// Convenience rule builder that substitutes
    // // LocalDateTime with Date
    // .genericModelSubstitutes(ResponseEntity.class)// essayer avec Optional
    // // .genericModelSubstitutes(Optional.class)//
    // // .forCodeGeneration(true)//
    // .produces(set)//
    // .consumes(set)//
    // .useDefaultResponseMessages(true)//
    // // .apiInfo(this.metadata())//
    // .securitySchemes(Collections.singletonList(this.apiKey())).securityContexts(Collections.singletonList(
    // securityContext()))//
    // // .tags(new Tag("users", "Operations about users"))
    // ;
    // }

    // @Bean
    // public UiConfiguration uiConfig()
    // {
    // //
    // return UiConfigurationBuilder.builder()//
    // .deepLinking(Boolean.TRUE)//
    // .displayOperationId(Boolean.FALSE)//
    // .defaultModelExpandDepth(Integer.valueOf("1"))//
    // .defaultModelRendering(ModelRendering.EXAMPLE)//
    // .displayRequestDuration(Boolean.TRUE)//
    // .docExpansion(DocExpansion.NONE)//
    // .filter(Boolean.FALSE)//
    // // .maxDisplayedTags(Integer.valueOf("0"))//
    // .maxDisplayedTags(null)//
    // .operationsSorter(OperationsSorter.ALPHA)//
    // .showExtensions(Boolean.FALSE)//
    // .showExtensions(Boolean.FALSE).tagsSorter(TagsSorter.ALPHA)//
    // .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)//
    // .validatorUrl(null)//
    // .build();
    // }

    private ApiKey apiKey()
    {
        return new ApiKey(AuthAppConstant.AUTH_BAERER_TOKEN, AuthAppConstant.AUTHORISATION, AuthAppConstant.HEADER);
    }


    private ApiInfo metadata()
    {
        return new ApiInfoBuilder()//
        .title(this.swaggerAuthPropsService.getSwaggerProps().getTitle().trim())//
        .description(this.swaggerAuthPropsService.getSwaggerProps().getDescription().trim())//
        .contact(this.contact())//
        .license(this.swaggerAuthPropsService.getSwaggerProps().getLicence().trim())//
        .licenseUrl(this.swaggerAuthPropsService.getSwaggerProps().getLicenceUrl().trim())//
        .version(this.swaggerAuthPropsService.getSwaggerProps().getVersion().trim())//
        .build();
    }

    private Contact contact()
    {
        return new Contact(this.swaggerAuthPropsService.getSwaggerProps().getContactName().trim(),
        this.swaggerAuthPropsService.getSwaggerProps().getContactUrl().trim(), this.swaggerAuthPropsService
        .getSwaggerProps().getContactEmail().trim());

    }

    /**
     * Selector for the paths this security context applies to.
     * 
     * @return
     */
    @SuppressWarnings("deprecation")
    private SecurityContext securityContext()
    {
        return SecurityContext.builder().//
        securityReferences(defaultAuth())//
        // .forPaths(PathSelectors.regex("/anyPath.*"))//
        .forPaths(PathSelectors.any()).build();
    }

    /**
     * @return
     */
    private List<SecurityReference> defaultAuth()
    {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        // return Collections.singletonList(new SecurityReference("mykey", authorizationScopes));
        return Collections.singletonList(new SecurityReference(AuthAppConstant.AUTHORISATION, authorizationScopes));
    }

}
