/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : User.java
 * Date de création : 14 oct. 2020
 * Heure de création : 06:12:39
 * Package : fr.vincent.tuto.model
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.model.po;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.vincent.tuto.enumeration.RoleNameEnum;

/**
 * T_USERS table informations mapping of the in the database. The table contains the elements cited below :
 * <ul>
 * <li>object technical identifier</li>
 * <li>user login or username</li>
 * <li>user email</li>
 * <li>user encrypted password</li>
 * <li>flag indicating whether the connected user is active or not</li>
 * <li>object creation date</li>
 * <li>object creation timestamp</>
 * <li>user roles</li>
 * <li>object version</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
@Entity
@Table(name = "T_USERS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User extends AbstractPersistable<Long> implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 5618148825418207911L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    @NonNull
    private Long id;

    @NonNull
    @Size(min = 4, max = 80, message = "Minimum username length: 4 characters")
    @Column(name = "LOGIN", unique = true, nullable = false)
    private String username;

    @JsonIgnore
    @Column(name = "USER_PASSWORD")
    private String password;

    @Email
    @Size(min = 8, max = 25, message = "Minimum email length: 8 characters")
    @Column(name = "EMAIL", unique = true, nullable = false)
    private String email;
    
    @Column(name = "USER_ACTIVE")
    private Boolean active; // Actif si TRUE, inactif sinon
    
    @CreatedDate
    @Column(name = "DATE_CREATION")
    private LocalDate dateCreation;

    @LastModifiedDate
    @Column(name = "HORODATAGE")
    private LocalDateTime horodatage;

    @JsonIgnore
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<RoleNameEnum> roles = new HashSet<>();
    
    @JsonIgnore
    @Version
    @Column(name = "OPTLOCK", columnDefinition = "integer DEFAULT 0", nullable = false)
    private Integer version;
    
    // BUILDER

    public User id(final Long pId)
    {
        this.id = pId;
        return this;
    }

    public User username(final String pUsername)
    {
        this.username = pUsername;
        return this;
    }

    public User password(final String pPassword)
    {
        this.password = pPassword;
        return this;
    }

    public User email(final String pEmail)
    {
        this.email = pEmail;
        return this;
    }

    public User active(final Boolean pActive)
    {
        this.active = pActive;
        return this;
    }

    public User dateCreation(final LocalDate pDateCreation)
    {
        this.dateCreation = pDateCreation;
        return this;
    }

    public User horodatage(final LocalDateTime pHorodatage)
    {
        this.horodatage = pHorodatage;
        return this;
    }

    public User roles(final Set<RoleNameEnum> pRoles)
    {
        this.roles = pRoles;
        return this;
    }

    // GETTER/SETTER

    @Override
    public Long getId()
    {
        return this.id;
    }

    public String getUsername()
    {
        return this.username;
    }

    public String getPassword()
    {
        return this.password;
    }

    public String getEmail()
    {
        return this.email;
    }

    public Boolean getActive()
    {
        return this.active;
    }

    public Set<RoleNameEnum> getRoles()
    {
        return this.roles;
    }

    public Integer getVersion()
    {
        return this.version;
    }

    public LocalDate getDateCreation()
    {
        return this.dateCreation;
    }

    public LocalDateTime getHorodatage()
    {
        return this.horodatage;
    }

    @Override
    public void setId(final Long pPk)
    {
        this.id = pPk;
    }

    public void setUsername(final String pUsername)
    {
        this.username = pUsername;
    }

    public void setPassword(final String pPassword)
    {
        this.password = pPassword;
    }

    public void setEmail(final String pEmail)
    {
        this.email = pEmail;
    }

    public void setActive(final Boolean pActive)
    {
        this.active = pActive;
    }

    public void setRoles(final Set<RoleNameEnum> pRoles)
    {
        this.roles = pRoles;
    }

    public void setDateCreation(final LocalDate pDateCreation)
    {
        this.dateCreation = pDateCreation;
    }

    public void setHorodatage(final LocalDateTime pHorodatage)
    {
        this.horodatage = pHorodatage;
    }

    public void setVersion(final Integer pVersion)
    {
        this.version = pVersion;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(Object pObj)
    {
        if (this == pObj)
        {
            return true;
        }
        if (pObj == null || getClass() != pObj.getClass())
        {
            return false;
        }
        User other = (User) pObj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
    
    
    