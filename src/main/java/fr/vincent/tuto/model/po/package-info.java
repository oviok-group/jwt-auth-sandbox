/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 25 oct. 2020
 * Heure de création : 17:06:36
 * Package : fr.vincent.tuto.model.po
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Persistent object's
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.model.po;
