/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 13 oct. 2020
 * Heure de création : 15:23:07
 * Package : fr.vincent.tuto.model
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * App object's
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.model;
