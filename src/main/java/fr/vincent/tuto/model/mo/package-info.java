/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 25 oct. 2020
 * Heure de création : 17:08:04
 * Package : fr.vincent.tuto.model.mo
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Non persistent object
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.model.mo;
