/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 1 nov. 2020
 * Heure de création : 16:35:30
 * Package : fr.vincent.tuto.model.dto.request
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * @author Vincent Otchoun
 *
 */
package fr.vincent.tuto.model.dto.request;
