/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 1 nov. 2020
 * Heure de création : 16:34:18
 * Package : fr.vincent.tuto.model.dto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contains data transfer design pattern object's
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.model.dto;
