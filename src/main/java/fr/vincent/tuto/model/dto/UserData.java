/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : UserData.java
 * Date de création : 24 oct. 2020
 * Heure de création : 04:29:24
 * Package : fr.vincent.tuto.dto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.model.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import fr.vincent.tuto.model.po.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * {@link User}'s data to be authenticated
 * 
 * @author Vincent Otchoun
 */
@ApiModel(description = "User's data to be authenticated")
public class UserData implements Serializable // implements Comparator<T>
{
    /**
     * 
     */
    private static final long serialVersionUID = 7391677623221772681L;

    @ApiModelProperty(name = "id", dataType = "java.lang.Long", value = "User technical identifier", position = 0)
    private Long id;

    @ApiModelProperty(name = "username", dataType = "java.lang.String", value = "Unique user login", example = "admin", position = 1)
    private String username;

    @ApiModelProperty(name = "password", dataType = "java.lang.String", value = "User password", position = 2)
    private String password;

    @ApiModelProperty(name = "email", dataType = "java.lang.String", value = "User email", position = 3)
    private String email;

    @ApiModelProperty(name = "active", dataType = "java.lang.Boolean", value = "User state", position = 4)
    private Boolean active;

    @ApiModelProperty(name = "dateCreation", dataType = "java.time.LocalDate", value = "Object creation date", position = 5)
    private LocalDate dateCreation;

    @ApiModelProperty(name = "horodatage", dataType = "java.time.LocalDateTime", value = "Object creation timestamp", position = 6)
    private LocalDateTime horodatage;

    @ApiModelProperty(name = "roles", dataType = "java.util.Set", value = "User roles", position = 7)
    private Set<String> roles;

    @ApiModelProperty(name = "version", dataType = "java.lang.Integer", value = "User object version", position = 8)
    private Integer version;


    // BUILDER

    public UserData username(final String pUsername)
    {
        this.username = pUsername;
        return this;
    }

    public UserData password(final String pPassword)
    {
        this.password = pPassword;
        return this;
    }

    public UserData email(final String pEmail)
    {
        this.email = pEmail;
        return this;
    }

    public UserData active(final Boolean pActive)
    {
        this.active = pActive;
        return this;
    }

    public UserData dateCreation(final LocalDate pDateCreation)
    {
        this.dateCreation = pDateCreation;
        return this;
    }

    public UserData horodatage(final LocalDateTime pHorodatage)
    {
        this.horodatage = pHorodatage;
        return this;
    }

    public UserData roles(final Set<String> pRoles)
    {
        this.roles = pRoles;
        return this;
    }


    // GETTER - SETTER

    public Long getId()
    {
        return this.id;
    }
    public String getUsername()
    {
        return this.username;
    }


    public String getPassword()
    {
        return this.password;
    }

    public String getEmail()
    {
        return this.email;
    }

    public Boolean getActive()
    {
        return this.active;
    }

    public LocalDate getDateCreation()
    {
        return this.dateCreation;
    }

    public LocalDateTime getHorodatage()
    {
        return this.horodatage;
    }

    public Set<String> getRoles()
    {
        return this.roles;
    }

    public Integer getVersion()
    {
        return this.version;
    }

    public void setUsername(final String pUsername)
    {
        this.username = pUsername;
    }

    public void setPassword(final String pPassword)
    {
        this.password = pPassword;
    }

    public void setEmail(final String pEmail)
    {
        this.email = pEmail;
    }

    public void setActive(final Boolean pActive)
    {
        this.active = pActive;
    }

    public void setDateCreation(final LocalDate pDateCreation)
    {
        this.dateCreation = pDateCreation;
    }

    public void setHorodatage(final LocalDateTime pHorodatage)
    {
        this.horodatage = pHorodatage;
    }

    public void setRoles(final Set<String> pRoles)
    {
        this.roles = pRoles;
    }

    public void setId(final Long pId)
    {
        this.id = pId;
    }

    public void setVersion(final Integer pVersion)
    {
        this.version = pVersion;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
