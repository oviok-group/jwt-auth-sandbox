/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 15 oct. 2020
 * Heure de création : 13:42:52
 * Package : fr.vincent.tuto.repository
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contains the abstraction objects of the database access layer
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.repository;
