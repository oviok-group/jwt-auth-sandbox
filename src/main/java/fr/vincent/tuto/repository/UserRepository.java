/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : UserRepository.java
 * Date de création : 15 oct. 2020
 * Heure de création : 14:26:02
 * Package : fr.vincent.tuto.repository
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.repository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.vincent.tuto.model.po.User;

/**
 * Spring Data JPA repository for the {@link User} entity.
 * <ul>
 * <li>Return the list of active (inactive) users in the system</li>
 * <li>Search for a user by his login</li>
 * <li>Check if user exists with her login</li>
 * <li>Retrieve or find user data by email ignoring case.</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>
{

    String USERS_BY_LOGIN_CACHE = "usersByLogin";
    String USERS_BY_EMAIL_CACHE = "usersByEmail";

    /**
     * Retrieve the list of inactive users, those for whom active = FALSE.
     * 
     * @return inactive user's collection
     */
    // @Query("select u from User u where u.active = 0")
    Collection<User> findAllByActiveIsFalse();
    // getAllInactiveUSers();

    /**
     * Retrieve the list of active users, those for whom active = TRUE.
     * 
     * @return Active user's collection
     */
    // @Query("select u from User u where u.active = 1")
    Collection<User> findAllByActiveIsTrue();
    // getAllActiveUsers();

    /**
     * Search for a user by his login.
     * 
     * @param pUsername login of the desired user
     * @return user if is present or exception if not
     */
    // @Query("select u from User u where u.username =(:username)")
    // Optional<User> findByUsername(@Param(value = "username") final String pUsername);
    Optional<User> findOneByUsername(final String pUsername);

    /**
     * Retrieve or find user data by email ignoring case.
     * 
     * @param pEmail usder email
     * @return user data if present or empty if not
     */
    Optional<User> findOneByEmailIgnoreCase(final String pEmail);

    /**
     * Check if user exists with her login
     * 
     * @param pUsername login of specified user
     * @return true if present else
     */
    Boolean existsByUsername(final String pUsername);

    /**
     * Retrieve user by her roles and id
     * 
     * @param id user identifier
     * @return user if present empty if not
     */
    @EntityGraph(attributePaths = "roles")
    Optional<User> findOneWithRolesById(Long id);

    /**
     * Rerieve user by roles and username
     * 
     * @param login user username
     * @return user if present empty if not
     */
    @EntityGraph(attributePaths = "roles")
    @Cacheable(cacheNames = USERS_BY_LOGIN_CACHE)
    Optional<User> findOneWithRolesByUsername(String login);

    /**
     * Retrieve user by roles and email
     * 
     * @param email user email
     * @return user if present empty if not
     */
    @EntityGraph(attributePaths = "roles")
    @Cacheable(cacheNames = USERS_BY_EMAIL_CACHE)
    Optional<User> findOneWithRolesByEmailIgnoreCase(String email);


}
