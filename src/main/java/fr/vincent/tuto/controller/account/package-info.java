/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 3 nov. 2020
 * Heure de création : 12:43:12
 * Package : fr.vincent.tuto.controller.account
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Account and Authentication controllers.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.controller.account;
