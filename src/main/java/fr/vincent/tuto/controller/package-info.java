/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 25 oct. 2020
 * Heure de création : 22:19:46
 * Package : fr.vincent.tuto.controller
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * App's controllers objects
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.controller;
