/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 3 nov. 2020
 * Heure de création : 12:44:03
 * Package : fr.vincent.tuto.controller.user
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * {@link fr.vincent.tuto.model.po.User} operations controllers.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.controller.user;
