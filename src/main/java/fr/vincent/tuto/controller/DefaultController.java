package fr.vincent.tuto.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Api(value = "Contrôleur par défaut", description = "<font color='#1b4fe6'>Test la disponibilité du serveur</font>")
@RestController
@RequestMapping("/default")
public class DefaultController {
	
	private static Logger logger = LoggerFactory.getLogger(DefaultController.class);
	
    @ApiOperation(value = "Réponse du serveur", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "<font color='#4fe61b'>Successful Operation</font>.", content = @Content(mediaType = "application/json")), //
            @ApiResponse(responseCode="404",description="Not found",content=@Content),//
            @ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(value = "/pong")
	public ResponseEntity<String> pong() 
	{
		logger.info("Démarrage des services OK .....");
	    return new ResponseEntity<String>("Réponse du serveur: "+HttpStatus.OK.name(), HttpStatus.OK);
	}
	
    @ApiOperation(value = "Temps de Réponse du serveur", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "<font color='#4fe61b'>Successful Operation</font>.", content = @Content(mediaType = "application/json")), //
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content), //
            @ApiResponse(responseCode = "401", description = "Authentication Failure", content = @Content(schema = @Schema(hidden = true))) })
    @GetMapping("/ping")
    public @ResponseBody String ping()
    {
        logger.info("Réponse du serveur sur /default/api : " + HttpStatus.OK);
        return "{\"status\":\"OK\",\"timestamp\":\"" + System.currentTimeMillis() + "\"}";
    }
}