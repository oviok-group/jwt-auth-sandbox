/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : TechnicalAppError.java
 * Date de création : 16 oct. 2020
 * Heure de création : 23:43:35
 * Package : fr.vincent.tuto.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.exception;

/**
 * Object for intercepting technical or system errors
 * 
 * @author Vincent Otchoun
 */
public class TechnicalAppError extends RuntimeException
{
    /**
     * 
     */
    private static final long serialVersionUID = -790537705587908428L;

    /**
     * Default constructor
     */
    public TechnicalAppError()
    {
        super();
    }

    /**
     * Constructor with two parameters
     * 
     * @param message the message of the error caught
     * @param cause   the cause of the caught error
     */
    public TechnicalAppError(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Constructor with one parameter
     * 
     * @param message the message of the error caught
     */
    public TechnicalAppError(String message)
    {
        super(message);
    }

    /**
     * Constructor with one parameter
     * 
     * @param cause the cause of the caught error
     */
    public TechnicalAppError(Throwable cause)
    {
        super(cause);
    }

}
