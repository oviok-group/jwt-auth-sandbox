/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : BusinessAppException.java
 * Date de création : 17 oct. 2020
 * Heure de création : 17:11:03
 * Package : fr.vincent.tuto.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.exception;

import org.springframework.http.HttpStatus;

/**
 * Interception of business or application errors
 * 
 * @author Vincent Otchoun
 */
public class BusinessAppException extends RuntimeException
{
    /**
     * 
     */
    private static final long serialVersionUID = 4968568951195141633L;

    private Long resourceId;
    private String errorCode;
    private HttpStatus status;



    /**
     * Build the exception with the error message
     * 
     * @param pMessage error message
     */
    public BusinessAppException(final String pMessage)
    {
        super(pMessage);
    }


    /**
     * Build the exception with the resource identifier and the error message
     * 
     * @param pResourceId resource identifier
     * @param pMessage    error message
     */
    public BusinessAppException(final Long pResourceId, final String pMessage)
    {
        super(pMessage);
        this.setResourceId(pResourceId);
    }


    /**
     * Build the exception with the resource identifier, the error code, the message.
     * 
     * @param resourceId resource identifier
     * @param pErrorCode error code
     * @param pMessage   error message
     */
    public BusinessAppException(final Long pResourceId, final String pErrorCode, final String pMessage)
    {
        super(pMessage);
        this.setResourceId(pResourceId);
        this.setErrorCode(pErrorCode);
    }

    /**
     * Build the exception with the error code, the message.
     * 
     * @param pErrorCode error code
     * @param pMessage   error message
     */
    public BusinessAppException(final String pErrorCode, final String pMessage)
    {
        super(pMessage);
        this.setErrorCode(pErrorCode);
    }

    /**
     * Build the exception with the error code, the message and the status (HTTP return code)
     * 
     * @param pErrorCode error code
     * @param pMessage   error message
     * @param pStatus    HTTP return code
     */
    public BusinessAppException(final String pErrorCode, final String pMessage, final HttpStatus pStatus)
    {
        super(pMessage);
        this.setErrorCode(pErrorCode);
        this.setStatus(pStatus);
    }

    public Long getResourceId()
    {
        return this.resourceId;
    }

    public String getErrorCode()
    {
        return this.errorCode;
    }

    public HttpStatus getStatus()
    {
        return this.status;
    }

    private void setResourceId(final Long pResourceId)
    {
        this.resourceId = pResourceId;
    }

    private void setErrorCode(final String pErrorCode)
    {
        this.errorCode = pErrorCode;
    }

    private void setStatus(final HttpStatus pStatus)
    {
        this.status = pStatus;
    }
}
