/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 16 oct. 2020
 * Heure de création : 23:40:38
 * Package : fr.vincent.tuto.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contains errors or exceptions catching objects
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.exception;
