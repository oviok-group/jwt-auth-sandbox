/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 28 oct. 2020
 * Heure de création : 15:18:25
 * Package : fr.vincent.tuto.filter
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * App custom Filters.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.filter;
