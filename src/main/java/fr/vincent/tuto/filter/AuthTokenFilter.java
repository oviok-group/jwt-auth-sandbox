/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AuthTokenFilter.java
 * Date de création : 30 oct. 2020
 * Heure de création : 06:54:13
 * Package : fr.vincent.tuto.filter
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.filter;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import fr.vincent.tuto.constant.AuthAppConstant;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService;
import fr.vincent.tuto.service.security.jwt.AuthTokenProvider;
import fr.vincent.tuto.util.AuthSecurityUtils;

/**
 * Filters incoming requests and installs a Spring Security principal if a header corresponding to a valid user is
 * found. Responsible for the authentication and authorization process.
 * <p>
 * Uses {@link OncePerRequestFilter} since we are doing a database call, there is no point in doing this more than
 * once.
 * 
 * @author Vincent Otchoun
 */
@Component
public class AuthTokenFilter extends OncePerRequestFilter
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthTokenFilter.class);

    private String authorizationHeader;
    private String bearerToken;

    @Autowired
    private SwaggerAuthPropsService swaggerAuthPropsService;

    @Autowired
    private AuthTokenProvider authTokenProvider;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
    throws ServletException, IOException
    {
        //
        LOGGER.info("[doFilterInternal] - Check if a header corresponding to valid user ");

        //

        // this allows to authorize Cross Domain requests (requests between two separate networks)
        // response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader(AuthAppConstant.ALOW_ORIGIN, AuthAppConstant.ORIGIN);
        response.setHeader(AuthAppConstant.ALLOW_CREDENTIALS, AuthAppConstant.CREDENTIALS);
        response.setHeader(AuthAppConstant.ALLOW_METHODS, AuthAppConstant.METHODS);
        response.setHeader(AuthAppConstant.ALLOW_AGE, AuthAppConstant.MAX_AGE);
        response.setHeader(AuthAppConstant.ALLOW_HEADERS, AuthAppConstant.HEADERS);

        // Retrieve the JWT Token on the request
        final Optional<String> authJWT = AuthSecurityUtils.extractJwtFromRequest(request, this.authorizationHeader,
        this.bearerToken);

        //
        try
        {
            if (authJWT.isPresent())
            {
                final String authToken = authJWT.get();
                this.authTokenProvider.validateToken(authToken);
                final Authentication auth = this.authTokenProvider.getAuthentication(authToken);
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        }
        catch (Exception e)
        {
            // this is very important, since it guarantees the user is not authenticated at all
            SecurityContextHolder.clearContext();
            // this.processFilterException(response, e);
            this.processException(response, e);
        }

        filterChain.doFilter(request, response);
    }

    @Override
    public void afterPropertiesSet() throws ServletException
    {
        //
        this.authorizationHeader = this.swaggerAuthPropsService.getAuthProps().getAuthorizationHeader().trim();
        this.bearerToken = this.swaggerAuthPropsService.getAuthProps().getBearerToken().trim() + StringUtils.SPACE;
    }

    ///////////////
    //// GETTER
    //////////////

    public String getAuthorizationHeader()
    {
        return this.authorizationHeader;
    }

    public String getBearerToken()
    {
        return this.bearerToken;
    }

    public SwaggerAuthPropsService getSwaggerAuthPropsService()
    {
        return this.swaggerAuthPropsService;
    }

    public AuthTokenProvider getAuthTokenProvider()
    {
        return this.authTokenProvider;
    }

    ///////////////////////
    //// PRIVATE METHODS
    ///////////////////////

    /**
     * @param response
     * @param e
     * @throws IOException
     */
    private void processException(HttpServletResponse response, Exception e) throws IOException
    {
        response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
    }
}
