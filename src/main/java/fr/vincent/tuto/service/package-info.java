/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 15 oct. 2020
 * Heure de création : 14:49:31
 * Package : fr.vincent.tuto.service
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contains the different components of business or other processing for the proper functioning of the application
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.service;
