/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 25 oct. 2020
 * Heure de création : 15:30:36
 * Package : fr.vincent.tuto.service.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contains object's mapping services
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.service.mapper;
