/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : UserMapperService.java
 * Date de création : 25 oct. 2020
 * Heure de création : 15:19:45
 * Package : fr.vincent.tuto.model.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.service.mapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.google.common.annotations.VisibleForTesting;

import fr.vincent.tuto.enumeration.RoleNameEnum;
import fr.vincent.tuto.model.dto.UserData;
import fr.vincent.tuto.model.po.User;

/**
 * Mapper for the entity {@link User} and its DTO called {@link UserData} and vice versa.
 * 
 * @author Vincent Otchoun
 */
@Service
public class UserMapperService implements InitializingBean, DisposableBean
{
    //
    private static final String BEAN_NOT_NULL_MSG = "The bean cannot be null";

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Convert {@link UserData} to {@link User} persistent object.
     * 
     * @param pUserDTO
     * @return
     */
    public User toEntity(final UserData pUserDTO, final Boolean pActiveDto)
    {
        //
        final User user = this.modelMapper.map(pUserDTO, User.class);
        user.setUsername(pUserDTO.getUsername());
        user.setEmail(pUserDTO.getEmail());
        user.setPassword(pUserDTO.getPassword());
        user.setVersion(0);
        user.setActive(pActiveDto);

        // creation date
        if (pUserDTO.getDateCreation() == null)
        {
            user.setDateCreation(LocalDate.now());
        }
        else
        {
            user.setDateCreation(pUserDTO.getDateCreation());
        }

        // timestamp
        if (pUserDTO.getHorodatage() == null)
        {
            user.setHorodatage(LocalDateTime.now());
        }
        else
        {
            user.setHorodatage(pUserDTO.getHorodatage());
        }

        // roles
        final Set<RoleNameEnum> nameEnums = pUserDTO.getRoles()//
        .stream()//
        .filter(Objects::nonNull)//
        .map(string -> RoleNameEnum.valueOf(string))//
        .collect(Collectors.toSet());
        user.setRoles(nameEnums);
        return user;
    }

    /**
     * Convert {@link UserData} list element to {@link User} element list.
     * 
     * @param pUserDTOs the list to convert
     * @return unmodifiable user list element
     */
    public Collection<User> toEntityList(final Collection<UserData> pUserDTOs, final Boolean pActiveDto)
    {
        return pUserDTOs.stream().map(dto -> toEntity(dto, pActiveDto)).collect(Collectors.toUnmodifiableList());
    }

    /**
     * Convert {@link User} persistent object to {@link UserData} object.
     * 
     * @param pUser
     * @return
     */
    public UserData toDto(final User pUser)
    {
        final UserData dto = this.modelMapper.map(pUser, UserData.class);
        final Set<String> rolesDto = pUser.getRoles()//
        .stream()//
        .filter(Objects::nonNull)//
        .map(RoleNameEnum::getAuthority)//
        .collect(Collectors.toSet());
        dto.setRoles(rolesDto);
        return dto;
    }



    /**
     * Convert {@link User} list element to {@link UserData} element list.
     * 
     * @param pUsers user list to convert
     * @return unmodifiable user list element
     */
    public Collection<UserData> toDtoList(final Collection<User> pUsers)
    {
        return pUsers.stream().map(user -> toDto(user)).collect(Collectors.toUnmodifiableList());
    }


    @Override
    public void afterPropertiesSet() throws Exception
    {
        Assert.notNull(this.modelMapper, BEAN_NOT_NULL_MSG);
    }

    @Override
    public void destroy() throws Exception
    {
        this.modelMapper = null;
    }


    /**
     * @param modelMapper the modelMapper to set
     */
    @VisibleForTesting
    public void setModelMapper(ModelMapper modelMapper)
    {
        this.modelMapper = modelMapper;
    }

}
