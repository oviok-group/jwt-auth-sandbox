/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AuthUserDetailsService.java
 * Date de création : 26 oct. 2020
 * Heure de création : 13:15:27
 * Package : fr.vincent.tuto.service.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.service.security;

import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.google.common.annotations.VisibleForTesting;

import fr.vincent.tuto.enumeration.AppErrorCode;
import fr.vincent.tuto.exception.BusinessAppException;
import fr.vincent.tuto.repository.UserRepository;

/**
 * Custom implementation of {@link UserDetailsService} to help spring security loading user's specific data from the
 * database.
 * 
 * @author Vincent Otchoun
 */
@Component
public class AuthUserDetailsService implements UserDetailsService
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthUserDetailsService.class);

    private static final String MAIL_MSG_1 = "User with email ";
    private static final String MAIL_MSG_2 = " was not found in the database";
    private static final String USER_MSG = "User ";
    private static final String USER_MSG_NOT_FOUND = " was not found in the database";
    private static final String USER_MSG_NOT_ACTIVATED = " was not activated";

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String pUsername) throws UsernameNotFoundException
    {
        LOGGER.debug("[loadUserByUsername] - Authenticating {} ", pUsername);

        // Check email validity
        boolean isValidEmail = new EmailValidator().isValid(pUsername, null);
        if (isValidEmail)
        {
            return this.userRepository.findOneWithRolesByEmailIgnoreCase(pUsername)//
            .map(user -> this.createSpringSecurityUser(pUsername, user))//
            .orElseThrow(() -> new BusinessAppException(AppErrorCode.USER_MAIL_NOT_FOUND.name(), MAIL_MSG_1 + pUsername
            + MAIL_MSG_2));
        }

        final String lowercaseUsername = pUsername.toLowerCase(Locale.ENGLISH);
        return this.userRepository.findOneWithRolesByUsername(lowercaseUsername)//
        .map(user -> this.createSpringSecurityUser(lowercaseUsername, user))//
        .orElseThrow(() -> new BusinessAppException(AppErrorCode.USER_LOGIN_NOT_FOUND.name(), USER_MSG
        + lowercaseUsername + USER_MSG_NOT_FOUND));
    }

    public UserRepository getUserRepository()
    {
        return this.userRepository;
    }

    @VisibleForTesting
    public void setUserRepository(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    /**
     * Create Spring Security {@link User} ith domain {@link fr.vincent.tuto.model.po.User} informations
     * 
     * @param pLogin the domain user login or username
     * @param pUser  the domain user information
     * @return
     */
    private User createSpringSecurityUser(final String pLogin, final fr.vincent.tuto.model.po.User pUser)
    {
        //
        LOGGER.debug("[createSpringSecurityUser] - Create spring security user");

        final Boolean active = pUser.getActive();
        if (BooleanUtils.isFalse(active))
        {
            throw new BusinessAppException(AppErrorCode.USER_NOT_ACTIVATED.name(), USER_MSG + pLogin.toLowerCase()
            + USER_MSG_NOT_ACTIVATED);
        }
        
        Set<SimpleGrantedAuthority> grantedAuthorities = pUser.getRoles()//
        .stream()//
        .map(role -> new SimpleGrantedAuthority(role.getAuthority()))//
        .collect(Collectors.toSet());
        return new User(pUser.getUsername(), pUser.getPassword(), grantedAuthorities);
    }
}
