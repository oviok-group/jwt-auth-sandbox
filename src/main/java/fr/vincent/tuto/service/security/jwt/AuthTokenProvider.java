/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AuthTokenProvider.java
 * Date de création : 26 oct. 2020
 * Heure de création : 17:47:18
 * Package : fr.vincent.tuto.service.security.jwt
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.service.security.jwt;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import fr.vincent.tuto.enumeration.AppErrorCode;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService;
import fr.vincent.tuto.service.security.AuthUserDetailsService;
import fr.vincent.tuto.util.AuthSecurityUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;

/**
 * Provide's many function's for JWT token processing.
 * 
 * @author Vincent Otchoun
 */
@Component
public class AuthTokenProvider implements InitializingBean
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthTokenProvider.class);

    private static final String AUTHORITY_DELIMITER = ",";
    private static final String BASE64_TOKEN_WARN_MSG = "Warning: the JWT key used is not Base64-encoded. We recommend using the `base64-token-secret` key for optimum security.";
    private static final String USING_BASE64_TOKEN_KEY = "Using a Base64-encoded JWT secret key";
    private static final String INVALID_CREDENTIALS = AppErrorCode.INVALID_CREDENTIALS.name();
    private static final String EXPIRED_TOKEN = AppErrorCode.EXPIRED_TOKEN.name();

    // In Milliseconds
    private Long tokenValidity;
    private Long rememberMeTokenValidity;
    private Long refreshTokenValidity;

    // Authorities key
    private String authoritiesKey;

    // Secret Key
    private Key key;
    byte[] keyBytes = null;

    @Autowired
    private SwaggerAuthPropsService swaggerAuthPropsService;

    @Autowired
    private AuthUserDetailsService authUserDetailsService;

    /**
     * Create access JWT Token for user Authentication/Authorization.
     * 
     * @param pAuthentication token for an authentication request or for an authenticated principal once the request
     * @param pRememberMe     for remembering or not
     * @return the access token
     */
    public String createAccessToken(final Authentication pAuthentication, final Boolean pRememberMe)
    {
        LOGGER.info("[createAccessToken] - Create acces token with remember me with [{}] flag", pRememberMe);

        final String roles = this.getRoleFromAuthentification(pAuthentication);
        final Date tokenValidityDate = this.getTokenValidity(pRememberMe, Boolean.FALSE);

        return Jwts.builder()//
        .setId(UUID.randomUUID().toString())//
        .setIssuedAt(new Date())//
        .setSubject(pAuthentication.getName())//
        .claim(this.authoritiesKey, roles)//
        .signWith(this.key, SignatureAlgorithm.HS512)//
        .setExpiration(tokenValidityDate)//
        .compact();
    }

    /**
     * Create refresh JWT Token for user Authentication/Authorization.
     * 
     * @param pAuthentication token for an authentication request or for an authenticated principal once the request
     * @param pRefreshToken   for refreshing or not
     * @return the refresh token
     */
    public String createRefreshToken(final Authentication pAuthentication, final Boolean pRefreshToken)
    {
        LOGGER.info("[createRefreshToken] - Create refresh token with [{}] flag", pRefreshToken);

        final String roles = this.getRoleFromAuthentification(pAuthentication);
        final Date tokenValidityDate = this.getTokenValidity(Boolean.FALSE, pRefreshToken);

        return Jwts.builder()//
        .setId(UUID.randomUUID().toString())//
        .setIssuedAt(new Date())//
        .setSubject(pAuthentication.getName())//
        .claim(this.authoritiesKey, roles)//
        .signWith(this.key, SignatureAlgorithm.HS512)//
        .setExpiration(tokenValidityDate)//
        .compact();
    }

    /**
     * Get the token for an authentication request or for an authenticated principal from JWT token.
     * 
     * @param pAuthToken jwt token
     * @return authentication request
     */
    public Authentication getAuthentication(final String pAuthToken)
    {
        LOGGER.info("[getAuthentication] - Get authentication,\n token [{}] ", pAuthToken);

        //
        final String username = this.getUsername(pAuthToken);
        final UserDetails springSecurityUser = this.authUserDetailsService.loadUserByUsername(username);
        return new UsernamePasswordAuthenticationToken(springSecurityUser, pAuthToken, springSecurityUser
        .getAuthorities());
    }

    /**
     * @param pAuthToken
     * @return
     */
    public Boolean validateToken(final String pAuthToken)
    {
        LOGGER.info("[validateToken] - Validate authentication,\n token [{}] ", pAuthToken);
        
        //
        try
        {
            // Jwt token has not been tampered with
            Jwts.parser().setSigningKey(this.key).parseClaimsJws(pAuthToken);
            return Boolean.TRUE;
        }
        catch (SignatureException | MalformedJwtException | UnsupportedJwtException | ExpiredJwtException
        | IllegalArgumentException e)
        {
            this.processTokenValidationException(e);
        }
        return Boolean.FALSE;
    }



    /**
     * @param pAuthToken
     * @return
     */
    public Claims getTokenClaims(final String pAuthToken)
    {
        return Jwts.parser()//
        .setSigningKey(this.key)//
        .parseClaimsJws(pAuthToken)//
        .getBody();
    }

    /**
     * @param pAuthToken
     * @return
     */
    public String getUsername(final String pAuthToken)
    {
        return this.getTokenClaims(pAuthToken).getSubject();
    }

    /**
     * @param pAuthToken
     * @return
     */
    public String getTokenSignature(final String pAuthToken)
    {
        return Jwts.parser()//
        .setSigningKey(this.key)//
        .parseClaimsJws(pAuthToken)//
        .getSignature();
    }

    /**
     * @param pAuthToken
     * @return
     */
    @SuppressWarnings("rawtypes")
    public JwsHeader getTokenHeader(final String pAuthToken)
    {
        return Jwts.parser()//
        .setSigningKey(this.key)//
        .parseClaimsJws(pAuthToken)//
        .getHeader();
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        // initialize all service fields or parameters
        final String tokenSecret = this.swaggerAuthPropsService.getAuthProps().getTokenSecret().trim();
        final String base64TokenSecret = this.swaggerAuthPropsService.getAuthProps().getBase64TokenSecret().trim();

        if (StringUtils.isNotBlank(tokenSecret))
        {
            LOGGER.warn("[afterPropertiesSet] - {} ", BASE64_TOKEN_WARN_MSG);
            this.keyBytes = tokenSecret.getBytes(StandardCharsets.UTF_8);
        }
        else if (StringUtils.isNotBlank(base64TokenSecret))
        {
            LOGGER.debug("[afterPropertiesSet] - {} ", USING_BASE64_TOKEN_KEY);
            this.keyBytes = Decoders.BASE64.decode(base64TokenSecret);
        }

        //
        this.key = Keys.hmacShaKeyFor(this.keyBytes);

        // Convert token validities in millseconds
        this.tokenValidity = 1000L * this.swaggerAuthPropsService.getAuthProps().getTokenValidity();
        this.rememberMeTokenValidity = 1000L * this.swaggerAuthPropsService.getAuthProps().getTokenValidityForRememberMe();
        this.refreshTokenValidity = 1000L * this.swaggerAuthPropsService.getAuthProps().getRefreshTokenValidity();

        //
        this.authoritiesKey = this.swaggerAuthPropsService.getAuthProps().getAuthoritiesKey().trim();
    }

    //////////////////////////////////
    //// GETTER - SETTER
    //////////////////////////////////

    public SwaggerAuthPropsService getOtherAppPropsService()
    {
        return this.swaggerAuthPropsService;
    }

    public AuthUserDetailsService getAuthUserDetailsService()
    {
        return this.authUserDetailsService;
    }


    //////////////////////////////////
    //// PRIVATE - METHODS
    //////////////////////////////////

    /**
     * @param pAuthentication
     * @return
     */
    private String getRoleFromAuthentification(final Authentication pAuthentication)
    {
        return pAuthentication.getAuthorities().stream()//
        .filter(Objects::nonNull)//
        .map(GrantedAuthority::getAuthority)//
        .collect(Collectors.joining(
        AUTHORITY_DELIMITER));
    }


    /**
     * @param pRememberMe
     * @param pRefreshToken
     * @return
     */
    private Date getTokenValidity(final Boolean pRememberMe, final Boolean pRefreshToken)
    {
        Date validity;
        final Long now = (new Date()).getTime();
        if (BooleanUtils.isTrue(pRememberMe))
        {
            validity = new Date(now + this.rememberMeTokenValidity);
        }
        else if (BooleanUtils.isTrue(pRefreshToken))
        {
            validity = new Date(now + this.refreshTokenValidity);
        }
        else
        {
            validity = new Date(now + this.tokenValidity);
        }
        return validity;
    }

    /**
     * @param e
     */
    private void processTokenValidationException(RuntimeException e)
    {
        if (AuthSecurityUtils.isNotExpiredException(e))
        {
            LOGGER.warn("[validateToken] - {} , {}", INVALID_CREDENTIALS, e.getMessage());
        }
        else
        {
            LOGGER.warn("[validateToken] - {} , {}", EXPIRED_TOKEN, e.getMessage());
        }
    }
}
