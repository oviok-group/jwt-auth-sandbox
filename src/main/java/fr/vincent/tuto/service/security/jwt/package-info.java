/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 26 oct. 2020
 * Heure de création : 13:14:14
 * Package : fr.vincent.tuto.service.security.jwt
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * JWT Token services.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.service.security.jwt;
