/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 26 oct. 2020
 * Heure de création : 13:13:29
 * Package : fr.vincent.tuto.service.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Spring Security services component.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.service.security;
