/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : SwaggerAuthPropsService.java
 * Date de création : 25 oct. 2020
 * Heure de création : 22:13:29
 * Package : fr.vincent.tuto.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.service.props;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

/**
 * Load other App properties from file's.
 * 
 * @author Vincent Otchoun
 */
@Service
@ConfigurationProperties(prefix = "votjwt", ignoreInvalidFields = false, ignoreUnknownFields = true)
public class SwaggerAuthPropsService
{

    private final SwaggerProps swaggerProps = new SwaggerProps();
    private final AuthProps authProps = new AuthProps();
    private final MailProps mailProps = new MailProps();

    ///////////////////
    // GETTER
    //////////////////

    public SwaggerProps getSwaggerProps()
    {
        return this.swaggerProps;
    }

    public AuthProps getAuthProps()
    {
        return this.authProps;
    }

    public MailProps getMailProps()
    {
        return this.mailProps;
    }

    /**
     * Swagger documentation's generation properties management and root context.
     * 
     * @author Vincent Otchoun
     */
    public static class SwaggerProps
    {
        private String title;
        private String description;
        private String version;
        private String termeOfServiceUrl;
        private String contactName;
        private String contactUrl;
        private String contactEmail;
        private String licence;
        private String licenceUrl;
        private String defaultIncludePattern;
        private String host;
        private List<String> protocols = new ArrayList<>();
        private Boolean useDefaultResponseMessages;
        private Boolean codeGeneration;
        private String appBasePackage;

        // swagger root context properties
        private String nomMachineDev;
        private String contexteRacineDefault;
        private String contexteRacine;

        public String getTitle()
        {
            return this.title;
        }

        public String getDescription()
        {
            return this.description;
        }

        public String getVersion()
        {
            return this.version;
        }

        public String getTermeOfServiceUrl()
        {
            return this.termeOfServiceUrl;
        }

        public String getContactName()
        {
            return this.contactName;
        }

        public String getContactUrl()
        {
            return this.contactUrl;
        }

        public String getContactEmail()
        {
            return this.contactEmail;
        }

        public String getLicence()
        {
            return this.licence;
        }

        public String getLicenceUrl()
        {
            return this.licenceUrl;
        }

        public String getDefaultIncludePattern()
        {
            return this.defaultIncludePattern;
        }

        public String getHost()
        {
            return this.host;
        }

        public List<String> getProtocols()
        {
            return this.protocols;
        }

        public Boolean getUseDefaultResponseMessages()
        {
            return this.useDefaultResponseMessages;
        }

        public Boolean getCodeGeneration()
        {
            return this.codeGeneration;
        }

        public String getAppBasePackage()
        {
            return this.appBasePackage;
        }

        public String getContexteRacine()
        {
            return this.contexteRacine;
        }

        public String getNomMachineDev()
        {
            return this.nomMachineDev;
        }

        public String getContexteRacineDefault()
        {
            return this.contexteRacineDefault;
        }

        public void setTitle(final String pTitle)
        {
            this.title = pTitle;
        }

        public void setDescription(final String pDescription)
        {
            this.description = pDescription;
        }

        public void setVersion(final String pVersion)
        {
            this.version = pVersion;
        }

        public void setTermeOfServiceUrl(final String pTermeOfServiceUrl)
        {
            this.termeOfServiceUrl = pTermeOfServiceUrl;
        }

        public void setContactName(final String pContactName)
        {
            this.contactName = pContactName;
        }

        public void setContactUrl(final String pContactUrl)
        {
            this.contactUrl = pContactUrl;
        }

        public void setContactEmail(final String pContactEmail)
        {
            this.contactEmail = pContactEmail;
        }

        public void setLicence(final String pLicence)
        {
            this.licence = pLicence;
        }

        public void setLicenceUrl(final String pLicenceUrl)
        {
            this.licenceUrl = pLicenceUrl;
        }

        public void setDefaultIncludePattern(final String pDefaultIncludePattern)
        {
            this.defaultIncludePattern = pDefaultIncludePattern;
        }

        public void setHost(final String pHost)
        {
            this.host = pHost;
        }

        public void setProtocols(final List<String> pProtocols)
        {
            this.protocols = pProtocols;
        }

        public void setUseDefaultResponseMessages(Boolean pUseDefaultResponseMessages)
        {
            this.useDefaultResponseMessages = pUseDefaultResponseMessages;
        }

        public void setCodeGeneration(Boolean pCodeGeneration)
        {
            this.codeGeneration = pCodeGeneration;
        }

        public void setAppBasePackage(final String pAppBasePackage)
        {
            this.appBasePackage = pAppBasePackage;
        }

        public void setNomMachineDev(final String pNomMachineDev)
        {
            this.nomMachineDev = pNomMachineDev;
        }

        public void setContexteRacineDefault(final String pContexteRacineDefault)
        {
            this.contexteRacineDefault = pContexteRacineDefault;
        }

        public void setContexteRacine(final String pContexteRacine)
        {
            this.contexteRacine = pContexteRacine;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Security : authentification and authorization properties
     * 
     * @author Vincent Otchoun
     */
    public static class AuthProps
    {
        private String tokenSecret;
        private String base64TokenSecret;
        private Long tokenValidity;
        private Long tokenValidityForRememberMe;
        private Long refreshTokenValidity;
        private String authorizationHeader;
        private String authoritiesKey;
        private String bearerToken;

        public String getTokenSecret()
        {
            return this.tokenSecret;
        }

        public String getBase64TokenSecret()
        {
            return this.base64TokenSecret;
        }

        public Long getTokenValidity()
        {
            return this.tokenValidity;
        }

        public Long getTokenValidityForRememberMe()
        {
            return this.tokenValidityForRememberMe;
        }

        public Long getRefreshTokenValidity()
        {
            return this.refreshTokenValidity;
        }

        public String getAuthorizationHeader()
        {
            return this.authorizationHeader;
        }

        public String getAuthoritiesKey()
        {
            return this.authoritiesKey;
        }

        public String getBearerToken()
        {
            return this.bearerToken;
        }

        public void setTokenSecret(final String pTokenSecret)
        {
            this.tokenSecret = pTokenSecret;
        }

        public void setBase64TokenSecret(final String pBase64TokenSecret)
        {
            this.base64TokenSecret = pBase64TokenSecret;
        }

        public void setTokenValidity(final Long pTokenValidity)
        {
            this.tokenValidity = pTokenValidity;
        }

        public void setTokenValidityForRememberMe(final Long pTokenValidityForRememberMe)
        {
            this.tokenValidityForRememberMe = pTokenValidityForRememberMe;
        }

        public void setRefreshTokenValidity(final Long pRefreshTokenValidity)
        {
            this.refreshTokenValidity = pRefreshTokenValidity;
        }

        public void setAuthorizationHeader(final String pAuthorizationHeader)
        {
            this.authorizationHeader = pAuthorizationHeader;
        }

        public void setAuthoritiesKey(final String pAuthoritiesKey)
        {
            this.authoritiesKey = pAuthoritiesKey;
        }

        public void setBearerToken(final String pBearerToken)
        {
            this.bearerToken = pBearerToken;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Mail properties
     * 
     * @author Vincent Otchoun
     */
    public static class MailProps
    {
        private String hostValue;
        private Integer port;
        private String username;
        private String password;
        private String protocol;
        private Boolean tlsValue;
        private Boolean authValue;
        private String fromValue;

        public String getHostValue()
        {
            return this.hostValue;
        }

        public Integer getPort()
        {
            return this.port;
        }

        public String getUsername()
        {
            return this.username;
        }

        public String getPassword()
        {
            return this.password;
        }

        public String getProtocol()
        {
            return this.protocol;
        }

        public Boolean getTlsValue()
        {
            return this.tlsValue;
        }

        public Boolean getAuthValue()
        {
            return this.authValue;
        }

        public String getFromValue()
        {
            return this.fromValue;
        }

        public void setHostValue(final String pHostValue)
        {
            this.hostValue = pHostValue;
        }

        public void setPort(final Integer pPort)
        {
            this.port = pPort;
        }

        public void setUsername(final String pUsername)
        {
            this.username = pUsername;
        }

        public void setPassword(final String pPassword)
        {
            this.password = pPassword;
        }

        public void setProtocol(final String pProtocol)
        {
            this.protocol = pProtocol;
        }

        public void setTlsValue(final Boolean pTlsValue)
        {
            this.tlsValue = pTlsValue;
        }

        public void setAuthValue(final Boolean pAuthValue)
        {
            this.authValue = pAuthValue;
        }

        public void setFromValue(final String pFromValue)
        {
            this.fromValue = pFromValue;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

}
