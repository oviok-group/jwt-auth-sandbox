/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : DatabasePropsService.java
 * Date de création : 15 oct. 2020
 * Heure de création : 15:04:46
 * Package : fr.vincent.tuto.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.service.props;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

/**
 * Loading providing the feed properties of the various components of the application.
 * 
 * @author Vincent Otchoun
 */
@Service
@ConfigurationProperties(prefix = "votjwt", ignoreInvalidFields = false, ignoreUnknownFields = true)
public class DatabasePropsService
{
    //
    private final DataSourceProps dataSourceProps = new DataSourceProps();
    private final JpaHibernateProps jpaHibernateProps = new JpaHibernateProps();
    private final HikariProps hikariProps = new HikariProps();

    ///////////////////
    // GETTER
    //////////////////

    public DataSourceProps getDataSourceProps()
    {
        return this.dataSourceProps;
    }

    public JpaHibernateProps getJpaHibernateProps()
    {
        return this.jpaHibernateProps;
    }

    public HikariProps getHikariProps()
    {
        return this.hikariProps;
    }

    /**
     * Loading properties to supply components ensuring the connection to the database.
     * 
     * @author Vincent Otchoun
     */
    public static class DataSourceProps
    {
        private String driverClassName;
        private String jdbcUrl;
        private String userName;
        private String password;
        private Boolean testWhileIdle;
        private String validationQuery;
        private Boolean poolPreparedStatements;
        private Integer maxOpenPreparedStatements;
        private String persistenceUnitName;
        private String packageToScan;
        private Boolean generateDdl;
        private String datasourceClassName;
        private String type;
        private String platform;

        /*
         * Spring JDBC initializer DataSource properties for Hikari pool manager
         */
        private String cachePrepareStatements;
        private String prepareStatementCacheSize;
        private String prepareStatementCacheSqlLimit;
        private String useServerPrepareStatements;

        /*
         * Non-programmatic approach : uses Spring JDBC initializer when application started to create database's
         * schema's or load initial data with DDL/DML scripts's.
         */
        private String initializationMode;
        private String initSchema;
        private String initData;
        private Boolean initContinueOnError;

        public String getDriverClassName()
        {
            return this.driverClassName;
        }

        public String getJdbcUrl()
        {
            return this.jdbcUrl;
        }

        public String getUserName()
        {
            return this.userName;
        }

        public String getPassword()
        {
            return this.password;
        }

        public Boolean getTestWhileIdle()
        {
            return this.testWhileIdle;
        }

        public String getValidationQuery()
        {
            return this.validationQuery;
        }

        public Boolean getPoolPreparedStatements()
        {
            return this.poolPreparedStatements;
        }

        public Integer getMaxOpenPreparedStatements()
        {
            return this.maxOpenPreparedStatements;
        }

        public String getPersistenceUnitName()
        {
            return this.persistenceUnitName;
        }

        public String getPackageToScan()
        {
            return this.packageToScan;
        }

        public Boolean getGenerateDdl()
        {
            return this.generateDdl;
        }

        public String getDatasourceClassName()
        {
            return this.datasourceClassName;
        }

        public String getType()
        {
            return this.type;
        }

        public String getPlatform()
        {
            return this.platform;
        }

        public String getCachePrepareStatements()
        {
            return this.cachePrepareStatements;
        }

        public String getPrepareStatementCacheSize()
        {
            return this.prepareStatementCacheSize;
        }

        public String getPrepareStatementCacheSqlLimit()
        {
            return this.prepareStatementCacheSqlLimit;
        }

        public String getUseServerPrepareStatements()
        {
            return this.useServerPrepareStatements;
        }

        public String getInitializationMode()
        {
            return this.initializationMode;
        }

        public String getInitSchema()
        {
            return this.initSchema;
        }

        public String getInitData()
        {
            return this.initData;
        }

        public Boolean getInitContinueOnError()
        {
            return this.initContinueOnError;
        }

        public void setDriverClassName(final String pDriverClassName)
        {
            this.driverClassName = pDriverClassName;
        }

        public void setJdbcUrl(final String pJdbcUrl)
        {
            this.jdbcUrl = pJdbcUrl;
        }

        public void setUserName(final String pUserName)
        {
            this.userName = pUserName;
        }

        public void setPassword(final String pPassword)
        {
            this.password = pPassword;
        }

        public void setTestWhileIdle(final Boolean pTestWhileIdle)
        {
            this.testWhileIdle = pTestWhileIdle;
        }

        public void setValidationQuery(final String pValidationQuery)
        {
            this.validationQuery = pValidationQuery;
        }

        public void setPoolPreparedStatements(final Boolean pPoolPreparedStatements)
        {
            this.poolPreparedStatements = pPoolPreparedStatements;
        }

        public void setMaxOpenPreparedStatements(final Integer pMaxOpenPreparedStatements)
        {
            this.maxOpenPreparedStatements = pMaxOpenPreparedStatements;
        }

        public void setPersistenceUnitName(final String pPersistenceUnitName)
        {
            this.persistenceUnitName = pPersistenceUnitName;
        }

        public void setPackageToScan(final String pPackageToScan)
        {
            this.packageToScan = pPackageToScan;
        }

        public void setGenerateDdl(final Boolean pGenerateDdl)
        {
            this.generateDdl = pGenerateDdl;
        }

        public void setDatasourceClassName(final String pDatasourceClassName)
        {
            this.datasourceClassName = pDatasourceClassName;
        }

        public void setType(final String pType)
        {
            this.type = pType;
        }

        public void setPlatform(final String pPlatform)
        {
            this.platform = pPlatform;
        }

        public void setCachePrepareStatements(final String pCachePrepareStatements)
        {
            this.cachePrepareStatements = pCachePrepareStatements;
        }

        public void setPrepareStatementCacheSize(final String pPrepareStatementCacheSize)
        {
            this.prepareStatementCacheSize = pPrepareStatementCacheSize;
        }

        public void setPrepareStatementCacheSqlLimit(final String pPrepareStatementCacheSqlLimit)
        {
            this.prepareStatementCacheSqlLimit = pPrepareStatementCacheSqlLimit;
        }

        public void setUseServerPrepareStatements(final String pUseServerPrepareStatements)
        {
            this.useServerPrepareStatements = pUseServerPrepareStatements;
        }

        public void setInitializationMode(final String pInitializationMode)
        {
            this.initializationMode = pInitializationMode;
        }

        public void setInitSchema(final String pInitSchema)
        {
            this.initSchema = pInitSchema;
        }

        public void setInitData(final String pInitData)
        {
            this.initData = pInitData;
        }

        public void setInitContinueOnError(final Boolean pInitContinueOnError)
        {
            this.initContinueOnError = pInitContinueOnError;
        }

        /////////////
        // TOSTRING
        /////////////

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }



    /**
     * Loading properties to feed JPA/Hibernate components for database information access operations.
     * 
     * @author Vincent Otchoun
     */
    public static class JpaHibernateProps
    {
        private String databaseName;
        private String dialect;
        private Boolean showSql;
        private Boolean formatSql;
        private Boolean useSql;
        private Boolean useSqlComments;
        private Boolean bytecodeUseReflectionOptimizer;
        private String namingStrategy;
        private Boolean useSecondLevelCache;
        private Boolean generateStatistics;
        private Boolean useReflectionOptimizer;
        private String ddlAuto;
        private Boolean enableLazy;

        public String getDatabaseName()
        {
            return this.databaseName;
        }

        public String getDialect()
        {
            return this.dialect;
        }

        public Boolean getShowSql()
        {
            return this.showSql;
        }

        public Boolean getFormatSql()
        {
            return this.formatSql;
        }

        public Boolean getUseSql()
        {
            return this.useSql;
        }

        public Boolean getUseSqlComments()
        {
            return this.useSqlComments;
        }

        public Boolean getBytecodeUseReflectionOptimizer()
        {
            return this.bytecodeUseReflectionOptimizer;
        }

        public String getNamingStrategy()
        {
            return this.namingStrategy;
        }

        public Boolean getUseSecondLevelCache()
        {
            return this.useSecondLevelCache;
        }

        public Boolean getGenerateStatistics()
        {
            return this.generateStatistics;
        }

        public Boolean getUseReflectionOptimizer()
        {
            return this.useReflectionOptimizer;
        }

        public String getDdlAuto()
        {
            return this.ddlAuto;
        }

        public Boolean getEnableLazy()
        {
            return this.enableLazy;
        }

        public void setDatabaseName(final String pDatabaseName)
        {
            this.databaseName = pDatabaseName;
        }

        public void setDialect(final String pDialect)
        {
            this.dialect = pDialect;
        }

        public void setShowSql(final Boolean pShowSql)
        {
            this.showSql = pShowSql;
        }

        public void setFormatSql(final Boolean pFormatSql)
        {
            this.formatSql = pFormatSql;
        }

        public void setUseSql(final Boolean pUseSql)
        {
            this.useSql = pUseSql;
        }

        public void setUseSqlComments(final Boolean pUseSqlComments)
        {
            this.useSqlComments = pUseSqlComments;
        }

        public void setBytecodeUseReflectionOptimizer(final Boolean pBytecodeUseReflectionOptimizer)
        {
            this.bytecodeUseReflectionOptimizer = pBytecodeUseReflectionOptimizer;
        }

        public void setNamingStrategy(final String pNamingStrategy)
        {
            this.namingStrategy = pNamingStrategy;
        }

        public void setUseSecondLevelCache(final Boolean pUseSecondLevelCache)
        {
            this.useSecondLevelCache = pUseSecondLevelCache;
        }

        public void setGenerateStatistics(final Boolean pGenerateStatistics)
        {
            this.generateStatistics = pGenerateStatistics;
        }

        public void setUseReflectionOptimizer(final Boolean pUseReflectionOptimizer)
        {
            this.useReflectionOptimizer = pUseReflectionOptimizer;
        }

        public void setDdlAuto(final String pDdlAuto)
        {
            this.ddlAuto = pDdlAuto;
        }

        public void setEnableLazy(final Boolean pEnableLazy)
        {
            this.enableLazy = pEnableLazy;
        }

        /////////////
        // TOSTRING
        /////////////
        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }


    /**
     * Loading properties to feed Hikari components (database resource access pool manager).
     * 
     * @author Vincent Otchoun
     */
    public static class HikariProps
    {
        private Integer minimumIdle;
        private Integer maximumPoolSize;
        private Long idleTimeout;
        private String poolName;
        private Long maxLifetime;
        private Long connectionTimeout;

        private Boolean cachePrepareStatements;
        private Integer prepareStatementCacheSize;
        private Integer prepareStatementCacheSqlLimit;
        private Boolean useServerPrepareStatements;

        public Integer getMinimumIdle()
        {
            return this.minimumIdle;
        }

        public Integer getMaximumPoolSize()
        {
            return this.maximumPoolSize;
        }

        public Long getIdleTimeout()
        {
            return this.idleTimeout;
        }

        public String getPoolName()
        {
            return this.poolName;
        }

        public Long getMaxLifetime()
        {
            return this.maxLifetime;
        }

        public Long getConnectionTimeout()
        {
            return this.connectionTimeout;
        }

        public Boolean getCachePrepareStatements()
        {
            return this.cachePrepareStatements;
        }

        public Integer getPrepareStatementCacheSize()
        {
            return this.prepareStatementCacheSize;
        }

        public Integer getPrepareStatementCacheSqlLimit()
        {
            return this.prepareStatementCacheSqlLimit;
        }

        public Boolean getUseServerPrepareStatements()
        {
            return this.useServerPrepareStatements;
        }

        public void setMinimumIdle(final Integer pMinimumIdle)
        {
            this.minimumIdle = pMinimumIdle;
        }

        public void setMaximumPoolSize(final Integer pMaximumPoolSize)
        {
            this.maximumPoolSize = pMaximumPoolSize;
        }

        public void setIdleTimeout(final Long pIdleTimeout)
        {
            this.idleTimeout = pIdleTimeout;
        }

        public void setPoolName(final String pPoolName)
        {
            this.poolName = pPoolName;
        }

        public void setMaxLifetime(final Long pMaxLifetime)
        {
            this.maxLifetime = pMaxLifetime;
        }

        public void setConnectionTimeout(final Long pConnectionTimeout)
        {
            this.connectionTimeout = pConnectionTimeout;
        }

        public void setCachePrepareStatements(final Boolean pCachePrepareStatements)
        {
            this.cachePrepareStatements = pCachePrepareStatements;
        }

        public void setPrepareStatementCacheSize(final Integer pPrepareStatementCacheSize)
        {
            this.prepareStatementCacheSize = pPrepareStatementCacheSize;
        }

        public void setPrepareStatementCacheSqlLimit(final Integer pPrepareStatementCacheSqlLimit)
        {
            this.prepareStatementCacheSqlLimit = pPrepareStatementCacheSqlLimit;
        }

        public void setUseServerPrepareStatements(final Boolean pUseServerPrepareStatements)
        {
            this.useServerPrepareStatements = pUseServerPrepareStatements;
        }

        /////////////
        // TOSTRING
        /////////////
        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }
}
