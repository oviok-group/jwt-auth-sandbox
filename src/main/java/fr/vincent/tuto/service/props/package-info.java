/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 15 oct. 2020
 * Heure de création : 14:50:34
 * Package : fr.vincent.tuto.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Embeds the loading and digestion components of the application properties
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.service.props;
