/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : JwtAuthSandboxApp.java
 * Date de création : 13 oct. 2020
 * Heure de création : 14:53:23
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.google.common.collect.Lists;

import fr.vincent.tuto.config.AppEnvLogStartConfig;
import fr.vincent.tuto.config.DatabaseAccesConfig;
import fr.vincent.tuto.enumeration.RoleNameEnum;
import fr.vincent.tuto.model.po.User;
import fr.vincent.tuto.repository.UserRepository;

/**
 * Le starter boot de l'application d'authentification avec JWT
 * 
 * @author Vincent Otchoun
 */
@SpringBootApplication
@Import(DatabaseAccesConfig.class)
public class JwtAuthSandboxApp
{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    // @Autowired
    // private static AppEnvLogStartConfig appEnvLogStartConfig;

    /**
     * @param args
     */
    public static void main(String... args)
    {
        //
        // SpringApplication.run(JwtAuthSandboxApp.class, args);

        //
        // System.setProperty("javax.xml.bind.JAXBContextFactory", "org.eclipse.persistence.jaxb.JAXBContextFactory");

        // new SpringApplicationBuilder(JwtAuthSandboxApp.class).run(args);

        final SpringApplication application = new SpringApplication(JwtAuthSandboxApp.class);
        AppEnvLogStartConfig.addDefaultProfile(application);

        final Environment environment = application.run(args).getEnvironment();
        AppEnvLogStartConfig.logStartUp(environment);
    }

    /**
     * Initialisation de la base au démarrage de l'application. Il permet également d'initialiser les ressources base de
     * données pour les tests unitaires. Lorsqu'il est activé il faut commenter la ligne 137 dans la classe des des TU :
     * UserRepositoryTest pour faire faire fonctionner les TU correctement lors du build.
     * 
     * @return
     */
    @Bean
    CommandLineRunner init()
    {
        return args -> {
            this.creerJeuDeDonnees().forEach(user -> this.userRepository.save(user));
        };
    }


    /**
     * Fournir le jeu de données pour intialisser la base de données.
     * 
     * @return
     */
    private List<User> creerJeuDeDonnees()
    {
        final List<User> users = Lists.newArrayList();

        // ADMIN
        final Set<RoleNameEnum> adminRole = new HashSet<>();
        adminRole.add(RoleNameEnum.ROLE_ADMIN);
        final User admin = new User()//
        .username("admin1").password(this.bCryptPasswordEncoder.encode("admin1_19511982"))//
        .email("admin1.test@live.fr").active(Boolean.TRUE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(adminRole);

        // ADMIN INACTIF
        final User adminInactif = new User()//
        .username("admin2").password(this.bCryptPasswordEncoder.encode("admin2_19821951#"))//
        .email("admin2.test@live.fr").active(Boolean.FALSE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(adminRole);

        // CLIENT
        final Set<RoleNameEnum> clientRole = new HashSet<>();
        clientRole.add(RoleNameEnum.ROLE_CLIENT);
        final User client = new User()//
        .username("client1").password(this.bCryptPasswordEncoder.encode("client1_19511982"))//
        .email("client1.test@live.fr").active(Boolean.TRUE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(clientRole);

        // CLIENT INACTIF
        final User clientInactif = new User()//
        .username("client2").password(this.bCryptPasswordEncoder.encode("client2_19821951#"))//
        .email("client2.test@live.fr").active(Boolean.FALSE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(clientRole);

        // ANONYMOUS
        final Set<RoleNameEnum> anonymousRole = new HashSet<>();
        anonymousRole.add(RoleNameEnum.ROLE_ANONYMOUS);
        final User anonymous = new User()//
        .username("anonymous1").password(this.bCryptPasswordEncoder.encode("anonymous1_19511982"))//
        .email("anonymous1.test@live.fr").active(Boolean.TRUE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(anonymousRole);

        // ANONYMOUS INACTIF
        final User anonymousInactif = new User()//
        .username("anonymous2").password(this.bCryptPasswordEncoder.encode("anonymous2_19821951#"))//
        .email("anonymous2.test@live.fr").active(Boolean.FALSE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(anonymousRole);

        users.add(admin);
        users.add(adminInactif);
        users.add(client);
        users.add(clientInactif);
        users.add(anonymous);
        users.add(anonymousInactif);
        return users;
    }
}
