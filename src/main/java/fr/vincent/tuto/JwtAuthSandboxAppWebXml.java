/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : JwtAuthSandboxAppWebXml.java
 * Date de création : 13 oct. 2020
 * Heure de création : 14:55:03
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Add the web profile to the application to enable deployment in a container if needed.
 * 
 * @author Vincent Otchoun
 */
public class JwtAuthSandboxAppWebXml extends SpringBootServletInitializer
{

    /**
     * {@inheritDoc}
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        return application.sources(JwtAuthSandboxApp.class);
    }

}
