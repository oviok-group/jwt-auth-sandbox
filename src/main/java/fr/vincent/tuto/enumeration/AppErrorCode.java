/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AppErrorCode.java
 * Date de création : 26 oct. 2020
 * Heure de création : 11:41:12
 * Package : fr.vincent.tuto.enumeration
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.enumeration;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumeration of REST Error types.
 * 
 * @author Vincent Otchoun
 */
public enum AppErrorCode
{
    //
    USER_NOT_ACTIVATED(0),
    USER_LOGIN_NOT_FOUND(1), //
    USER_MAIL_NOT_FOUND(2), //
    INVALID_CREDENTIALS(3), //
    EXPIRED_TOKEN(4), //
    GLOBAL(8),
    AUTHENTICATION(10), JWT_TOKEN_EXPIRED(11);

    private int errorCode;

    private AppErrorCode(int errorCode)
    {
        this.errorCode = errorCode;
    }

    @JsonValue
    public int getErrorCode()
    {
        return errorCode;
    }
}
