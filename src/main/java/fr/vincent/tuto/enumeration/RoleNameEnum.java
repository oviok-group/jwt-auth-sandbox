/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : RoleNameEnum.java
 * Date de création : 13 oct. 2020
 * Heure de création : 15:11:09
 * Package : fr.vincent.tuto.enumeration
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.enumeration;

import org.springframework.security.core.GrantedAuthority;

/**
 * {@link User} roles enumeration define
 * 
 * @author Vincent Otchoun
 */
public enum RoleNameEnum implements GrantedAuthority
{
    ROLE_ADMIN, ROLE_CLIENT, ROLE_ANONYMOUS;

    @Override
    public String getAuthority()
    {
        return name();
    }
}
