/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 13 oct. 2020
 * Heure de création : 15:10:16
 * Package : fr.vincent.tuto.enumeration
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * App Enum layer
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.enumeration;
