/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AuthTokenFilterTest.java
 * Date de création : 31 oct. 2020
 * Heure de création : 11:17:18
 * Package : fr.vincent.tuto.filter
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.filter;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.security.Key;

import javax.servlet.ServletException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.TestsUtils;
import fr.vincent.tuto.config.JwtAuthSandboxBaseConfig;
import fr.vincent.tuto.repository.UserRepository;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService;
import fr.vincent.tuto.service.security.AuthUserDetailsService;
import fr.vincent.tuto.service.security.jwt.AuthTokenProvider;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

/**
 * Unit test for {@link AuthTokenFilter} class.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "authTokenFilterTest", classes = { JwtAuthSandboxBaseConfig.class,
        SwaggerAuthPropsService.class, UserRepository.class, AuthUserDetailsService.class, AuthTokenProvider.class,
        AuthTokenFilter.class })
@SpringBootTest
@ActiveProfiles("test")
public class AuthTokenFilterTest
{

    @Autowired
    private AuthTokenFilter authTokenFilter;

    private Key testKey;
    private byte[] keyBytes;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.keyBytes = Decoders.BASE64.decode(TestsUtils.REAL_BASE64_TEST_TOKEN);
        this.testKey = Keys.hmacShaKeyFor(this.keyBytes);

        this.authTokenFilter.afterPropertiesSet();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.authTokenFilter = null;
        this.testKey = null;
        this.keyBytes = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.filter.AuthTokenFilter#afterPropertiesSet()}.
     * 
     * @throws ServletException
     */
    @Test
    public void testAfterPropertiesSet() throws ServletException
    {
        this.authTokenFilter.afterPropertiesSet();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.filter.AuthTokenFilter#doFilterInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)}.
     * 
     * @throws IOException
     * @throws ServletException
     */
    @Test
    public void testDoFilterInternal() throws ServletException, IOException
    {
        Authentication authentication = TestsUtils.createAuthentication();

        // with real secret token
        final String jwtToken = this.authTokenFilter.getAuthTokenProvider().createAccessToken(authentication,
        Boolean.TRUE);

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(TestsUtils.AUTHORIZATION_HEADER, TestsUtils.AUTH_TOKEN_PREFIX + jwtToken);
        request.setRequestURI("/api/test");

        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain filterChain = new MockFilterChain();

        this.authTokenFilter.doFilterInternal(request, response, filterChain);
        
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication().getName()).isEqualTo(
        TestsUtils.ANONYMOUS_ROLE);
        assertThat(SecurityContextHolder.getContext().getAuthentication().getCredentials().toString()).isEqualTo(
        jwtToken);
    }

    @Test
    public void testDoFilterUnsupportedToken() throws ServletException, IOException
    {
        String unsupportedToken = TestsUtils.createUnsupportedToken(this.testKey);

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(TestsUtils.AUTHORIZATION_HEADER, TestsUtils.AUTH_TOKEN_PREFIX + unsupportedToken);
        request.setRequestURI("/api/test");

        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain filterChain = new MockFilterChain();

        this.authTokenFilter.doFilterInternal(request, response, filterChain);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
        assertThat(response.getErrorMessage()).isEqualTo("Signed plaintext JWSs are not supported.");
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    public void testDoFilterInternalInvalidToken() throws ServletException, IOException
    {
        final String jwtToken = "wrong_jwt";

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(TestsUtils.AUTHORIZATION_HEADER, TestsUtils.AUTH_TOKEN_PREFIX + jwtToken);
        request.setRequestURI("/api/test");
        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain filterChain = new MockFilterChain();

        this.authTokenFilter.doFilterInternal(request, response, filterChain);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    public void testDoFilterMissingAuthorization() throws ServletException, IOException
    {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setRequestURI("/api/test");
        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain filterChain = new MockFilterChain();

        this.authTokenFilter.doFilterInternal(request, response, filterChain);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    public void testDoFilterMissingToken() throws ServletException, IOException
    {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(TestsUtils.AUTHORIZATION_HEADER, TestsUtils.AUTH_TOKEN_PREFIX);
        request.setRequestURI("/api/test");
        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain filterChain = new MockFilterChain();

        this.authTokenFilter.doFilterInternal(request, response, filterChain);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    public void testDoFilterWrongScheme() throws ServletException, IOException
    {
        Authentication authentication = TestsUtils.createAuthentication();
        // with real secret token
        final String jwtToken = this.authTokenFilter.getAuthTokenProvider().createAccessToken(authentication,
        Boolean.FALSE);
        
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(TestsUtils.AUTHORIZATION_HEADER, TestsUtils.BASIC_AUTH_PREFIX + jwtToken);
        request.setRequestURI("/api/test");
        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain filterChain = new MockFilterChain();

        this.authTokenFilter.doFilterInternal(request, response, filterChain);
        
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.filter.AuthTokenFilter#getAuthorizationHeader()}.
     */
    @Test
    public void testGetAuthorizationHeader()
    {
        assertThat(this.authTokenFilter.getAuthorizationHeader()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.filter.AuthTokenFilter#getBearerToken()}.
     */
    @Test
    public void testGetBearerToken()
    {
        assertThat(this.authTokenFilter.getBearerToken()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.filter.AuthTokenFilter#getSwaggerAuthPropsService()}.
     */
    @Test
    public void testGetSwaggerAuthPropsService()
    {
        assertThat(this.authTokenFilter.getSwaggerAuthPropsService()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.filter.AuthTokenFilter#getAuthTokenProvider()}.
     */
    @Test
    public void testGetAuthTokenProvider()
    {
        assertThat(this.authTokenFilter.getAuthTokenProvider()).isNotNull();
    }

}
