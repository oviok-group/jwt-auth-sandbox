/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : UserRepositoryTest.java
 * Date de création : 18 oct. 2020
 * Heure de création : 16:21:10
 * Package : fr.vincent.tuto.repository
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;

import fr.vincent.tuto.config.DatabaseAccesConfig;
import fr.vincent.tuto.config.JwtAuthSandboxBaseConfig;
import fr.vincent.tuto.enumeration.RoleNameEnum;
import fr.vincent.tuto.model.po.User;
import fr.vincent.tuto.service.props.DatabasePropsService;

/**
 * Database informations access {@link UserRepository} unit tests.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "userRepositoryTest", classes = { JwtAuthSandboxBaseConfig.class,
        DatabasePropsService.class, DatabaseAccesConfig.class })
@ActiveProfiles("test")
@SpringBootTest
public class UserRepositoryTest
{
    //
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder; // pour chiffrer les mots de passe des utilisateurs

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        //
        // this.initDatabase();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.userRepository = null;
        this.bCryptPasswordEncoder = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#getAllInactiveUSers()}.
     */
    @Test
    public void testGetAllInactiveUSers()
    {
        final List<User> inactiveUSers = (List<User>) this.userRepository.findAllByActiveIsFalse();

        assertThat(inactiveUSers).isNotEmpty();
        assertThat(inactiveUSers.size()).isEqualTo(3);
        assertThat(this.creerJeuDeDonnees().get(0).equals(inactiveUSers.get(0))).isFalse();
        assertThat(this.creerJeuDeDonnees().get(0).equals(null)).isFalse();
        assertThat(inactiveUSers.get(0).equals(inactiveUSers.get(0))).isTrue();
        assertThat(inactiveUSers.get(0).hashCode()).isNotEqualTo(inactiveUSers.get(1).hashCode());
        assertThat(inactiveUSers.get(0).toString()).isNotEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#getAllActiveUsers()}.
     */
    @Test
    public void testGetAllctiveUSers()
    {
        final List<User> activeUSers = (List<User>) this.userRepository.findAllByActiveIsTrue();

        assertThat(activeUSers).isNotEmpty();
        assertThat(activeUSers.size()).isEqualTo(3);
    }


    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#findByUsername(java.lang.String)}.
     */
    @Test
    public void testFindByUsername()
    {
        final String loginToSearch = "admin1";
        Optional<User> optional = this.userRepository.findOneByUsername(loginToSearch);

        assertThat(optional.isPresent()).isTrue();

        final User user = optional.get();
        final List<RoleNameEnum> roleNameEnums = new ArrayList<>(user.getRoles());

        assertThat(user.getId()).isGreaterThan(0);
        assertThat(user.getUsername()).isEqualTo(loginToSearch);
        assertThat(user.getPassword()).startsWith("$2a$10$");
        assertThat(user.getEmail()).contains(".test@live.fr");
        assertThat(user.getActive()).isTrue();
        assertThat(user.getDateCreation()).isBefore(LocalDate.now().plusYears(1));
        assertThat(user.getHorodatage()).isBefore(LocalDateTime.now());
        assertThat(user.getRoles()).isNotEmpty();
        assertThat(roleNameEnums.get(0).getAuthority()).isEqualTo(RoleNameEnum.ROLE_ADMIN.name());
        assertThat(user.getRoles().size()).isGreaterThan(0);
        assertThat(user.getVersion()).isEqualTo(0);
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#existsByUsername(java.lang.String)}.
     */
    @Test
    public void testExistsByUsername()
    {
        final String loginToSearch = "anonymous1";
        final Boolean result = this.userRepository.existsByUsername(loginToSearch);

        assertThat(result).isNotNull();
        assertThat(result).isTrue();
    }

    @Test
    public void testExistsByUsernameFalse()
    {
        final String loginToSearch = "anonymous";
        final Boolean result = this.userRepository.existsByUsername(loginToSearch);

        assertThat(result).isNotNull();
        assertThat(result).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#existsByUsername(java.lang.String)}.
     */
    @Test
    public void testFindOneByEmailIgnoreCase()
    {
        final String emailToSearch = "admin1.test@live.fr";
        final Optional<User> result = this.userRepository.findOneByEmailIgnoreCase(emailToSearch);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getUsername()).isEqualTo(this.creerJeuDeDonnees().get(0).getUsername());
        assertThat(result.get().getId()).isEqualTo(1);
    }

    @Test
    public void testFindOneByEmailIgnoreCase_()
    {
        final String emailToSearch = "admin1_478.test@live.fr";
        final Optional<User> result = this.userRepository.findOneByEmailIgnoreCase(emailToSearch);

        assertThat(result.isPresent()).isFalse();
        assertThat(result).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#findOneWithRolesById(java.lang.Long)}.
     */
    @Test
    public void testFindOneWithRolesById()
    {
        final Long idToSearch = 2L;
        final Optional<User> result = this.userRepository.findOneWithRolesById(idToSearch);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getUsername()).isEqualTo(this.creerJeuDeDonnees().get(1).getUsername());
        assertThat(result.get().getId()).isEqualTo(2);
    }

    @Test
    public void testFindOneWithRolesByIdShouldReturnEmpty()
    {
        final Long idToSearch = Long.MAX_VALUE;
        final Optional<User> result = this.userRepository.findOneWithRolesById(idToSearch);

        assertThat(result.isPresent()).isFalse();
        assertThat(result).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#findOneWithRolesByUsername(java.lang.String)}.
     */
    @Test
    public void testFindOneWithRolesByUsername()
    {
        final String loginToSearch = "anonymous2";
        final Optional<User> result = this.userRepository.findOneWithRolesByUsername(loginToSearch);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getUsername()).isEqualTo(this.creerJeuDeDonnees().get(5).getUsername());
        assertThat(result.get().getId()).isEqualTo(6);
    }

    @Test
    public void testFindOneWithRolesByUsernameShouldReturnEmpty()
    {
        final String loginToSearch = "anonymous";
        final Optional<User> result = this.userRepository.findOneWithRolesByUsername(loginToSearch);

        assertThat(result.isPresent()).isFalse();
        assertThat(result).isEmpty();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.repository.UserRepository#findOneWithRolesByEmailIgnoreCase(java.lang.String)}.
     */
    @Test
    public void testFindOneWithRolesByEmailIgnoreCase()
    {
        final String emailToSearch = "client2.test@live.fr";
        final Optional<User> result = this.userRepository.findOneByEmailIgnoreCase(emailToSearch);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getUsername()).isEqualTo(this.creerJeuDeDonnees().get(3).getUsername());
        assertThat(result.get().getId()).isEqualTo(4);
    }

    @Test
    public void testFindOneWithRolesByEmailIgnoreCaseShouldReturnEmpty()
    {

        final String emailToSearch = "admin1_478.test@live.fr";
        final Optional<User> result = this.userRepository.findOneByEmailIgnoreCase(emailToSearch);

        assertThat(result.isPresent()).isFalse();
        assertThat(result).isEmpty();
    }

    @Test
    public void testFindAllUsers()
    {
        // this.initDatabase();
        final List<User> users = this.userRepository.findAll();

        assertThat(users).isNotEmpty();
        assertThat(users.size()).isEqualTo(6);
    }

    @Test
    public void testSaveNewUser()
    {
        final Set<RoleNameEnum> anonymousRole = new HashSet<>();
        anonymousRole.add(RoleNameEnum.ROLE_ANONYMOUS);

        final User user = new User();
        user.setUsername("login_new");
        user.setPassword(this.bCryptPasswordEncoder.encode("new_paswword"));
        user.setEmail("nwe-user.test@live.fr");
        user.setActive(Boolean.FALSE);
        user.setDateCreation(null);
        user.setHorodatage(null);
        user.setRoles(anonymousRole);

        final User savedUser = this.userRepository.save(user);

        // retrieve all users
        final List<User> users = this.userRepository.findAll();

        assertThat(users).isNotEmpty();
        assertThat(users.size()).isGreaterThan(0);
        assertThat(users).contains(savedUser);
    }

    /**
     * Ajouter les informations du jeu de données dans la table en base de données
     */
    @SuppressWarnings("unused")
    private void initDatabase()
    {
        for (User user : creerJeuDeDonnees())
        {
            this.userRepository.saveAndFlush(user);
        }
    }

    /**
     * Fournir le jeu de données pour intialisser la base de données pour les tests unitaires
     * 
     * @return
     */
    private List<User> creerJeuDeDonnees()
    {
        final List<User> users = Lists.newArrayList();

        // ADMIN
        final Set<RoleNameEnum> adminRole = new HashSet<>();
        adminRole.add(RoleNameEnum.ROLE_ADMIN);
        final User admin = new User()//
        .username("admin1").password(this.bCryptPasswordEncoder.encode("admin1_19511982"))//
        .email("admin1.test@live.fr").active(Boolean.TRUE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(adminRole);
        users.add(0, admin);

        // ADMIN INACTIF
        final User adminInactif = new User()//
        .username("admin2").password(this.bCryptPasswordEncoder.encode("admin2_19821951#"))//
        .email("admin2.test@live.fr").active(Boolean.FALSE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(adminRole);
        users.add(1, adminInactif);

        // CLIENT
        final Set<RoleNameEnum> clientRole = new HashSet<>();
        clientRole.add(RoleNameEnum.ROLE_CLIENT);
        final User client = new User()//
        .username("client1").password(this.bCryptPasswordEncoder.encode("client1_19511982"))//
        .email("client1.test@live.fr").active(Boolean.TRUE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(clientRole);
        users.add(2, client);

        // CLIENT INACTIF
        final User clientInactif = new User()//
        .username("client2").password(this.bCryptPasswordEncoder.encode("client2_19821951#"))//
        .email("client2.test@live.fr").active(Boolean.FALSE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(clientRole);
        users.add(3, clientInactif);

        // ANONYMOUS
        final Set<RoleNameEnum> anonymousRole = new HashSet<>();
        anonymousRole.add(RoleNameEnum.ROLE_ANONYMOUS);
        final User anonymous = new User()//
        .username("anonymous1").password(this.bCryptPasswordEncoder.encode("anonymous1_19511982"))//
        .email("anonymous1.test@live.fr").active(Boolean.TRUE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(anonymousRole);
        users.add(4, anonymous);

        // ANONYMOUS INACTIF
        final User anonymousInactif = new User()//
        .username("anonymous2").password(this.bCryptPasswordEncoder.encode("anonymous2_19821951#"))//
        .email("anonymous2.test@live.fr").active(Boolean.FALSE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(anonymousRole);
        users.add(5, anonymousInactif);

        return users;
    }
}
