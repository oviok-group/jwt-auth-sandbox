/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : UserRepositoryIT.java
 * Date de création : 19 oct. 2020
 * Heure de création : 13:01:11
 * Package : fr.vincent.tuto.repository
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.config.DatabaseAccesConfig;
import fr.vincent.tuto.config.JwtAuthSandboxBaseConfig;
import fr.vincent.tuto.model.po.User;
import fr.vincent.tuto.service.props.DatabasePropsService;

/**
 * Databse informations access {@link UserRepository} integration tests.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "userRepositoryIT", classes = { JwtAuthSandboxBaseConfig.class,
        DatabasePropsService.class, DatabaseAccesConfig.class })
@ActiveProfiles("test")
@Sql(scripts = { "classpath:scripts/schema/schema-h2.sql",
        "classpath:scripts/data/data-h2.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
@SpringBootTest
public class UserRepositoryIT
{
    //
    @Autowired
    private UserRepository userRepository;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        //
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.userRepository = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#findAllByActiveIsFalse()}.
     */
    @Test
    public void testGetAllInactiveUSers()
    {
        final List<User> inactiveUsers = (List<User>) this.userRepository.findAllByActiveIsFalse();

        assertThat(inactiveUsers).isNotEmpty();
        assertThat(inactiveUsers.size()).isEqualTo(3);
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#findAllByActiveIsTrue()}.
     */
    @Test
    public void testGetAllActiveUsers()
    {
        final List<User> activeUsers = (List<User>) this.userRepository.findAllByActiveIsTrue();

        assertThat(activeUsers).isNotEmpty();
        assertThat(activeUsers.size()).isEqualTo(3);
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#findByUsername(java.lang.String)}.
     */
    @Test
    public void testFindByUsername()
    {
        //
        final String loginToSearch = "client2";
        final Optional<User> optional = this.userRepository.findOneByUsername(loginToSearch);

        final User user = optional.get();
        assertThat(user).isNotNull();

        assertThat(user.getId()).isGreaterThan(0);
        assertThat(user.getUsername()).isEqualTo(loginToSearch);
        assertThat(user.getPassword()).startsWith("$2a$10$");
        assertThat(user.getEmail()).contains(".test@live.fr");
        assertThat(user.getActive()).isFalse();
        assertThat(user.getDateCreation()).isBefore(LocalDate.now().plusYears(1));
        assertThat(user.getHorodatage()).isBefore(LocalDateTime.now());
        assertThat(user.getRoles()).isNotEmpty();
        assertThat(user.getRoles().size()).isEqualTo(1);
        assertThat(user.getVersion()).isEqualTo(0);
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#existsByUsername(java.lang.String)}.
     */
    @Test
    public void testExistsByUsername()
    {
        final String loginToSearch = "anonymous1";
        final Boolean result = this.userRepository.existsByUsername(loginToSearch);

        assertThat(result).isNotNull();
        assertThat(result).isTrue();
    }

    @Test
    public void testExistsByUsernameFalse()
    {
        final String loginToSearch = "anonymous";
        final Boolean result = this.userRepository.existsByUsername(loginToSearch);

        assertThat(result).isNotNull();
        assertThat(result).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#findOneWithRolesById(java.lang.Long)}.
     */
    @Test
    public void testFindOneWithRolesById()
    {
        final Long idToSearch = 2L;
        final Optional<User> result = this.userRepository.findOneWithRolesById(idToSearch);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getUsername()).isEqualTo("admin2");
        assertThat(result.get().getId()).isEqualTo(2);
    }

    @Test
    public void testFindOneWithRolesByIdShouldReturnEmpty()
    {
        final Long idToSearch = Long.MAX_VALUE;
        final Optional<User> result = this.userRepository.findOneWithRolesById(idToSearch);

        assertThat(result.isPresent()).isFalse();
        assertThat(result).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.repository.UserRepository#findOneWithRolesByUsername(java.lang.String)}.
     */
    @Test
    public void testFindOneWithRolesByUsername()
    {
        final String loginToSearch = "anonymous2";
        final Optional<User> result = this.userRepository.findOneWithRolesByUsername(loginToSearch);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getUsername()).isEqualTo(loginToSearch);
        assertThat(result.get().getId()).isEqualTo(6);
    }

    @Test
    public void testFindOneWithRolesByUsernameShouldReturnEmpty()
    {
        final String loginToSearch = "anonymous";
        final Optional<User> result = this.userRepository.findOneWithRolesByUsername(loginToSearch);

        assertThat(result.isPresent()).isFalse();
        assertThat(result).isEmpty();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.repository.UserRepository#findOneWithRolesByEmailIgnoreCase(java.lang.String)}.
     */
    @Test
    public void testFindOneWithRolesByEmailIgnoreCase()
    {
        final String emailToSearch = "client2.test@live.fr";
        final Optional<User> result = this.userRepository.findOneByEmailIgnoreCase(emailToSearch);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getUsername()).isEqualTo("client2");
        assertThat(result.get().getId()).isEqualTo(4);
    }

    @Test
    public void testFindOneWithRolesByEmailIgnoreCaseShouldReturnEmpty()
    {

        final String emailToSearch = "admin1_478.test@live.fr";
        final Optional<User> result = this.userRepository.findOneByEmailIgnoreCase(emailToSearch);

        assertThat(result.isPresent()).isFalse();
        assertThat(result).isEmpty();
    }
}
