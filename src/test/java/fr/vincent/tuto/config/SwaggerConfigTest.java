/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : SwaggerConfigTest.java
 * Date de création : 3 nov. 2020
 * Heure de création : 13:36:10
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.constant.AuthAppConstant;
import fr.vincent.tuto.service.props.DatabasePropsService;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Unit tests class for coverage of {@link SwaggerConfig}.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "swaggerConfigTest", classes = { JwtAuthSandboxBaseConfig.class,
        DatabasePropsService.class, DatabaseAccesConfig.class, SwaggerAuthPropsService.class,
        SwaggerConfig.class })
@SpringBootTest
@ActiveProfiles("test")
public class SwaggerConfigTest
{

    @Autowired
    private SwaggerConfig swaggerConfig;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.swaggerConfig = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.SwaggerConfig#defaultControllerDocket()}.
     */
    @Test
    public void testDefaultControllerDocket()
    {
        final Docket defaultController = this.swaggerConfig.defaultControllerDocket();
        
        assertThat(defaultController).isNotNull();
        assertThat(defaultController.getGroupName()).isNotEmpty();
        assertThat(defaultController.getGroupName()).isEqualTo(AuthAppConstant.DEFAULT_GROUPENAME);
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.SwaggerConfig#userControllerDocket()}.
     */
    @Test
    public void testUserControllerDocket()
    {
        final Docket userController = this.swaggerConfig.userControllerDocket();

        assertThat(userController).isNotNull();
        assertThat(userController.getGroupName()).isNotEmpty();
        assertThat(userController.getGroupName()).isEqualTo(AuthAppConstant.USER_GROUPENAME);
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.SwaggerConfig#accountControllerDocket()}.
     */
    @Test
    public void testAccountControllerDocket()
    {
        final Docket accountController = this.swaggerConfig.accountControllerDocket();

        assertThat(accountController).isNotNull();
        assertThat(accountController.getGroupName()).isNotEmpty();
        assertThat(accountController.getGroupName()).isEqualTo(AuthAppConstant.ACCOUNT_GROUPENAME);
    }

    @Test
    public void testNotNullResources()
    {
        assertThat(this.swaggerConfig).isNotNull();
        assertThat(this.swaggerConfig.getSwaggerAuthPropsService()).isNotNull();
    }

}
