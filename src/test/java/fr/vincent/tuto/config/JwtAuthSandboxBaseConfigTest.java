/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : JwtAuthSandboxBaseConfigTest.java
 * Date de création : 18 oct. 2020
 * Heure de création : 16:12:53
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * {@link JwtAuthSandboxBaseConfig} unit tests class.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "jwtAuthSandboxBaseConfigTest", classes = { JwtAuthSandboxBaseConfig.class })
@ActiveProfiles("test")
@SpringBootTest
public class JwtAuthSandboxBaseConfigTest
{
    @Autowired
    private JwtAuthSandboxBaseConfig jwtAuthSandboxBaseConfig;

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.jwtAuthSandboxBaseConfig = null;
    }

    @Test
    public void testJwtAuthBCrypt()
    {
        //
        final String password = BCrypt.hashpw("secret-password-key", BCrypt.gensalt(12));
        // System.err.println(">>>>> le Hash du mot de passe est : \n" + password);

        final boolean verif = BCrypt.checkpw("secret-password-key", password);
        // System.err.println(">>>>> verif est t : \n" + verif);

        assertThat(password).isNotEmpty();
        assertThat(password).startsWith("$");
        assertThat(verif).isTrue();
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.JwtAuthSandboxBaseConfig#bCryptPasswordEncoder()}.
     */
    @Test
    public void testBCryptPasswordEncoder()
    {
        final BCryptPasswordEncoder passwordEncoder = this.jwtAuthSandboxBaseConfig.bCryptPasswordEncoder();

        assertThat(passwordEncoder).isNotNull();

        String secret = "admin1_19511982";
        final String hashedPassword = passwordEncoder.encode(secret);

        assertThat(hashedPassword).isNotEmpty();
        assertThat(hashedPassword).startsWith("$");
    }

}
