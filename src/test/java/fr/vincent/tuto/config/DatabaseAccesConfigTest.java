/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : DatabaseAccesConfigTest.java
 * Date de création : 17 oct. 2020
 * Heure de création : 02:54:08
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.config;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.SharedEntityManagerBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;

import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.HikariPool;
import com.zaxxer.hikari.pool.HikariProxyConnection;

import fr.vincent.tuto.exception.TechnicalAppError;
import fr.vincent.tuto.service.props.DatabasePropsService;

/**
 * {@link DatabaseAccesConfig} unit test class.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "databasePropsServiceTest", classes = { JwtAuthSandboxBaseConfig.class,
        DatabasePropsService.class, DatabaseAccesConfig.class })
@SpringBootTest
@ActiveProfiles("test")
public class DatabaseAccesConfigTest
{
    //
    private static final String DRIVER_CLASS_NAME = "org.h2.Driver";
    private static final String PERSIT_UNIT_NAME = "jwt-auth-sandbox-pu-test";

    @Autowired
    private DatabaseAccesConfig accessConfig;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.accessConfig = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.DatabaseAccesConfig#dataSource()}.
     * 
     * @throws Exception
     */
    @Test
    public void testDataSource() throws Exception
    {
        final HikariDataSource dataSource = this.accessConfig.dataSource();

        assertThat(dataSource).isNotNull();
        assertThat(dataSource.getClass()).isSameAs(HikariDataSource.class);
        assertThat(dataSource.getConnectionTimeout()).isGreaterThan(0);
        assertThat(dataSource.getDriverClassName()).isEqualTo(DRIVER_CLASS_NAME);
        assertThat(dataSource.getIdleTimeout()).isGreaterThan(0);
    }

    @Test(expected = TechnicalAppError.class)
    public void testDataSourceShouldCatchException() throws Exception
    {
        DatabaseAccesConfig config = new DatabaseAccesConfig();
        config.setDatabasePropsService(null);
        final HikariDataSource dataSource = config.dataSource();

        assertThat(dataSource).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.DatabaseAccesConfig#hikariPool()}.
     * 
     * @throws SQLException
     */
    @Test
    public void testHikariPool() throws SQLException
    {
        final HikariPool hikariPool = this.accessConfig.hikariPool();

        assertThat(hikariPool).isNotNull();
        assertThat(hikariPool.getClass()).isSameAs(HikariPool.class);
        assertThat(hikariPool.getIdleConnections()).isGreaterThan(0);
        assertThat(hikariPool.poolState).isEqualTo(0);
        assertThat(hikariPool.getConnection()).isExactlyInstanceOf(HikariProxyConnection.class);
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.DatabaseAccesConfig#entityManagerFactory()}.
     */
    @Test
    public void testEntityManagerFactory()
    {
        final LocalContainerEntityManagerFactoryBean bean = this.accessConfig.entityManagerFactory();

        assertThat(bean).isNotNull();
        assertThat(bean.getDataSource()).isNotNull();
        assertThat(bean.getDataSource()).isExactlyInstanceOf(HikariDataSource.class);
        assertThat(bean.getJpaDialect()).isExactlyInstanceOf(HibernateJpaDialect.class);
        assertThat(bean.getJpaVendorAdapter()).isExactlyInstanceOf(HibernateJpaVendorAdapter.class);
        assertThat(bean.getPersistenceUnitName()).isEqualTo(PERSIT_UNIT_NAME);

    }

    /**
     * Test method for {@link fr.vincent.tuto.config.DatabaseAccesConfig#sharedEntityManagerBean()}.
     */
    @Test
    public void testSharedEntityManagerBean()
    {
        final SharedEntityManagerBean sharedEntity = this.accessConfig.sharedEntityManager();

        assertThat(sharedEntity).isNotNull();
        assertThat(sharedEntity.getEntityManagerFactory()).isNotNull();
        assertThat(sharedEntity.getPersistenceUnitName()).isNull();
        assertThat(sharedEntity.getObject()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.DatabaseAccesConfig#transactionManager()}.
     */
    @Test
    public void testTransactionManager()
    {
        final PlatformTransactionManager transactionManager = this.accessConfig.transactionManager();

        assertThat(transactionManager).isNotNull();
        assertThat(transactionManager).isExactlyInstanceOf(JpaTransactionManager.class);
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.DatabaseAccesConfig#hibernatJpaVendorAdapter()}.
     */
    @Test
    public void testHibernatJpaVendorAdapter()
    {
        final JpaVendorAdapter vendorAdapter = this.accessConfig.hibernatJpaVendorAdapter();

        assertThat(vendorAdapter).isNotNull();
        assertThat(vendorAdapter).isExactlyInstanceOf(HibernateJpaVendorAdapter.class);
        assertThat(vendorAdapter.getJpaDialect()).isNotNull();
        assertThat(vendorAdapter.getJpaPropertyMap()).isNotEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.DatabaseAccesConfig#hibernatJpaDialect()}.
     */
    @Test
    public void testHibernatJpaDialect()
    {
        final JpaDialect dialect = this.accessConfig.hibernatJpaDialect();

        assertThat(dialect).isNotNull();
        assertThat(dialect).isExactlyInstanceOf(HibernateJpaDialect.class);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.config.DatabaseAccesConfig#persistenceExceptionTranslationPostProcessor()}.
     */
    @Test
    public void testPersistenceExceptionTranslationPostProcessor()
    {
        final PersistenceExceptionTranslationPostProcessor processor = this.accessConfig
        .exceptionTranslationPostProcessor();

        assertThat(processor).isNotNull();
        assertThat(processor).isExactlyInstanceOf(PersistenceExceptionTranslationPostProcessor.class);
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.DatabaseAccesConfig#sqlExceptionTranslator()}.
     */
    @Test
    public void testSqlExceptionTranslator()
    {
        final SQLErrorCodeSQLExceptionTranslator codeSQLExceptionTranslator = this.accessConfig
        .sqlExceptionTranslator();

        assertThat(codeSQLExceptionTranslator).isNotNull();
        assertThat(codeSQLExceptionTranslator).isExactlyInstanceOf(SQLErrorCodeSQLExceptionTranslator.class);
    }

    @Test
    public void testRessourceIsNotNull()
    {
        assertThat(this.accessConfig).isNotNull();
        assertThat(this.accessConfig.getDatabasePropsService()).isNotNull();
    }

}
