/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : JwtAuthEnvAddressConfigTest.java
 * Date de création : 1 nov. 2020
 * Heure de création : 18:27:43
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.config;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.UnknownHostException;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import fr.vincent.tuto.service.props.DatabasePropsService;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService;

/**
 * Unit tests class coverage for {@link JwtAuthEnvAddressConfig}.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "jwtAuthEnvAddressConfigTest", classes = { JwtAuthSandboxBaseConfig.class,
        DatabasePropsService.class, DatabaseAccesConfig.class, SwaggerAuthPropsService.class,
        JwtAuthEnvAddressConfig.class })
@SpringBootTest
@ActiveProfiles("test")
public class JwtAuthEnvAddressConfigTest
{
    //
    @Autowired
    private JwtAuthEnvAddressConfig jwtAuthEnvAddressConfig;

    @Value("${votjwt.swagger-props.nom-machine-dev}")
    private String hostAdresse;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        //
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.jwtAuthEnvAddressConfig = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.JwtAuthEnvAddressConfig#getHostAddress()}.
     */
    @Test
    public void testGetHostAddress()
    {
        final String host = this.jwtAuthEnvAddressConfig.getHostAddress();

        assertThat(host).isNotEmpty();
        assertThat(host.length()).isGreaterThan(0);
        assertThat(host).isEqualTo(this.hostAdresse.trim());
    }

    @Test
    public void testGetHostAddressWithNull() throws UnknownHostException
    {
        ReflectionTestUtils.setField(this.jwtAuthEnvAddressConfig, "hostAddress", null);
        final String host = this.jwtAuthEnvAddressConfig.getHostAddress();

        assertThat(host).isNull();
    }

    @Test
    public void testGetHostAddressWithEmpty() throws UnknownHostException
    {
        ReflectionTestUtils.setField(this.jwtAuthEnvAddressConfig, "hostAddress", StringUtils.EMPTY);
        final String host = this.jwtAuthEnvAddressConfig.getHostAddress();

        assertThat(host).isNull();
    }

    @Test
    public void testGetHostAddressWithWrong() throws UnknownHostException
    {
        ReflectionTestUtils.setField(this.jwtAuthEnvAddressConfig, "hostAddress", "Wrong_value");
        final String host = this.jwtAuthEnvAddressConfig.getHostAddress();

        assertThat(host).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.JwtAuthEnvAddressConfig#isLocalHost()}.
     */
    @Test
    public void testIsLocalHost()
    {
        final Boolean isValidHost = this.jwtAuthEnvAddressConfig.isLocalHost();

        assertThat(isValidHost).isNotNull();
        assertThat(isValidHost).isTrue();
    }

    @Test
    public void testIsLocalHostFalse()
    {
        ReflectionTestUtils.setField(this.jwtAuthEnvAddressConfig, "hostAddress", "Wrong_value");

        final Boolean isValidHost = this.jwtAuthEnvAddressConfig.isLocalHost();

        assertThat(isValidHost).isNotNull();
        assertThat(isValidHost).isFalse();
    }

    @Test(expected = NullPointerException.class)
    public void testIsLocalHostShouldReturnException()
    {
        ReflectionTestUtils.setField(this.jwtAuthEnvAddressConfig, "hostAddress", "Wrong_value");

        final Boolean isValidHost = this.jwtAuthEnvAddressConfig.isLocalHost();

        assertThat(isValidHost).isNotNull();
        assertThat(isValidHost).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.JwtAuthEnvAddressConfig#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.jwtAuthEnvAddressConfig.afterPropertiesSet();
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.JwtAuthEnvAddressConfig#destroy()}.
     * 
     * @throws Exception
     */
    @Test
    public void testDestroy() throws Exception
    {
        this.jwtAuthEnvAddressConfig.destroy();
    }
}
