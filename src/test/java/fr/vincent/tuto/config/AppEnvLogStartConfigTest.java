/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AppEnvLogStartConfigTest.java
 * Date de création : 2 nov. 2020
 * Heure de création : 03:25:28
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.config;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.JwtAuthSandboxApp;
import fr.vincent.tuto.service.props.DatabasePropsService;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService;

/**
 * Unit test class coverage for {@link AppEnvLogStartConfig}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "appEnvLogStartConfigTest", classes = { JwtAuthSandboxBaseConfig.class,
        DatabasePropsService.class, DatabaseAccesConfig.class, SwaggerAuthPropsService.class,
        AppEnvLogStartConfig.class })
@SpringBootTest
@ActiveProfiles(profiles = { "test", "prod", "dev", "cloud" })
public class AppEnvLogStartConfigTest
{
    //
    @Autowired
    private ConfigurableEnvironment environment;

    @Autowired
    private AppEnvLogStartConfig appEnvLogStartConfig;

    // @Autowired
    // private SpringApplication springApplication;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.appEnvLogStartConfig = null;
        this.environment = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.AppEnvLogStartConfig#logStartUp(org.springframework.core.env.Environment)}.
     */
    @Test
    public void testLogStartUp()
    {
        AppEnvLogStartConfig.logStartUp(this.environment);
    }

    @Test(expected = NullPointerException.class)
    public void testLogStartUpWithNull()
    {
        AppEnvLogStartConfig.logStartUp(null);
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.AppEnvLogStartConfig#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.appEnvLogStartConfig.afterPropertiesSet();
    }

    /**
     * Test method for {@link fr.vincent.tuto.config.AppEnvLogStartConfig#addDefaultProfile(org.springframework.boot.SpringApplication)}.
     */
    @Test
    public void testAddDefaultProfile()
    {
        final SpringApplication application = new SpringApplication(JwtAuthSandboxApp.class);
        AppEnvLogStartConfig.addDefaultProfile(application);
    }

    @Test(expected = NullPointerException.class)
    public void testAddDefaultProfileWithNull()
    {
        AppEnvLogStartConfig.addDefaultProfile(null);
    }
}
