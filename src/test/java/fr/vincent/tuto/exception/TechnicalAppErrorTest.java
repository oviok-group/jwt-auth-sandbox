/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : TechnicalAppErrorTest.java
 * Date de création : 17 oct. 2020
 * Heure de création : 00:05:20
 * Package : fr.vincent.tuto.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.exception;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * {@link TechnicalAppError} unit tests class.
 * 
 * @author Vincent Otchoun
 */
public class TechnicalAppErrorTest
{
    
    private TechnicalAppError error;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.error = new TechnicalAppError();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.error = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.exception.TechnicalAppError#TechnicalAppError()}.
     */
    @Test
    public void testTechnicalAppError()
    {
        // AssertJ assertion's
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getLocalizedMessage()).isNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.exception.TechnicalAppError#TechnicalAppError(java.lang.String, java.lang.Throwable)}.
     */
    @Test
    public void testTechnicalAppErrorStringThrowable()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        error = new TechnicalAppError(message, throwable);
        //
        assertThat(this.error.getCause().getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.error.getCause()).isEqualTo(throwable);
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
        assertThat(this.error.getLocalizedMessage()).isNotNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    @Test
    public void testTechnicalAppErrorStringThrowable_Null()
    {
        error = new TechnicalAppError((String) null, (Throwable) null);
        //
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getLocalizedMessage()).isNull();
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
    }
    /**
     * Test method for {@link fr.vincent.tuto.exception.TechnicalAppError#TechnicalAppError(java.lang.String)}.
     */
    @Test
    public void testTechnicalAppErrorString()
    {
        final String message = "Message Erreur Exception";
        error = new TechnicalAppError(message);

        //
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.error.getLocalizedMessage()).isNotNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    @Test
    public void testTechnicalAppErrorString_Null()
    {
        error = new TechnicalAppError((String) null);

        // AssertJ assertion's
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getLocalizedMessage()).isNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.exception.TechnicalAppError#TechnicalAppError(java.lang.Throwable)}.
     */
    @Test
    public void testTechnicalAppErrorThrowable()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        error = new TechnicalAppError(throwable);
        //
        assertThat(this.error.getCause()).isNotNull();
        assertThat(this.error.getCause().getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.error.getCause()).isNotNull();
        assertThat(this.error.getLocalizedMessage()).isNotNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    @Test
    public void testTechnicalAppErrorThrowable_Null()
    {
        error = new TechnicalAppError((Throwable) null);

        // AssertJ assertion's
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getStackTrace()).isNotNull();
        assertThat(this.error.getLocalizedMessage()).isNull();
    }

}
