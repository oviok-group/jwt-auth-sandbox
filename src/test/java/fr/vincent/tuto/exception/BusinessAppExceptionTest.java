/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : BusinessAppExceptionTest.java
 * Date de création : 18 oct. 2020
 * Heure de création : 16:05:35
 * Package : fr.vincent.tuto.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.exception;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

/**
 * {@link BusinessAppException} init tests class.
 * 
 * @author Vincent Otchoun
 */
public class BusinessAppExceptionTest
{

    private BusinessAppException error;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        // this.execption =new businessa
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.error = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.exception.BusinessAppException#BusinessAppException(java.lang.String)}.
     */
    @Test
    public void testBusinessAppExceptionString()
    {
        final String message = "Message Erreur Exception";
        this.error = new BusinessAppException(message);

        //
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.error.getLocalizedMessage()).isNotNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    @Test
    public void testBusinessAppExceptionString_Null()
    {
        this.error = new BusinessAppException((String) null);

        // AssertJ assertion's
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getLocalizedMessage()).isNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exception.BusinessAppException#BusinessAppException(java.lang.Long, java.lang.String)}.
     */
    @Test
    public void testBusinessAppExceptionLongString()
    {
        final String message = "Message Erreur Exception with ressourId";
        this.error = new BusinessAppException(1L, message);

        assertThat(this.error.getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
        assertThat(this.error.getLocalizedMessage()).isNotNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    @Test
    public void testBusinessAppExceptionLongString_Null()
    {
        this.error = new BusinessAppException((Long) null, null);
        //
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getLocalizedMessage()).isNull();
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exception.BusinessAppException#BusinessAppException(java.lang.Long, java.lang.String, java.lang.String)}.
     */
    @Test
    public void testBusinessAppExceptionLongStringString()
    {
        final String message = "Message Erreur Exception with ressourId and error code";
        this.error = new BusinessAppException(2L, "Erreur Saisie", message);

        assertThat(this.error.getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.error.getErrorCode()).isEqualToIgnoringCase("Erreur Saisie");
        assertThat(this.error.getResourceId()).isGreaterThan(1L);

        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
        assertThat(this.error.getLocalizedMessage()).isNotNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    @Test
    public void testBusinessAppExceptionLongStringString_Null()
    {
        this.error = new BusinessAppException((Long) null, null, null);

        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getErrorCode()).isNull();
        assertThat(this.error.getResourceId()).isNull();

        //
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getLocalizedMessage()).isNull();
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exception.BusinessAppException#BusinessAppException(java.lang.String, java.lang.String)}.
     */
    @Test
    public void testBusinessAppExceptionStringString()
    {
        final String message = "Message Erreur Exception with error code";
        this.error = new BusinessAppException("Erreur maj", message);

        assertThat(this.error.getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.error.getErrorCode()).isEqualToIgnoringCase("Erreur maj");
        assertThat(this.error.getResourceId()).isNull();

        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
        assertThat(this.error.getLocalizedMessage()).isNotNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    @Test
    public void testBusinessAppExceptionStringString_Null()
    {
        this.error = new BusinessAppException((String) null, (String) null);

        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getErrorCode()).isNull();
        assertThat(this.error.getResourceId()).isNull();

        //
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getLocalizedMessage()).isNull();
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exception.BusinessAppException#BusinessAppException(java.lang.String, java.lang.String, org.springframework.http.HttpStatus)}.
     */
    @Test
    public void testBusinessAppExceptionStringStringHttpStatus()
    {

        final String message = "Message Erreur Exception with error code and HTTP status code";
        this.error = new BusinessAppException("Erreur POST", message, HttpStatus.OK);

        assertThat(this.error.getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.error.getErrorCode()).isEqualToIgnoringCase("Erreur POST");
        assertThat(this.error.getStatus()).isEqualTo(HttpStatus.OK);
        assertThat(this.error.getResourceId()).isNull();

        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
        assertThat(this.error.getLocalizedMessage()).isNotNull();
        assertThat(this.error.getStackTrace()).isNotNull();
    }

    @Test
    public void testBusinessAppExceptionStringStringHttpStatus_Null()
    {
        this.error = new BusinessAppException((String) null, (String) null, null);

        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getErrorCode()).isNull();
        assertThat(this.error.getStatus()).isNull();
        assertThat(this.error.getResourceId()).isNull();

        //
        assertThat(this.error.getCause()).isNull();
        assertThat(this.error.getMessage()).isNull();
        assertThat(this.error.getLocalizedMessage()).isNull();
        assertThat(this.error.getStackTrace().length > 0).isEqualTo(true);
    }
}
