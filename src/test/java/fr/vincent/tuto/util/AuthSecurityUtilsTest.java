/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AuthSecurityUtilsTest.java
 * Date de création : 29 oct. 2020
 * Heure de création : 02:16:57
 * Package : fr.vincent.tuto.util
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import fr.vincent.tuto.TestsUtils;
import fr.vincent.tuto.enumeration.RoleNameEnum;

/**
 * Test class for {@link AuthSecurityUtils} utilities class.
 * 
 * @author Vincent Otchoun
 */
public class AuthSecurityUtilsTest
{
    //
    private static final String ADMIN = "admin";
    private static final String TOKEN = "token";
    private static final String ANONYMOUS = "anonymous";
    private static final String CLIENT = "client";
    private static final String AUTH_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbm9ueW1vdXMiLCJleHAiOjE2MDM4NzgyMzF9.-V9sx5fnyHT7mY1B0e1vp1MY-OVEGW_DvOKOhD6wtB-HEZOa6Je3z1huJyJchD-iIRtaeaZKaL8jGAanoqrlMQ";

    /**
     * Test method for {@link fr.vincent.tuto.util.AuthSecurityUtils#getCurrentUsername()}.
     */
    @Test
    public void testGetCurrentUsername()
    {
        final SecurityContext context = SecurityContextHolder.createEmptyContext();
        final Authentication authentication = this.creatieAuthentication(ADMIN, ADMIN);
        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final Optional<String> username = AuthSecurityUtils.getCurrentUsername();

        assertThat(username).isNotNull();
        assertThat(username.get()).isNotNull();
        assertThat(username.get()).isEqualToIgnoringCase(ADMIN);
    }

    @Test
    public void testGetCurrentUsernameWithUserDetails()
    {
        final SecurityContext context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(this.createRole(RoleNameEnum.ROLE_ANONYMOUS.getAuthority()));

        final User user = new User(ANONYMOUS, ANONYMOUS, authorities);
        final UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user,
        user.getPassword(), user.getAuthorities());
        context.setAuthentication(authenticationToken);
        SecurityContextHolder.setContext(context);

        final Optional<String> username = AuthSecurityUtils.getCurrentUsername();

        assertThat(username).isNotNull();
        assertThat(username.get()).isNotNull();
        assertThat(username.get()).isEqualToIgnoringCase(ANONYMOUS);
    }


    /**
     * Test method for {@link fr.vincent.tuto.util.AuthSecurityUtils#getCurrentUserJWTToken()}.
     */
    @Test
    public void testGetCurrentUserJWTToken()
    {
        final SecurityContext context = SecurityContextHolder.createEmptyContext();
        final Authentication authentication = this.creatieAuthentication(ADMIN, TOKEN);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final Optional<String> jwt = AuthSecurityUtils.getCurrentUserJWTToken();

        assertThat(jwt).isNotNull();
        assertThat(jwt.get()).isNotNull();
        assertThat(jwt.get()).isEqualToIgnoringCase(TOKEN);
    }

    /**
     * Test method for {@link fr.vincent.tuto.util.AuthSecurityUtils#isAuthenticated()}.
     */
    @Test
    public void testIsAuthenticated()
    {
        final SecurityContext context = SecurityContextHolder.createEmptyContext();
        final Authentication authentication = this.creatieAuthentication(ADMIN, TOKEN);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final Boolean isAuthenticated = AuthSecurityUtils.isAuthenticated();

        assertThat(isAuthenticated).isNotNull();
        assertThat(isAuthenticated).isTrue();
    }

    @Test
    public void testAnonymousIsNotAuthenticated()
    {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(this.createRole(RoleNameEnum.ROLE_ANONYMOUS.getAuthority()));
        
        final Authentication authentication = new UsernamePasswordAuthenticationToken(ANONYMOUS, ANONYMOUS,
        authorities);
        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        boolean isAuthenticated = AuthSecurityUtils.isAuthenticated();

        assertThat(isAuthenticated).isNotNull();
        assertThat(isAuthenticated).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.util.AuthSecurityUtils#isCurrentUserInRole(java.lang.String)}.
     */
    @Test
    public void testIsCurrentUserInRole()
    {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(this.createRole(RoleNameEnum.ROLE_CLIENT.getAuthority()));

        final Authentication authentication = new UsernamePasswordAuthenticationToken(CLIENT, CLIENT, authorities);
        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        assertThat(AuthSecurityUtils.isCurrentUserInRole(RoleNameEnum.ROLE_CLIENT.getAuthority())).isTrue();
        assertThat(AuthSecurityUtils.isCurrentUserInRole(RoleNameEnum.ROLE_ADMIN.getAuthority())).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.util.AuthSecurityUtils#isValidAuthorizationHeader(java.lang.String)}.
     */
    @Test
    public void testIsValidAuthorizationHeader()
    {
        //
        final Boolean isVald = AuthSecurityUtils.isValidAuthorizationHeader("Bearer " + AUTH_TOKEN,
        TestsUtils.AUTH_TOKEN_PREFIX);
        
        assertThat(isVald).isNotNull();
        assertThat(isVald).isTrue();
    }

    @Test
    public void testIsValidAuthorizationHeaderFalseWithUppercase()
    {
        //
        final Boolean isVald = AuthSecurityUtils.isValidAuthorizationHeader("BEARER " + AUTH_TOKEN,
        TestsUtils.AUTH_TOKEN_PREFIX);

        assertThat(isVald).isNotNull();
        assertThat(isVald).isFalse();
    }

    @Test
    public void testIsValidAuthorizationHeaderFalseWithUppercaseAndNoSpace()
    {
        //
        final Boolean isVald = AuthSecurityUtils.isValidAuthorizationHeader("Bearer" + AUTH_TOKEN,
        TestsUtils.AUTH_TOKEN_PREFIX);

        assertThat(isVald).isNotNull();
        assertThat(isVald).isFalse();
    }

    @Test
    public void testIsValidAuthorizationHeaderFalseWithoutPrefix()
    {
        //

        //
        final Boolean isVald = AuthSecurityUtils.isValidAuthorizationHeader(AUTH_TOKEN,
        TestsUtils.AUTH_TOKEN_PREFIX);

        assertThat(isVald).isNotNull();
        assertThat(isVald).isFalse();
    }

    @Test
    public void testIsValidAuthorizationHeaderFalseWithEmptyBearer()
    {
        //
        final Boolean isVald = AuthSecurityUtils.isValidAuthorizationHeader(StringUtils.EMPTY,
        TestsUtils.AUTH_TOKEN_PREFIX);

        assertThat(isVald).isNotNull();
        assertThat(isVald).isFalse();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.util.AuthSecurityUtils#tokenHeaderResolver(HttpServletRequest, java.lang.String,java.lang.String)}.
     */
    @Test
    public void testTokenHeaderResolver()
    {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(TestsUtils.AUTHORIZATION_HEADER, TestsUtils.AUTH_TOKEN_PREFIX + AUTH_TOKEN);

        final Optional<String> authBearer = AuthSecurityUtils.extractJwtFromRequest(request,
        TestsUtils.AUTHORIZATION_HEADER,
        TestsUtils.AUTH_TOKEN_PREFIX);

        // System.err.println(">>>>>> Auth Token \n" + authBearer.get());

        assertThat(authBearer).isNotNull();
        assertThat(authBearer.get()).isEqualTo(AUTH_TOKEN);
    }

    @Test
    public void testTokenHeaderResolverShouldReturnEmpty()
    {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(TestsUtils.AUTHORIZATION_HEADER, TestsUtils.AUTH_TOKEN_PREFIX + AUTH_TOKEN);

        final Optional<String> authBearer = AuthSecurityUtils.extractJwtFromRequest(request,
        TestsUtils.AUTHORIZATION_HEADER,
        "BEARER");

        assertThat(authBearer).isEmpty();
    }

    @Test
    public void testTokenHeaderResolverEmpty()
    {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(TestsUtils.AUTHORIZATION_HEADER, TestsUtils.TOKEN_PREFIX + AUTH_TOKEN);

        final Optional<String> authBearer = AuthSecurityUtils.extractJwtFromRequest(request,
        TestsUtils.AUTHORIZATION_HEADER,
        TestsUtils.AUTH_TOKEN_PREFIX);

        assertThat(authBearer).isEmpty();
    }


    /**
     * @param pPrincipal
     * @param pCredentials
     * @return
     */
    private UsernamePasswordAuthenticationToken creatieAuthentication(final String pPrincipal,
    final String pCredentials)
    {
        return new UsernamePasswordAuthenticationToken(pPrincipal, pCredentials);
    }

    /**
     * @param pRole
     * @return
     */
    private SimpleGrantedAuthority createRole(final String pRole)
    {
        return new SimpleGrantedAuthority(pRole);
    }

}
