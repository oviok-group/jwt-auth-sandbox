/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : DatabasePropsServiceTest.java
 * Date de création : 16 oct. 2020
 * Heure de création : 09:59:12
 * Package : fr.vincent.tuto.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.service.props;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.config.JwtAuthSandboxBaseConfig;
import fr.vincent.tuto.service.props.DatabasePropsService.DataSourceProps;
import fr.vincent.tuto.service.props.DatabasePropsService.HikariProps;
import fr.vincent.tuto.service.props.DatabasePropsService.JpaHibernateProps;

/**
 * {@link DatabasePropsService} unit tests class.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "databasePropsServiceTest", classes = { JwtAuthSandboxBaseConfig.class,
        DatabasePropsService.class })
@SpringBootTest
@ActiveProfiles("test")
public class DatabasePropsServiceTest
{
    //
    @Autowired
    private DatabasePropsService databasePropsService;


    /**
     * Test method for {@link fr.vincent.tuto.service.props.DatabasePropsService#getDataSourceProps()}.
     */
    @Test
    public void testGetDataSourceProps()
    {
        final DataSourceProps dataSource = this.databasePropsService.getDataSourceProps();

        assertThat(dataSource).isNotNull();

        assertThat(dataSource.getType()).isEqualToIgnoringCase("com.zaxxer.hikari.HikariDataSource");
        assertThat(dataSource.getDatasourceClassName()).isEqualToIgnoringCase("org.h2.jdbcx.JdbcDataSource");
        assertThat(dataSource.getDriverClassName()).isEqualToIgnoringCase("org.h2.Driver");
        assertThat(dataSource.getJdbcUrl()).contains("jdbc:h2:mem:");
        assertThat(dataSource.getUserName()).isEqualToIgnoringCase("sa");
        assertThat(dataSource.getPassword()).isEmpty();
        assertThat(dataSource.getTestWhileIdle()).isTrue();
        assertThat(dataSource.getPoolPreparedStatements()).isTrue();
        assertThat(dataSource.getValidationQuery()).isEqualToIgnoringCase("select 1");
        assertThat(dataSource.getMaxOpenPreparedStatements()).isGreaterThan(0);
        assertThat(dataSource.getMaxOpenPreparedStatements()).isEqualTo(250);
        assertThat(dataSource.getPersistenceUnitName()).isEqualTo("jwt-auth-sandbox-pu-test");
        assertThat(dataSource.getPackageToScan()).contains("fr.vincent.");
        assertThat(dataSource.getGenerateDdl()).isFalse();
        assertThat(dataSource.toString()).isNotEmpty();
        assertThat(dataSource.getPlatform()).isEqualTo("H2");

        //
        assertThat(dataSource.getCachePrepareStatements()).isEqualTo("cachePrepStmts");
        assertThat(dataSource.getPrepareStatementCacheSize()).isEqualTo("prepStmtCacheSize");
        assertThat(dataSource.getPrepareStatementCacheSqlLimit()).isEqualTo("prepStmtCacheSqlLimit");
        assertThat(dataSource.getUseServerPrepareStatements()).isEqualTo("useServerPrepStmts");

        //
        assertThat(dataSource.getInitializationMode()).isNotEmpty();
        assertThat(dataSource.getInitSchema()).isNotEmpty();
        assertThat(dataSource.getInitData()).isNotEmpty();
        assertThat(dataSource.getInitContinueOnError()).isFalse();
    }

    @Test
    public void testGetDataSourcePropsWithNull()
    {
        //
        final DataSourceProps dataSource = this.databasePropsService.getDataSourceProps();

        dataSource.setType(null);
        dataSource.setDriverClassName(null);
        dataSource.setJdbcUrl(null);
        dataSource.setUserName(null);
        dataSource.setPassword(null);
        dataSource.setTestWhileIdle(null);
        dataSource.setValidationQuery(null);
        dataSource.setPoolPreparedStatements(null);
        dataSource.setMaxOpenPreparedStatements(null);
        dataSource.setPersistenceUnitName(null);
        dataSource.setPackageToScan(null);
        dataSource.setPlatform(null);

        dataSource.setCachePrepareStatements(null);
        dataSource.setPrepareStatementCacheSize(null);
        dataSource.setPrepareStatementCacheSqlLimit(null);
        dataSource.setUseServerPrepareStatements(null);

        //
        dataSource.setInitializationMode(null);
        dataSource.setInitSchema(null);
        dataSource.setInitData(null);
        dataSource.setInitContinueOnError(null);

        assertThat(dataSource).isNotNull();
        assertThat(dataSource.getType()).isNull();
        assertThat(dataSource.getDriverClassName()).isNull();
        assertThat(dataSource.getJdbcUrl()).isNull();
        assertThat(dataSource.getUserName()).isNull();
        assertThat(dataSource.getPassword()).isNull();
        assertThat(dataSource.getTestWhileIdle()).isNull();
        assertThat(dataSource.getValidationQuery()).isNull();
        assertThat(dataSource.getPoolPreparedStatements()).isNull();
        assertThat(dataSource.getMaxOpenPreparedStatements()).isNull();
        assertThat(dataSource.getPersistenceUnitName()).isNull();
        assertThat(dataSource.getPersistenceUnitName()).isNull();
        assertThat(dataSource.getPackageToScan()).isNull();
        assertThat(dataSource.getPlatform()).isNull();

        assertThat(dataSource.getCachePrepareStatements()).isNull();
        assertThat(dataSource.getPrepareStatementCacheSize()).isNull();
        assertThat(dataSource.getPrepareStatementCacheSqlLimit()).isNull();
        assertThat(dataSource.getUseServerPrepareStatements()).isNull();

        //
        assertThat(dataSource.getInitializationMode()).isNull();
        assertThat(dataSource.getInitSchema()).isNull();
        assertThat(dataSource.getInitData()).isNull();
        assertThat(dataSource.getInitContinueOnError()).isNull();
    }


    /**
     * Test method for {@link fr.vincent.tuto.service.props.DatabasePropsService#getJpaHibernateProps()}.
     */
    @Test
    public void testGetJpaHibernateProps()
    {
        final JpaHibernateProps jpaHibernate = this.databasePropsService.getJpaHibernateProps();

        //
        assertThat(jpaHibernate).isNotNull();
        assertThat(jpaHibernate.getDatabaseName()).isEqualToIgnoringCase("H2");
        assertThat(jpaHibernate.getDialect()).contains("H2Dialect");
        assertThat(jpaHibernate.getShowSql()).isTrue();
        assertThat(jpaHibernate.getFormatSql()).isTrue();
        assertThat(jpaHibernate.getUseSql()).isTrue();
        assertThat(jpaHibernate.getUseSqlComments()).isTrue();
        assertThat(jpaHibernate.getUseReflectionOptimizer()).isFalse();
        assertThat(jpaHibernate.getBytecodeUseReflectionOptimizer()).isFalse();
        assertThat(jpaHibernate.getEnableLazy()).isTrue();
        assertThat(jpaHibernate.getNamingStrategy()).contains("ImprovedNamingStrategy");
        assertThat(jpaHibernate.getUseSecondLevelCache()).isFalse();
        assertThat(jpaHibernate.getGenerateStatistics()).isFalse();
        assertThat(jpaHibernate.getBytecodeUseReflectionOptimizer()).isFalse();
        assertThat(jpaHibernate.getDdlAuto()).contains("create");
        assertThat(jpaHibernate.toString()).isNotEmpty();
    }

    @Test
    public void testGetJpaHibernatePropsWithNull()
    {
        final JpaHibernateProps jpaHibernate = this.databasePropsService.getJpaHibernateProps();
        jpaHibernate.setDatabaseName(null);
        jpaHibernate.setDialect(null);
        jpaHibernate.setShowSql(null);
        jpaHibernate.setFormatSql(null);
        jpaHibernate.setUseSql(null);
        jpaHibernate.setUseSqlComments(null);
        jpaHibernate.setBytecodeUseReflectionOptimizer(null);
        jpaHibernate.setNamingStrategy(null);
        jpaHibernate.setUseSecondLevelCache(null);
        jpaHibernate.setGenerateStatistics(null);
        jpaHibernate.setBytecodeUseReflectionOptimizer(null);
        jpaHibernate.setDdlAuto(null);

        //
        assertThat(jpaHibernate).isNotNull();
        assertThat(jpaHibernate.getDatabaseName()).isNull();
        assertThat(jpaHibernate.getDialect()).isNull();
        assertThat(jpaHibernate.getShowSql()).isNull();
        assertThat(jpaHibernate.getFormatSql()).isNull();
        assertThat(jpaHibernate.getUseSql()).isNull();
        assertThat(jpaHibernate.getUseSqlComments()).isNull();
        assertThat(jpaHibernate.getBytecodeUseReflectionOptimizer()).isNull();
        assertThat(jpaHibernate.getNamingStrategy()).isNull();
        assertThat(jpaHibernate.getUseSecondLevelCache()).isNull();
        assertThat(jpaHibernate.getGenerateStatistics()).isNull();
        assertThat(jpaHibernate.getBytecodeUseReflectionOptimizer()).isNull();
        assertThat(jpaHibernate.getDdlAuto()).isNull();
    }


    /**
     * Test method for {@link fr.vincent.tuto.service.props.DatabasePropsService#getHikariProps()}.
     */
    @Test
    public void testGetHikariProps()
    {
        final HikariProps hikari = this.databasePropsService.getHikariProps();

        //
        assertThat(hikari).isNotNull();
        assertThat(hikari.getMinimumIdle()).isGreaterThan(0);
        assertThat(hikari.getMaximumPoolSize()).isGreaterThan(0);
        assertThat(hikari.getIdleTimeout()).isGreaterThan(0);
        assertThat(hikari.getPoolName()).isNotNull();
        assertThat(hikari.getPoolName()).isEqualToIgnoringCase("votjwtJPAHikariCPTest");
        assertThat(hikari.getMaxLifetime()).isGreaterThan(0);
        assertThat(hikari.getConnectionTimeout()).isGreaterThan(0);
        assertThat(hikari.toString()).isNotEmpty();

        assertThat(hikari.getCachePrepareStatements()).isTrue();
        assertThat(hikari.getPrepareStatementCacheSize()).isEqualTo(250);
        assertThat(hikari.getPrepareStatementCacheSqlLimit()).isEqualTo(2048);
        assertThat(hikari.getUseServerPrepareStatements()).isTrue();
    }

    @Test
    public void testGetHikariPropsWithNull()
    {
        final HikariProps hikari = this.databasePropsService.getHikariProps();
        hikari.setMinimumIdle(null);
        hikari.setMaximumPoolSize(null);
        hikari.setIdleTimeout(null);
        hikari.setPoolName(null);
        hikari.setMaxLifetime(null);
        hikari.setConnectionTimeout(null);

        hikari.setCachePrepareStatements(null);
        hikari.setPrepareStatementCacheSize(null);
        hikari.setPrepareStatementCacheSqlLimit(null);
        hikari.setUseServerPrepareStatements(null);

        //
        assertThat(hikari).isNotNull();
        assertThat(hikari.getMinimumIdle()).isNull();
        assertThat(hikari.getMaximumPoolSize()).isNull();
        assertThat(hikari.getIdleTimeout()).isNull();
        assertThat(hikari.getPoolName()).isNull();
        assertThat(hikari.getMaxLifetime()).isNull();
        assertThat(hikari.getConnectionTimeout()).isNull();

        assertThat(hikari.getCachePrepareStatements()).isNull();
        assertThat(hikari.getPrepareStatementCacheSize()).isNull();
        assertThat(hikari.getPrepareStatementCacheSqlLimit()).isNull();
        assertThat(hikari.getUseServerPrepareStatements()).isNull();
    }


    @Test
    public void testResourceIsNotNull()
    {
        assertThat(this.databasePropsService).isNotNull();
    }

}
