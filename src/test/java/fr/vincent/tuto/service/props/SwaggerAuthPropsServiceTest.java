/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : SwaggerAuthPropsServiceTest.java
 * Date de création : 25 oct. 2020
 * Heure de création : 23:03:26
 * Package : fr.vincent.tuto.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.service.props;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Base64;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;

import fr.vincent.tuto.config.JwtAuthSandboxBaseConfig;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService.AuthProps;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService.MailProps;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService.SwaggerProps;
import io.jsonwebtoken.io.Encoders;

/**
 * Unit tests for the {@link SwaggerAuthPropsService} class.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "otherAppPropsServiceTest", classes = { JwtAuthSandboxBaseConfig.class,
        SwaggerAuthPropsService.class })
@SpringBootTest
@ActiveProfiles("test")
public class SwaggerAuthPropsServiceTest
{
    //
    @Autowired
    private SwaggerAuthPropsService swaggerAuthPropsService;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        //
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.swaggerAuthPropsService = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.props.SwaggerAuthPropsService#getSwaggerProps()}.
     */
    @Test
    public void testGetSwaggerProps()
    {
        final SwaggerProps props = this.swaggerAuthPropsService.getSwaggerProps();

        assertThat(props).isNotNull();

        assertThat(props.getTitle()).isNotNull();
        assertThat(props.getTitle().trim()).isEqualTo("API Authentification avec JWT");
        assertThat(props.getDescription()).isNotNull();
        assertThat(props.getDescription().trim()).contains(">Gestion des droits :");
        // assertThat(props.getVersion()).isEmpty();
        assertThat(props.getVersion()).isNotNull();
        assertThat(props.getVersion().trim()).isEqualTo("3.0");
        assertThat(props.getTermeOfServiceUrl()).isNotNull();
        assertThat(props.getTermeOfServiceUrl().trim()).isEqualTo("1.0.0");
        assertThat(props.getContactName()).isNotNull();
        assertThat(props.getContactName().trim()).containsIgnoringCase("vincent");
        assertThat(props.getContactUrl().trim()).isNotNull();
        assertThat(props.getContactUrl().trim()).contains("tuto.vincent.fr");
        assertThat(props.getContactEmail()).isNotNull();
        assertThat(props.getContactEmail().trim()).contains("bleckyss31");
        assertThat(props.getLicence()).isNotNull();
        assertThat(props.getLicence().trim()).isEqualTo("Apache 2.0");
        assertThat(props.getLicenceUrl()).isNotNull();
        assertThat(props.getLicenceUrl().trim()).contains("http://www.apache.org/");
        assertThat(props.getDefaultIncludePattern()).isNotNull();
        assertThat(props.getDefaultIncludePattern().trim()).isEqualTo("1.0.0");
        assertThat(props.getHost()).isNotNull();
        assertThat(props.getHost().trim()).isEqualTo("host1");
        assertThat(props.getProtocols()).isNotEmpty();
        assertThat(props.getProtocols().size()).isEqualTo(3);
        assertThat(props.getProtocols().get(0).trim()).isEqualTo("protocol1");
        assertThat(props.getUseDefaultResponseMessages()).isNotNull();
        assertThat(props.getUseDefaultResponseMessages()).isFalse();
        assertThat(props.getCodeGeneration()).isNotNull();
        assertThat(props.getCodeGeneration()).isFalse();
        assertThat(props.getAppBasePackage()).isNotNull();
        assertThat(props.getAppBasePackage().trim()).isEqualTo("fr.vincent.tuto.controller");
        assertThat(props.getContexteRacine()).isEmpty();
        assertThat(props.getNomMachineDev()).isNotNull();
        assertThat(props.getNomMachineDev().trim()).isEqualToIgnoringCase("OVIOK");
        assertThat(props.getContexteRacineDefault()).isNotEmpty();
        assertThat(props.getContexteRacineDefault().trim()).isEqualTo("/");

        assertThat(props.toString()).isNotNull();
    }

    @Test
    public void testGetSwaggerProps_WithNullValues()
    {
        final SwaggerProps props = this.swaggerAuthPropsService.getSwaggerProps();

        props.setTitle(null);
        props.setDescription(null);
        props.setVersion(null);
        props.setTermeOfServiceUrl(null);
        props.setContactName(null);
        props.setContactUrl(null);
        props.setContactEmail(null);
        props.setLicence(null);
        props.setLicenceUrl(null);
        props.setDefaultIncludePattern(null);
        props.setHost(null);
        props.setProtocols(null);
        props.setUseDefaultResponseMessages(null);
        props.setCodeGeneration(null);
        props.setAppBasePackage(null);
        props.setContexteRacine(null);
        props.setNomMachineDev(null);
        props.setContexteRacineDefault(null);

        assertThat(props).isNotNull();
        assertThat(props.getTitle()).isNull();
        assertThat(props.getDescription()).isNull();
        assertThat(props.getVersion()).isNull();
        assertThat(props.getTermeOfServiceUrl()).isNull();
        assertThat(props.getContactName()).isNull();
        assertThat(props.getContactUrl()).isNull();
        assertThat(props.getContactEmail()).isNull();
        assertThat(props.getLicence()).isNull();
        assertThat(props.getLicenceUrl()).isNull();
        assertThat(props.getDefaultIncludePattern()).isNull();
        assertThat(props.getHost()).isNull();
        assertThat(props.getProtocols()).isNull();
        assertThat(props.getUseDefaultResponseMessages()).isNull();
        assertThat(props.getCodeGeneration()).isNull();
        assertThat(props.getAppBasePackage()).isNull();
        assertThat(props.getContexteRacine()).isNull();
        assertThat(props.getNomMachineDev()).isNull();
        assertThat(props.getContexteRacineDefault()).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.props.SwaggerAuthPropsService#getAuthProps()}.
     */
    @Test
    public void testGetAuthProps()
    {
        final AuthProps authProps = this.swaggerAuthPropsService.getAuthProps();

        assertThat(authProps).isNotNull();
        assertThat(authProps.getTokenSecret()).isNotNull();
        assertThat(authProps.getTokenSecret()).isEmpty();

        final String encodeSecretJwt = Encoders.BASE64.encode(authProps.getTokenSecret().getBytes());
        final String encodeSecret = Base64.getEncoder().encodeToString("SecretKeyToGenJWTs".getBytes());
        final String encodeSecretSpring = Base64Utils.encodeToString("SecretKeyToGenJWTs".getBytes());

        // System.err.println(">> le Token en base64 est : \n" + encodeSecretJwt);
        // System.err.println(">> le Token en base64 est : \n" + encodeSecret);
        // System.err.println(">> le Token en base64 Spring est : \n" + encodeSecretSpring);


        assertThat(encodeSecretJwt).isEmpty();
        assertThat(encodeSecret).isEqualTo("U2VjcmV0S2V5VG9HZW5KV1Rz");
        assertThat(encodeSecretSpring).isEqualTo("U2VjcmV0S2V5VG9HZW5KV1Rz");

        final byte[] decodeByte = Base64.getDecoder().decode(authProps.getBase64TokenSecret());
        final String decodeSecret = new String(decodeByte);
        final byte[] decodeByteSpring = Base64Utils.decodeFromString(authProps.getBase64TokenSecret());

        // System.err.println(">> le Token en base64 décode est : \n" + decodeSecret);
        // System.err.println(">> le Token en base64 Spring décode est : \n" + new String(decodeByteSpring));

        assertThat(authProps.getBase64TokenSecret()).isNotNull();
        assertThat(decodeSecret).isNotNull();
        assertThat(new String(decodeByteSpring)).isNotNull();
        assertThat(authProps.getTokenValidity()).isGreaterThan(0);
        assertThat(authProps.getTokenValidityForRememberMe()).isGreaterThan(0);

        assertThat(authProps.getRefreshTokenValidity()).isEqualTo(86400);

        assertThat(authProps.getAuthorizationHeader()).isEqualTo("Authorization");
        assertThat(authProps.getAuthoritiesKey()).isEqualTo("auth");
        assertThat(authProps.getBearerToken()).isEqualTo("Bearer ");

        assertThat(authProps.toString()).isNotEmpty();
    }

    @Test
    public void testGetAuthProps_WithNullElement()
    {
        final AuthProps authProps = this.swaggerAuthPropsService.getAuthProps();

        authProps.setTokenSecret(null);
        authProps.setBase64TokenSecret(null);
        authProps.setTokenValidity(null);
        authProps.setTokenValidityForRememberMe(null);
        authProps.setAuthorizationHeader(null);
        authProps.setAuthoritiesKey(null);
        authProps.setBearerToken(null);
        authProps.setRefreshTokenValidity(null);
        
        assertThat(authProps).isNotNull();
        assertThat(authProps.getTokenSecret()).isNull();
        assertThat(authProps.getBase64TokenSecret()).isNull();
        assertThat(authProps.getTokenValidity()).isNull();
        assertThat(authProps.getTokenValidityForRememberMe()).isNull();
        assertThat(authProps.getAuthorizationHeader()).isNull();
        assertThat(authProps.getAuthoritiesKey()).isNull();
        assertThat(authProps.getBearerToken()).isNull();
        assertThat(authProps.getRefreshTokenValidity()).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.props.SwaggerAuthPropsService#getMailProps()}.
     */
    @Ignore
    @Test
    public void testGetMailProps()
    {
        //
        final MailProps mailProps = this.swaggerAuthPropsService.getMailProps();

        assertThat(mailProps).isNotNull();
        assertThat(mailProps.getHostValue()).isEqualTo("localhost");
        assertThat(mailProps.getPort()).isEqualTo(25);
        assertThat(mailProps.getUsername()).isEqualTo("test");
        assertThat(mailProps.getPassword()).isEqualTo("test-mail");
        assertThat(mailProps.getProtocol()).isEqualToIgnoringCase("SMTP");
        assertThat(mailProps.getTlsValue()).isFalse();
        assertThat(mailProps.getAuthValue()).isFalse();
        assertThat(mailProps.getFromValue()).containsIgnoringCase("@localhost");

        assertThat(mailProps.toString()).isNotEmpty();
    }

    @Test
    public void testGetMailProps_WithNullElement()
    {
        //
        final MailProps mailProps = this.swaggerAuthPropsService.getMailProps();

        mailProps.setHostValue(null);
        mailProps.setPort(null);
        mailProps.setUsername(null);
        mailProps.setPassword(null);
        mailProps.setProtocol(null);
        mailProps.setTlsValue(null);
        mailProps.setAuthValue(null);
        mailProps.setFromValue(null);
        
        assertThat(mailProps).isNotNull();
        assertThat(mailProps.getHostValue()).isNull();
        assertThat(mailProps.getPort()).isNull();
        assertThat(mailProps.getUsername()).isNull();
        assertThat(mailProps.getPassword()).isNull();
        assertThat(mailProps.getProtocol()).isNull();
        assertThat(mailProps.getTlsValue()).isNull();
        assertThat(mailProps.getAuthValue()).isNull();
        assertThat(mailProps.getFromValue()).isNull();
    }
}
