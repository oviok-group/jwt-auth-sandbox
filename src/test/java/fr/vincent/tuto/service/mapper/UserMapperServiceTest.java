/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : UserMapperServiceTest.java
 * Date de création : 25 oct. 2020
 * Heure de création : 21:40:00
 * Package : fr.vincent.tuto.service.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.config.JwtAuthSandboxBaseConfig;
import fr.vincent.tuto.enumeration.RoleNameEnum;
import fr.vincent.tuto.model.dto.UserData;
import fr.vincent.tuto.model.po.User;

/**
 * Unit tests for the {@link UserMapperService} class.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "authObjectMapperServiceTest", classes = { JwtAuthSandboxBaseConfig.class,
        UserMapperService.class })
@SpringBootTest
@ActiveProfiles("test")
public class UserMapperServiceTest
{

    @Autowired
    private UserMapperService userMapperService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder; // pour chiffrer les mots de passe des utilisateurs

    @InjectMocks
    @Resource
    private ModelMapper modelMapper;

    private User user;

    private UserData userData;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.userMapperService.setModelMapper(this.modelMapper);

        this.user = this.createUser();
        this.userData = this.createDto();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.modelMapper = null;
        this.userMapperService = null;
        this.user = null;
        this.userData = null;
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.service.mapper.UserMapperService#toEntity(fr.vincent.tuto.model.dto.UserData, java.lang.Boolean)}.
     */
    @Test
    public void testToEntity()
    {
        final User result = this.userMapperService.toEntity(this.userData, Boolean.TRUE);

        assertThat(result).isNotNull();
        assertThat(result.getId()).isNull();
    }

    @Test
    public void testToEntity_NonActive()
    {
        final User result = this.userMapperService.toEntity(this.userData, Boolean.FALSE);

        assertThat(result).isNotNull();
        assertThat(result.getId()).isNull();
    }

    @Test
    public void testToEntity_WithNullDates()
    {
        final Set<String> adminRole = new HashSet<>();
        adminRole.add(RoleNameEnum.ROLE_ADMIN.name());
        adminRole.add(RoleNameEnum.ROLE_CLIENT.name());
        final UserData dto = new UserData()//
        .username("admin1Dto").password(this.bCryptPasswordEncoder.encode("admin1Dto_19511982"))//
        .email("admin1Dto.test@live.fr")//
        .dateCreation(null).horodatage(null)//
        .roles(adminRole);

        final User result = this.userMapperService.toEntity(dto, Boolean.TRUE);

        assertThat(result).isNotNull();
        assertThat(result.getId()).isNull();
        assertThat(result.getRoles().size()).isEqualTo(2);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.service.mapper.UserMapperService#toEntityList(java.util.Collection, java.lang.Boolean)}.
     */
    @Test
    public void testToEntityList()
    {
        final List<UserData> userDatas = Arrays.asList(this.userData);
        final List<User> users = (List<User>) this.userMapperService.toEntityList(userDatas, Boolean.TRUE);

        assertThat(users).isNotEmpty();
        assertThat(users.size()).isEqualTo(1);
        assertThat(users.get(0).getId()).isNull();
        assertThat(users.get(0).getActive()).isTrue();
        assertThat(users.get(0).getRoles()).isNotNull();
        assertThat(users.get(0).getRoles().size()).isEqualTo(2);
    }

    @Test
    public void testToEntityListNonActive()
    {
        final List<UserData> userDatas = Arrays.asList(this.userData);
        final List<User> users = (List<User>) this.userMapperService.toEntityList(userDatas, Boolean.FALSE);

        assertThat(users).isNotEmpty();
        assertThat(users.size()).isEqualTo(1);
        assertThat(users.get(0).getId()).isNull();
        assertThat(users.get(0).getActive()).isFalse();
        assertThat(users.get(0).getRoles()).isNotNull();
        assertThat(users.get(0).getRoles().size()).isEqualTo(2);
    }

    @Test
    public void testToEntityListShouldThrowNullPointerExceptionWhenNull()
    {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            // do whatever you want to do here
            // ex : objectName.thisMethodShoulThrowNullPointerExceptionForNullParameter(null);

            this.userMapperService.toEntityList(null, Boolean.TRUE);
        });

        assertThat(exception).isNotNull();
        assertThat(exception.getMessage()).isNull();
    }

    @Test
    public void testToEntityList_EmptyList()
    {
        final List<User> users = (List<User>) this.userMapperService.toEntityList(Collections.EMPTY_LIST,
        Boolean.TRUE);

        assertThat(users).isEmpty();
        assertThat(users.size()).isEqualTo(0);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.service.mapper.UserMapperService#toDto(fr.vincent.tuto.model.po.User)}.
     */
    @Test
    public void testToDto()
    {
        final UserData result = this.userMapperService.toDto(this.user);

        assertThat(result).isNotNull();
        assertThat(result.getId()).isNotNull();
        assertThat(result.getUsername()).isNotNull();
        assertThat(result.getPassword()).isNotNull();
        assertThat(result.getActive()).isTrue();
        assertThat(result.getDateCreation()).isNotNull();
        assertThat(result.getHorodatage()).isNotNull();
        assertThat(result.getVersion()).isEqualTo(0);
        assertThat(result.getRoles()).isNotNull();
        assertThat(result.getRoles().size()).isEqualTo(2);
        assertThat(result.getUsername()).isEqualTo("admin1");

        assertThat(result.toString()).containsIgnoringCase("ANONYMOUS");
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.mapper.UserMapperService#toDtoList(java.util.Collection)}.
     */
    @Test
    public void testToDtoList()
    {

        final List<User> users = Arrays.asList(this.user);
        final List<UserData> userDatas = (List<UserData>) this.userMapperService.toDtoList(users);

        assertThat(userDatas).isNotEmpty();
        assertThat(userDatas.size()).isEqualTo(1);
        assertThat(userDatas.get(0).getId()).isNotNull();
        assertThat(userDatas.get(0).getActive()).isTrue();
        assertThat(userDatas.get(0).getRoles()).isNotNull();
        assertThat(userDatas.get(0).getRoles().size()).isEqualTo(2);
    }

    @Test
    public void testToDtoListWithEmptyList()
    {

        final List<UserData> userDatas = (List<UserData>) this.userMapperService.toDtoList(Collections.EMPTY_LIST);

        assertThat(userDatas).isEmpty();
        assertThat(userDatas.size()).isEqualTo(0);
    }

    @Test
    public void testToDtoListShouldThrowNullPointerExceptionWhenNull()
    {
        Exception exception = assertThrows(NullPointerException.class, () -> {

            this.userMapperService.toDtoList(null);
        });

        assertThat(exception).isNotNull();
        assertThat(exception.getMessage()).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.mapper.UserMapperService#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.userMapperService.afterPropertiesSet();
    }

    @Test
    public void testIsNotNulResources()
    {
        assertThat(this.bCryptPasswordEncoder).isNotNull();
        assertThat(this.modelMapper).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.mapper.UserMapperService#destroy()}.
     * 
     * @throws Exception
     */
    @Test
    public void testDestroy() throws Exception
    {
        this.userMapperService.destroy();
    }

    private User createUser()
    {
        final Set<RoleNameEnum> adminRole = new HashSet<>();
        adminRole.add(RoleNameEnum.ROLE_ADMIN);
        adminRole.add(RoleNameEnum.ROLE_ANONYMOUS);
        final User admin = new User()//
        .id(1l)//
        .username("admin1").password(this.bCryptPasswordEncoder.encode("admin1_19511982"))//
        .email("admin1.test@live.fr").active(Boolean.TRUE)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(adminRole);
        admin.setVersion(0);
        return admin;
    }

    private UserData createDto()
    {
        final Set<String> adminRole = new HashSet<>();
        adminRole.add(RoleNameEnum.ROLE_ADMIN.name());
        adminRole.add(RoleNameEnum.ROLE_ANONYMOUS.name());
        final UserData dto = new UserData()//
        .username("admin1Dto").password(this.bCryptPasswordEncoder.encode("admin1Dto_19511982"))//
        .email("admin1Dto.test@live.fr").active(null)//
        .dateCreation(LocalDate.now()).horodatage(LocalDateTime.now())//
        .roles(adminRole);
        return dto;

    }

}
