/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AuthUserDetailsServiceTest.java
 * Date de création : 26 oct. 2020
 * Heure de création : 16:33:36
 * Package : fr.vincent.tuto.service.security
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.service.security;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.TestsUtils;
import fr.vincent.tuto.config.JwtAuthSandboxBaseConfig;
import fr.vincent.tuto.exception.BusinessAppException;
import fr.vincent.tuto.repository.UserRepository;
import fr.vincent.tuto.service.props.DatabasePropsService;

/**
 * Unit tests for the {@link AuthUserDetailsService} class.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "authUserDetailsServiceTest", classes = { JwtAuthSandboxBaseConfig.class,
        DatabasePropsService.class, AuthUserDetailsService.class })
@SpringBootTest
@ActiveProfiles("test")
public class AuthUserDetailsServiceTest
{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthUserDetailsService authUserDetailsService;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.authUserDetailsService.setUserRepository(this.userRepository);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.userRepository = null;
        this.authUserDetailsService = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.security.AuthUserDetailsService#loadUserByUsername(java.lang.String)}.
     */
    @Test
    public void testLoadUserByUsername()
    {
        final UserDetails userDetails = this.authUserDetailsService.loadUserByUsername(
        TestsUtils.USER_ONE_USERNAME);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(TestsUtils.USER_ONE_USERNAME);
    }

    @Test
    public void testLoadUserByUsernameIgnoreCase()
    {
        final String usernameUppercase = TestsUtils.USER_ONE_USERNAME.toUpperCase(Locale.ENGLISH);
        final UserDetails userDetails = this.authUserDetailsService.loadUserByUsername(usernameUppercase);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(TestsUtils.USER_ONE_USERNAME);
    }

    @Test(expected = BusinessAppException.class)
    public void testLoadUserByEmailInactiveUser()
    {
        final UserDetails userDetails = this.authUserDetailsService.loadUserByUsername(TestsUtils.USER_TWO_EMAIL);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(TestsUtils.USER_TWO_USERNAME);
    }

    @Test(expected = BusinessAppException.class)
    public void testLoadUserByEmailIgnoreCaseInactiveUser()
    {
        final String emailUppercase = TestsUtils.USER_TWO_EMAIL.toUpperCase(Locale.ENGLISH);
        final UserDetails userDetails = this.authUserDetailsService.loadUserByUsername(emailUppercase);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(TestsUtils.USER_TWO_USERNAME);
    }

    @Test
    public void testLoadUserByEmailActiveUser()
    {
        final UserDetails userDetails = this.authUserDetailsService.loadUserByUsername(TestsUtils.USER_ONE_EMAIL);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(TestsUtils.USER_ONE_USERNAME);
    }

    @Test
    public void testLoadUserByEmailIgnoreCaseActiveUser()
    {
        final String emailUppercase = TestsUtils.USER_ONE_EMAIL.toUpperCase(Locale.ENGLISH);
        final UserDetails userDetails = this.authUserDetailsService.loadUserByUsername(emailUppercase);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(TestsUtils.USER_ONE_USERNAME);
    }

    @Test
    public void testLoadUserByEmailActiveUserIsPrioritizedOverLogin()
    {
        final UserDetails userDetails = this.authUserDetailsService.loadUserByUsername(TestsUtils.USER_THREE_EMAIL);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(TestsUtils.USER_THREE_USERNAME);
    }


    /**
     * Test method for {@link fr.vincent.tuto.service.security.AuthUserDetailsService#sGetUserRepository()}.
     */
    @Test
    public void testGetUserRepository()
    {
        this.authUserDetailsService.setUserRepository(this.userRepository);
        assertThat(this.authUserDetailsService.getUserRepository()).isNotNull();
    }

}
