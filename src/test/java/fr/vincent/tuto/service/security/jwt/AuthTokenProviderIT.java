/*
 * ----------------------------------------------
 * Projet ou Module : jwt-auth-sandbox
 * Nom de la classe : AuthTokenProviderIT.java
 * Date de création : 28 oct. 2020
 * Heure de création : 14:24:15
 * Package : fr.vincent.tuto.service.security.jwt
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.service.security.jwt;

import static org.assertj.core.api.Assertions.assertThat;

import java.security.Key;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import fr.vincent.tuto.TestsUtils;
import fr.vincent.tuto.config.JwtAuthSandboxBaseConfig;
import fr.vincent.tuto.exception.BusinessAppException;
import fr.vincent.tuto.repository.UserRepository;
import fr.vincent.tuto.service.props.SwaggerAuthPropsService;
import fr.vincent.tuto.service.security.AuthUserDetailsService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.WeakKeyException;

/**
 * Integrations tests for {@link AuthTokenProvider} class.
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:jwt-auth-sandbox-application-test.properties",
        "classpath:jwt-auth-sandbox-auth-swagger-test.properties" })
@ContextConfiguration(name = "authTokenProviderIT", classes = { JwtAuthSandboxBaseConfig.class,
        SwaggerAuthPropsService.class, UserRepository.class, AuthUserDetailsService.class, AuthTokenProvider.class })
@SpringBootTest
@ActiveProfiles("test")
@Sql(scripts = { "classpath:scripts/schema/schema-h2.sql",
        "classpath:scripts/data/data-h2.sql" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
public class AuthTokenProviderIT
{
    //
    @Autowired
    private AuthTokenProvider authTokenProvider;

    private Key testKey;
    private byte[] keyBytes;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        //
        this.keyBytes = Decoders.BASE64.decode(TestsUtils.REAL_BASE64_TEST_TOKEN);
        this.testKey = Keys.hmacShaKeyFor(this.keyBytes);

        // Key
        ReflectionTestUtils.setField(this.authTokenProvider, "key", testKey);

        // Token validity
        ReflectionTestUtils.setField(this.authTokenProvider, "tokenValidity", TestsUtils.ONE_MINUTE);
        ReflectionTestUtils.setField(this.authTokenProvider, "rememberMeTokenValidity", TestsUtils.ONE_MINUTE);
        ReflectionTestUtils.setField(this.authTokenProvider, "refreshTokenValidity", TestsUtils.ONE_MINUTE);

        // Authority Key
        ReflectionTestUtils.setField(this.authTokenProvider, "authoritiesKey", TestsUtils.AUTHORITIES_KEY);
        this.authTokenProvider.afterPropertiesSet();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.testKey = null;
        this.authTokenProvider = null;
        this.keyBytes = null;
    }


    /**
     * Test method for
     * {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#createAccessToken(org.springframework.security.core.Authentication, java.lang.Boolean)}.
     */
    @Test
    public void testCreateAccessToken()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);
        final Authentication authentication = this.authTokenProvider.getAuthentication(authToken);
        final String refreshToken = this.authTokenProvider.createAccessToken(authentication, Boolean.TRUE);

        assertThat(refreshToken).isNotNull();

        final String username = this.authTokenProvider.getUsername(refreshToken);
        final Claims refreshTokenClaims = this.authTokenProvider.getTokenClaims(refreshToken);

        assertThat(username).isNotNull();
        assertThat(refreshTokenClaims).isNotNull();

        assertThat(refreshTokenClaims.getSubject()).isEqualTo(username);
        assertThat(refreshTokenClaims.getId()).isNotEmpty();
        assertThat(refreshTokenClaims.getId()).isExactlyInstanceOf(String.class);
        assertThat(refreshTokenClaims.getNotBefore()).isNull();
        assertThat(refreshTokenClaims.getExpiration()).isAfter(new Date());
        assertThat(refreshTokenClaims.getIssuedAt()).isBefore(new Date());
    }

    @Test
    public void testCreateAccessToken_WithNoRememberMe()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);
        final Authentication authentication = this.authTokenProvider.getAuthentication(authToken);
        final String refreshToken = this.authTokenProvider.createAccessToken(authentication, Boolean.FALSE);

        assertThat(refreshToken).isNotNull();

        final String username = this.authTokenProvider.getUsername(refreshToken);
        final Claims refreshTokenClaims = this.authTokenProvider.getTokenClaims(refreshToken);

        assertThat(username).isNotNull();
        assertThat(refreshTokenClaims).isNotNull();

        assertThat(refreshTokenClaims.getSubject()).isEqualTo(username);
        assertThat(refreshTokenClaims.getId()).isNotEmpty();
        assertThat(refreshTokenClaims.getId()).isExactlyInstanceOf(String.class);
        assertThat(refreshTokenClaims.getNotBefore()).isNull();
        assertThat(refreshTokenClaims.getExpiration()).isAfter(new Date());
        assertThat(refreshTokenClaims.getIssuedAt()).isBefore(new Date());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateAccessToken_WithNullToken()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(null, TestsUtils.ANONYMOUS_ROLE);
        final Authentication authentication = this.authTokenProvider.getAuthentication(authToken);
        final String refreshToken = this.authTokenProvider.createAccessToken(authentication, Boolean.TRUE);

        assertThat(refreshToken).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void testCreateAccessToken_WithNullAuthentication()
    {
        final String refreshToken = this.authTokenProvider.createAccessToken(null, Boolean.TRUE);

        assertThat(refreshToken).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#createRefreshToken(org.springframework.security.core.Authentication, java.lang.Boolean)}.
     */
    @Test
    public void testCreateRefreshToken()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);
        final Authentication authentication = this.authTokenProvider.getAuthentication(authToken);
        final String refreshToken = this.authTokenProvider.createRefreshToken(authentication, Boolean.TRUE);

        assertThat(refreshToken).isNotNull();

        final String username = this.authTokenProvider.getUsername(refreshToken);
        final Claims refreshTokenClaims = this.authTokenProvider.getTokenClaims(refreshToken);

        assertThat(username).isNotNull();
        assertThat(refreshTokenClaims).isNotNull();

        assertThat(refreshTokenClaims.getSubject()).isEqualTo(username);
        assertThat(refreshTokenClaims.getId()).isNotEmpty();
        assertThat(refreshTokenClaims.getId()).isExactlyInstanceOf(String.class);
        assertThat(refreshTokenClaims.getNotBefore()).isNull();
        assertThat(refreshTokenClaims.getExpiration()).isAfter(new Date());
        assertThat(refreshTokenClaims.getIssuedAt()).isBefore(new Date());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateRefreshToken_WithNullToken()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(null, TestsUtils.ANONYMOUS_ROLE);
        final Authentication authentication = this.authTokenProvider.getAuthentication(authToken);
        final String refreshToken = this.authTokenProvider.createRefreshToken(authentication, Boolean.TRUE);

        assertThat(refreshToken).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#getAuthentication(java.lang.String)}.
     */
    @Test
    public void testGetAuthentication()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);
        final Authentication authentication = this.authTokenProvider.getAuthentication(authToken);

        assertThat(authentication).isNotNull();
        assertThat(authentication.getName()).isEqualTo(TestsUtils.ANONYMOUS_ROLE);
    }

    @Test(expected = BusinessAppException.class)
    public void testGetAuthentication_WithNotExistUser()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.NOT_EXIST_ANONYMOUS_ROLE);
        final Authentication authentication = this.authTokenProvider.getAuthentication(authToken);

        assertThat(authentication).isNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAuthentication_WithNullToken()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(null, TestsUtils.NOT_EXIST_ANONYMOUS_ROLE);
        final Authentication authentication = this.authTokenProvider.getAuthentication(authToken);

        assertThat(authentication).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#validateToken(java.lang.String)}.
     */

    @Test
    public void testValidateTokenReturnTrue()
    {
        final String realSignatureAuthToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);

        final boolean isTokenValid = this.authTokenProvider.validateToken(realSignatureAuthToken);

        assertThat(isTokenValid).isTrue();
    }
    @Test
    public void testValidateTokenReturnFalseWhenJWThasInvalidSignature()
    {
        final String badSignatureAuthToken = TestsUtils.createTestToken(TestsUtils.BAD_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);

        final boolean isTokenValid = this.authTokenProvider.validateToken(badSignatureAuthToken);

        assertThat(isTokenValid).isFalse();
    }

    @Test
    public void testValidateTokenReturnFalseWhenJWTisMalformed()
    {
        Authentication authentication = TestsUtils.createDataBaseAuthentication(this.authTokenProvider);
        String token = authTokenProvider.createAccessToken(authentication, false);
        String invalidToken = token.substring(1);
        boolean isTokenValid = authTokenProvider.validateToken(invalidToken);

        assertThat(isTokenValid).isFalse();
    }

    @Test
    public void testValidateTokenReturnFalseWhenJWTisExpired()
    {
        ReflectionTestUtils.setField(authTokenProvider, "tokenValidity", -TestsUtils.ONE_MINUTE);
        Authentication authentication = TestsUtils.createDataBaseAuthentication(this.authTokenProvider);
        String token = authTokenProvider.createAccessToken(authentication, false);
        boolean isTokenValid = authTokenProvider.validateToken(token);

        assertThat(isTokenValid).isFalse();
    }

    @Test
    public void testValidateTokenReturnFalseWhenJWTisUnsupported()
    {
        String unsupportedToken = TestsUtils.createUnsupportedToken(this.testKey);
        boolean isTokenValid = authTokenProvider.validateToken(unsupportedToken);

        assertThat(isTokenValid).isFalse();
    }

    @Test
    public void testValidateTokenReturnFalseWhenJWTisInvalid()
    {
        boolean isTokenValid = authTokenProvider.validateToken(StringUtils.EMPTY);

        assertThat(isTokenValid).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#getTokenClaims(java.lang.String)}.
     */
    @Test
    public void testGetTokenClaims()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);
        // final Authentication authentication = this.authTokenProvider.getAuthentication(authToken);

        final Claims claims = this.authTokenProvider.getTokenClaims(authToken);

        // System.err.println(">>>>>> Authnetication Claims : \n" + claims.toString());

        assertThat(claims).isNotNull();
        assertThat(claims.getSubject()).isEqualTo(TestsUtils.ANONYMOUS_ROLE);
        assertThat(claims.getId()).isNotEmpty();
        assertThat(claims.getId()).isExactlyInstanceOf(String.class);
        assertThat(claims.getNotBefore()).isNull();
        assertThat(claims.getExpiration()).isAfter(new Date());
        assertThat(claims.getIssuedAt()).isBefore(new Date());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTokenClaims_WithNullToken()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(null, TestsUtils.ANONYMOUS_ROLE);
        final Claims claims = this.authTokenProvider.getTokenClaims(authToken);
        assertThat(claims).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#getUsername(java.lang.String)}.
     */
    @Test
    public void testGetUsername()
    {
        // with real secret token
        final String realToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);

        // System.err.println(">>>>>>> Token : \n" + realToken);
        String username = this.authTokenProvider.getUsername(realToken);

        assertThat(username).isNotNull();
        assertThat(username).isEqualTo(TestsUtils.ANONYMOUS_ROLE);
    }

    @Test
    public void testGetUsername_WithEmptyUsername()
    {
        // with real secret token
        final String realToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN, StringUtils.EMPTY);

        // System.err.println(">>>>>>> Token : \n" + realToken);
        String username = this.authTokenProvider.getUsername(realToken);

        assertThat(username).isEmpty();
    }

    @Test
    public void testGetUsername_WithNullUsername()
    {
        // with real secret token
        final String realToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN, null);

        // System.err.println(">>>>>>> Token : \n" + realToken);
        String username = this.authTokenProvider.getUsername(realToken);

        assertThat(username).isNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetUsername_WithNullToken()
    {
        // with null secret token
        final String nullToken = TestsUtils.createTestToken(null, TestsUtils.ANONYMOUS_ROLE);
        String username = this.authTokenProvider.getUsername(nullToken);

        assertThat(username).isNull();
    }

    @Test(expected = WeakKeyException.class)
    public void testGetUsername_TokenEmpty()
    {
        // with empty secret token
        final String emptyToken = TestsUtils.createTestToken(StringUtils.EMPTY, TestsUtils.ANONYMOUS_ROLE);
        String username = this.authTokenProvider.getUsername(emptyToken);

        assertThat(username).isNull();
    }

    @Test
    public void testGetUsername_TokenWithEmpty()
    {
        // secret token + empty
        final String emptyToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN + StringUtils.EMPTY,
        TestsUtils.ANONYMOUS_ROLE);
        String username = this.authTokenProvider.getUsername(emptyToken);

        assertThat(username).isNotNull();
        assertThat(username).isEqualTo(TestsUtils.ANONYMOUS_ROLE);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#getTokenSignature(java.lang.String)}.
     */
    @Test
    public void testGetTokenSignature()
    {
        // with real secret token
        final String realToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);
        final String signature = this.authTokenProvider.getTokenSignature(realToken);

        //
        assertThat(signature).isNotNull();
        assertThat(StringUtils.isAlphanumeric(signature)).isFalse();
        assertThat(StringUtils.isMixedCase(signature)).isTrue();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTokenSignature_WithNullToken()
    {
        // with real secret token
        final String signature = TestsUtils.createTestToken(null, null);
        assertThat(signature).isNull();
    }

    @Test(expected = WeakKeyException.class)
    public void testGetTokenSignature_WithEmpty()
    {
        // with real secret token
        final String signature = TestsUtils.createTestToken(StringUtils.EMPTY, TestsUtils.ANONYMOUS_ROLE);
        assertThat(signature).isNull();
    }

    @Test
    public void testGetTokenSignature_WithEmptyUser()
    {
        // with real secret token
        final String signature = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN, StringUtils.EMPTY);

        assertThat(signature).isNotNull();
    }

    @Test
    public void testGetTokenSignature_TokenWithEmpty()
    {
        // with real secret token
        final String signature = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN + StringUtils.EMPTY,
        TestsUtils.ANONYMOUS_ROLE);
        assertThat(StringUtils.isAlphanumeric(signature)).isFalse();
        assertThat(StringUtils.isMixedCase(signature)).isTrue();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#getTokenHeader(java.lang.String)}.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void testGetTokenHeader()
    {
        // with real secret token
        final String authToken = TestsUtils.createTestToken(TestsUtils.REAL_BASE64_TEST_TOKEN,
        TestsUtils.ANONYMOUS_ROLE);
        final JwsHeader header = this.authTokenProvider.getTokenHeader(authToken);

        // System.err.println(">>>>> Header Content : \n " + header.toString());

        assertThat(header).isNotNull();
        assertThat(header.getType()).isEqualTo(Header.JWT_TYPE);
        assertThat(header.getAlgorithm()).isEqualTo(SignatureAlgorithm.HS512.name());
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.authTokenProvider.afterPropertiesSet();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#getOtherAppPropsService()}.
     */
    @Test
    public void testGetOtherAppPropsService()
    {
        assertThat(this.authTokenProvider.getOtherAppPropsService()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.service.security.jwt.AuthTokenProvider#getAuthUserDetailsService()}.
     */
    @Test
    public void testGetAuthUserDetailsService()
    {
        assertThat(this.authTokenProvider.getAuthUserDetailsService()).isNotNull();
        assertThat(this.authTokenProvider.getAuthUserDetailsService().getUserRepository()).isNotNull();
    }

    @Test
    public void testresourcesIsNotNull()
    {
        assertThat(this.authTokenProvider).isNotNull();
    }



}
