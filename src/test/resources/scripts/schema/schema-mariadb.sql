/*  -------------------------------------
	-- DDL : Data Definition Language
	-- DATABASE : MARIADB 
	-- SCHEMA OR CATALOG :  
	-------------------------------------
*/

/*  ---------------------------
	-- Scripts de Suppression 
	---------------------------
*/
-- Suppression de la séquence hibernate_sequence
DROP TABLE IF EXISTS hibernate_sequence;

-- Suppression de la TABLE T_USERS
DROP TABLE IF EXISTS T_USERS;

-- Suppression de la TABLE USER_ROLES
DROP TABLE IF EXISTS USER_ROLES;

/* ------------------------
	-- Scripts de Création 
	-----------------------
*/ 
-- Création de la séquence pour incrément automatique de l'identifiant
CREATE TABLE hibernate_sequence (
	next_val BIGINT(20)
) engine=MyISAM;

-- Initialisation de la séquence 
INSERT INTO hibernate_sequence VALUES (1);

-- Création de la TABLE T_USERS
CREATE TABLE T_USERS (
	ID BIGINT(20) NOT NULL,
	USER_ACTIVE BIT(1),
	DATE_CREATION DATE,
	EMAIL VARCHAR(25) NOT NULL,
	HORODATAGE DATETIME,
	USER_PASSWORD VARCHAR(255),
	LOGIN VARCHAR(80) NOT NULL,
	OPTLOCK INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (ID)
) engine=MyISAM;

-- Création de la TABLE USERS_ROLES
CREATE TABLE USER_ROLES (
	USER_ID BIGINT(20) NOT NULL,
	ROLES INT(11)
) engine=MyISAM;

-- Contrainte unicité de l'adresse électronique
ALTER TABLE T_USERS 
	ADD CONSTRAINT UK_kbdgs6v1gu1pcoq5u9ohje6ep unique (EMAIL);

-- Contrainte unicité du login
ALTER TABLE T_USERS 
	ADD CONSTRAINT UK_37ow8xtktqu7dgkbq7kfywdh0 unique (LOGIN);

-- Contrainte de la clé étrangère dans la TABLE USERS_ROLES
ALTER TABLE USER_ROLES 
	ADD CONSTRAINT FKs6y4k5lgw4a4ei5lj2u2ibkh5 
	FOREIGN KEY (USER_ID) 
	REFERENCES T_USERS (ID);